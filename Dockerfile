FROM openjdk:8-jre
COPY ./build/libs/inforge-0.0.1-SNAPSHOT.jar /usr/share/inforge/inforge.jar

EXPOSE 8080

WORKDIR /usr/share/inforge
ENTRYPOINT ["java", "-jar", "inforge.jar"]
