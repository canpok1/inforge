package jp.gr.java_conf.ktnet.inforge.component;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatworkSupportParserTest {

  @Autowired
  private ChatworkSupportParser target;
  
  @Test
  public void fetchArticleで例外が発生しないこと() throws Exception {
    target.fetchArticle(
        "http://help.chatwork.com/hc/ja/sections"
        + "/201917121-%E3%83%AA%E3%83%AA%E3%83%BC%E3%82%B9-%E3%81%8A%E7%9F%A5%E3%82%89%E3%81%9B");
  }
}
