CREATE SCHEMA IF NOT EXISTS inforge;

--------------------------------------------------
-- 外部サービス用アカウント情報テーブル
--------------------------------------------------
CREATE TABLE inforge.accounts (
    account_id IDENTITY PRIMARY KEY,
    display_name TEXT NOT NULL,
    service_type SMALLINT NOT NULL,
    user_name TEXT,
    api_token TEXT,
    icon_emoji TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP()
);
COMMENT ON TABLE inforge.accounts IS '外部サービス用アカウント情報テーブル';
COMMENT ON COLUMN inforge.accounts.account_id IS 'ID';
COMMENT ON COLUMN inforge.accounts.display_name IS '表示名';
COMMENT ON COLUMN inforge.accounts.service_type IS '外部サービスの種類
0: Chatwork
1: Slack';
COMMENT ON COLUMN inforge.accounts.user_name IS 'アカウント名';
COMMENT ON COLUMN inforge.accounts.api_token IS 'APIトークン';
COMMENT ON COLUMN inforge.accounts.icon_emoji IS 'Slackのアイコンとして使用する絵文字';
COMMENT ON COLUMN inforge.accounts.created_at IS '作成日時';
COMMENT ON COLUMN inforge.accounts.updated_at IS '更新日時';

--------------------------------------------------
-- タスク設定テーブル
--------------------------------------------------
CREATE TABLE inforge.task_settings (
    task_setting_id IDENTITY PRIMARY KEY,
    display_name TEXT NOT NULL,
    cron_expression TEXT,
    condition_type SMALLINT NOT NULL,
    condition_value TEXT,
    condition_account_id BIGINT,
    execution_type SMALLINT NOT NULL,
    execution_value TEXT,
    execution_account_id BIGINT,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    FOREIGN KEY(condition_account_id) REFERENCES accounts(account_id),
    FOREIGN KEY(execution_account_id) REFERENCES accounts(account_id)
);
COMMENT ON TABLE inforge.task_settings IS 'タスク設定テーブル';
COMMENT ON COLUMN inforge.task_settings.task_setting_id IS 'ID';
COMMENT ON COLUMN inforge.task_settings.display_name IS '表示名';
COMMENT ON COLUMN inforge.task_settings.cron_expression IS '定期実行設定';
COMMENT ON COLUMN inforge.task_settings.condition_type IS '条件種別
00:条件なし
10:POSTリクエスト受信時（Bitbucket）
20:RSS更新時
30:WEBサイト更新時（チャットワークサポート）
40:期限切れタスクあり（チャットワーク）
50:本日までタスクあり（チャットワーク）
';
COMMENT ON COLUMN inforge.task_settings.condition_value IS '条件値
条件なし:設定不要
POSTリクエスト受信時:認証トークン
RSS更新:RSSのURL
WEBサイト更新:WEBサイトのURL
期限切れタスクあり:タスク確認先のルームID
本日までタスクあり:タスク確認先のルームID
';
COMMENT ON COLUMN inforge.task_settings.condition_account_id IS '条件用アカウントのID';
COMMENT ON COLUMN inforge.task_settings.execution_type IS '実行種別
10: チャットへの通知（チャットワーク）
11: チャットへの通知（Slack）
';
COMMENT ON COLUMN inforge.task_settings.execution_value IS '実行値
チャットへの通知:通知先のルームID/チャネル名
';
COMMENT ON COLUMN inforge.task_settings.execution_account_id IS '実行用アカウントのID';
COMMENT ON COLUMN inforge.task_settings.created_at IS '作成日時';
COMMENT ON COLUMN inforge.task_settings.updated_at IS '更新日時';


--------------------------------------------------
-- 記事テーブル
--------------------------------------------------
CREATE TABLE inforge.articles (
    article_id IDENTITY PRIMARY KEY,
    title TEXT NOT NULL,
    url TEXT,
    task_setting_id BIGINT NOT NULL,
    notified_at TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    FOREIGN KEY(task_setting_id) REFERENCES task_settings(task_setting_id)
);
COMMENT ON TABLE inforge.articles IS '記事テーブル';
COMMENT ON COLUMN inforge.articles.article_id IS 'ID';
COMMENT ON COLUMN inforge.articles.title IS 'タイトル';
COMMENT ON COLUMN inforge.articles.url IS 'URL';
COMMENT ON COLUMN inforge.articles.task_setting_id IS 'タスク設定ID';
COMMENT ON COLUMN inforge.articles.created_at IS '作成日時';
COMMENT ON COLUMN inforge.articles.updated_at IS '更新日時';
