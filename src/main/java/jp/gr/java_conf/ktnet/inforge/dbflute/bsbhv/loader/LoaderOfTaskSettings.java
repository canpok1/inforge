package jp.gr.java_conf.ktnet.inforge.dbflute.bsbhv.loader;

import java.util.List;

import org.dbflute.bhv.*;
import org.dbflute.bhv.referrer.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;

/**
 * The referrer loader of TASK_SETTINGS as TABLE. <br>
 * <pre>
 * [primary key]
 *     TASK_SETTING_ID
 *
 * [column]
 *     TASK_SETTING_ID, DISPLAY_NAME, CRON_EXPRESSION, CONDITION_TYPE, CONDITION_VALUE, CONDITION_ACCOUNT_ID, EXECUTION_TYPE, EXECUTION_VALUE, EXECUTION_ACCOUNT_ID, CREATED_AT, UPDATED_AT
 *
 * [sequence]
 *     
 *
 * [identity]
 *     TASK_SETTING_ID
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     ACCOUNTS
 *
 * [referrer table]
 *     ARTICLES
 *
 * [foreign property]
 *     accountsByConditionAccountId, accountsByExecutionAccountId
 *
 * [referrer property]
 *     articlesList
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public class LoaderOfTaskSettings {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected List<TaskSettings> _selectedList;
    protected BehaviorSelector _selector;
    protected TaskSettingsBhv _myBhv; // lazy-loaded

    // ===================================================================================
    //                                                                   Ready for Loading
    //                                                                   =================
    public LoaderOfTaskSettings ready(List<TaskSettings> selectedList, BehaviorSelector selector)
    { _selectedList = selectedList; _selector = selector; return this; }

    protected TaskSettingsBhv myBhv()
    { if (_myBhv != null) { return _myBhv; } else { _myBhv = _selector.select(TaskSettingsBhv.class); return _myBhv; } }

    // ===================================================================================
    //                                                                       Load Referrer
    //                                                                       =============
    protected List<Articles> _referrerArticles;

    /**
     * Load referrer of articlesList by the set-upper of referrer. <br>
     * ARTICLES by TASK_SETTING_ID, named 'articlesList'.
     * <pre>
     * <span style="color: #0000C0">taskSettingsBhv</span>.<span style="color: #994747">load</span>(<span style="color: #553000">taskSettingsList</span>, <span style="color: #553000">settingsLoader</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">settingsLoader</span>.<span style="color: #CC4747">loadArticles</span>(<span style="color: #553000">articlesCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *         <span style="color: #553000">articlesCB</span>.setupSelect...
     *         <span style="color: #553000">articlesCB</span>.query().set...
     *         <span style="color: #553000">articlesCB</span>.query().addOrderBy...
     *     }); <span style="color: #3F7E5E">// you can load nested referrer from here</span>
     *     <span style="color: #3F7E5E">//}).withNestedReferrer(<span style="color: #553000">articlesLoader</span> -&gt; {</span>
     *     <span style="color: #3F7E5E">//    articlesLoader.load...</span>
     *     <span style="color: #3F7E5E">//});</span>
     * });
     * for (TaskSettings taskSettings : <span style="color: #553000">taskSettingsList</span>) {
     *     ... = taskSettings.<span style="color: #CC4747">getArticlesList()</span>;
     * }
     * </pre>
     * About internal policy, the value of primary key (and others too) is treated as case-insensitive. <br>
     * The condition-bean, which the set-upper provides, has settings before callback as follows:
     * <pre>
     * cb.query().setTaskSettingId_InScope(pkList);
     * cb.query().addOrderBy_TaskSettingId_Asc();
     * </pre>
     * @param refCBLambda The callback to set up referrer condition-bean for loading referrer. (NotNull)
     * @return The callback interface which you can load nested referrer by calling withNestedReferrer(). (NotNull)
     */
    public NestedReferrerLoaderGateway<LoaderOfArticles> loadArticles(ReferrerConditionSetupper<ArticlesCB> refCBLambda) {
        myBhv().loadArticles(_selectedList, refCBLambda).withNestedReferrer(refLs -> _referrerArticles = refLs);
        return hd -> hd.handle(new LoaderOfArticles().ready(_referrerArticles, _selector));
    }

    // ===================================================================================
    //                                                                    Pull out Foreign
    //                                                                    ================
    protected LoaderOfAccounts _foreignAccountsByConditionAccountIdLoader;
    public LoaderOfAccounts pulloutAccountsByConditionAccountId() {
        if (_foreignAccountsByConditionAccountIdLoader == null)
        { _foreignAccountsByConditionAccountIdLoader = new LoaderOfAccounts().ready(myBhv().pulloutAccountsByConditionAccountId(_selectedList), _selector); }
        return _foreignAccountsByConditionAccountIdLoader;
    }

    protected LoaderOfAccounts _foreignAccountsByExecutionAccountIdLoader;
    public LoaderOfAccounts pulloutAccountsByExecutionAccountId() {
        if (_foreignAccountsByExecutionAccountIdLoader == null)
        { _foreignAccountsByExecutionAccountIdLoader = new LoaderOfAccounts().ready(myBhv().pulloutAccountsByExecutionAccountId(_selectedList), _selector); }
        return _foreignAccountsByExecutionAccountIdLoader;
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    public List<TaskSettings> getSelectedList() { return _selectedList; }
    public BehaviorSelector getSelector() { return _selector; }
}
