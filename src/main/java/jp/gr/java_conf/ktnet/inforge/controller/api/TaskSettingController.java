package jp.gr.java_conf.ktnet.inforge.controller.api;

import jp.gr.java_conf.ktnet.inforge.service.api.TaskSettingService;
import jp.gr.java_conf.ktnet.inforge.service.api.TaskSettingService.GetAllResponse;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
@RequestMapping("/api/task-settings")
public class TaskSettingController {

  @Autowired
  private TaskSettingService taskSettingService;
  
  /**
   * すべてのタスク設定を取得します.
   * @return タスク設定.
   */
  @RequestMapping(method = RequestMethod.GET)
  public GetAllResponse getAll() {
    return taskSettingService.getAll();
  }
  
}
