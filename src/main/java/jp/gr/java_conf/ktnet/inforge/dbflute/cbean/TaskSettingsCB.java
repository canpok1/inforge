package jp.gr.java_conf.ktnet.inforge.dbflute.cbean;

import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.bs.BsTaskSettingsCB;

/**
 * The condition-bean of TASK_SETTINGS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class TaskSettingsCB extends BsTaskSettingsCB {
}
