package jp.gr.java_conf.ktnet.inforge.dbflute.exentity;

import jp.gr.java_conf.ktnet.inforge.dbflute.bsentity.BsArticles;

/**
 * The entity of ARTICLES.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Articles extends BsArticles {

    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;
}
