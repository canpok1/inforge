package jp.gr.java_conf.ktnet.inforge.config;

import jp.gr.java_conf.ktnet.inforge.scheduler.TaskExecuteScheduler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * 定期実行の設定クラスです.
 */
@Configuration
@Slf4j
public class SchedulingConfig implements SchedulingConfigurer {

  @Autowired
  private TaskExecuteScheduler taskExecuteScheduler;
  
  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
    taskExecuteScheduler.registerScheduledTask(taskRegistrar);
  }
}
