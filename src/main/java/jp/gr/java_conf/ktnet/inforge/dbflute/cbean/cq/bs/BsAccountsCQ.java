package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs;

import java.util.Map;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.ciq.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The base condition-query of ACCOUNTS.
 * @author DBFlute(AutoGenerator)
 */
public class BsAccountsCQ extends AbstractBsAccountsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected AccountsCIQ _inlineQuery;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsAccountsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                 InlineView/OrClause
    //                                                                 ===================
    /**
     * Prepare InlineView query. <br>
     * {select ... from ... left outer join (select * from ACCOUNTS) where FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">inline()</span>.setFoo...;
     * </pre>
     * @return The condition-query for InlineView query. (NotNull)
     */
    public AccountsCIQ inline() {
        if (_inlineQuery == null) { _inlineQuery = xcreateCIQ(); }
        _inlineQuery.xsetOnClause(false); return _inlineQuery;
    }

    protected AccountsCIQ xcreateCIQ() {
        AccountsCIQ ciq = xnewCIQ();
        ciq.xsetBaseCB(_baseCB);
        return ciq;
    }

    protected AccountsCIQ xnewCIQ() {
        return new AccountsCIQ(xgetReferrerQuery(), xgetSqlClause(), xgetAliasName(), xgetNestLevel(), this);
    }

    /**
     * Prepare OnClause query. <br>
     * {select ... from ... left outer join ACCOUNTS on ... and FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">on()</span>.setFoo...;
     * </pre>
     * @return The condition-query for OnClause query. (NotNull)
     * @throws IllegalConditionBeanOperationException When this condition-query is base query.
     */
    public AccountsCIQ on() {
        if (isBaseQuery()) { throw new IllegalConditionBeanOperationException("OnClause for local table is unavailable!"); }
        AccountsCIQ inlineQuery = inline(); inlineQuery.xsetOnClause(true); return inlineQuery;
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    protected ConditionValue _accountId;
    public ConditionValue xdfgetAccountId()
    { if (_accountId == null) { _accountId = nCV(); }
      return _accountId; }
    protected ConditionValue xgetCValueAccountId() { return xdfgetAccountId(); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_ExistsReferrer_TaskSettingsByConditionAccountIdList() { return xgetSQueMap("accountId_ExistsReferrer_TaskSettingsByConditionAccountIdList"); }
    public String keepAccountId_ExistsReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_ExistsReferrer_TaskSettingsByConditionAccountIdList", sq); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList() { return xgetSQueMap("accountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList"); }
    public String keepAccountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList", sq); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList() { return xgetSQueMap("accountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList"); }
    public String keepAccountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList", sq); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList() { return xgetSQueMap("accountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList"); }
    public String keepAccountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList", sq); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList() { return xgetSQueMap("accountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList"); }
    public String keepAccountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList", sq); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList() { return xgetSQueMap("accountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList"); }
    public String keepAccountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList", sq); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList() { return xgetSQueMap("accountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList"); }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList", sq); }
    public Map<String, Object> xdfgetAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdListParameter() { return xgetSQuePmMap("accountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList"); }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdListParameter(Object pm) { return xkeepSQuePm("accountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList", pm); }

    public Map<String, TaskSettingsCQ> xdfgetAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList() { return xgetSQueMap("accountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList"); }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq) { return xkeepSQue("accountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList", sq); }
    public Map<String, Object> xdfgetAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdListParameter() { return xgetSQuePmMap("accountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList"); }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdListParameter(Object pm) { return xkeepSQuePm("accountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList", pm); }

    /** 
     * Add order-by as ascend. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_AccountId_Asc() { regOBA("ACCOUNT_ID"); return this; }

    /**
     * Add order-by as descend. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_AccountId_Desc() { regOBD("ACCOUNT_ID"); return this; }

    protected ConditionValue _displayName;
    public ConditionValue xdfgetDisplayName()
    { if (_displayName == null) { _displayName = nCV(); }
      return _displayName; }
    protected ConditionValue xgetCValueDisplayName() { return xdfgetDisplayName(); }

    /** 
     * Add order-by as ascend. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_DisplayName_Asc() { regOBA("DISPLAY_NAME"); return this; }

    /**
     * Add order-by as descend. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_DisplayName_Desc() { regOBD("DISPLAY_NAME"); return this; }

    protected ConditionValue _serviceType;
    public ConditionValue xdfgetServiceType()
    { if (_serviceType == null) { _serviceType = nCV(); }
      return _serviceType; }
    protected ConditionValue xgetCValueServiceType() { return xdfgetServiceType(); }

    /** 
     * Add order-by as ascend. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_ServiceType_Asc() { regOBA("SERVICE_TYPE"); return this; }

    /**
     * Add order-by as descend. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_ServiceType_Desc() { regOBD("SERVICE_TYPE"); return this; }

    protected ConditionValue _userName;
    public ConditionValue xdfgetUserName()
    { if (_userName == null) { _userName = nCV(); }
      return _userName; }
    protected ConditionValue xgetCValueUserName() { return xdfgetUserName(); }

    /** 
     * Add order-by as ascend. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_UserName_Asc() { regOBA("USER_NAME"); return this; }

    /**
     * Add order-by as descend. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_UserName_Desc() { regOBD("USER_NAME"); return this; }

    protected ConditionValue _apiToken;
    public ConditionValue xdfgetApiToken()
    { if (_apiToken == null) { _apiToken = nCV(); }
      return _apiToken; }
    protected ConditionValue xgetCValueApiToken() { return xdfgetApiToken(); }

    /** 
     * Add order-by as ascend. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_ApiToken_Asc() { regOBA("API_TOKEN"); return this; }

    /**
     * Add order-by as descend. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_ApiToken_Desc() { regOBD("API_TOKEN"); return this; }

    protected ConditionValue _iconEmoji;
    public ConditionValue xdfgetIconEmoji()
    { if (_iconEmoji == null) { _iconEmoji = nCV(); }
      return _iconEmoji; }
    protected ConditionValue xgetCValueIconEmoji() { return xdfgetIconEmoji(); }

    /** 
     * Add order-by as ascend. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_IconEmoji_Asc() { regOBA("ICON_EMOJI"); return this; }

    /**
     * Add order-by as descend. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_IconEmoji_Desc() { regOBD("ICON_EMOJI"); return this; }

    protected ConditionValue _createdAt;
    public ConditionValue xdfgetCreatedAt()
    { if (_createdAt == null) { _createdAt = nCV(); }
      return _createdAt; }
    protected ConditionValue xgetCValueCreatedAt() { return xdfgetCreatedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_CreatedAt_Asc() { regOBA("CREATED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_CreatedAt_Desc() { regOBD("CREATED_AT"); return this; }

    protected ConditionValue _updatedAt;
    public ConditionValue xdfgetUpdatedAt()
    { if (_updatedAt == null) { _updatedAt = nCV(); }
      return _updatedAt; }
    protected ConditionValue xgetCValueUpdatedAt() { return xdfgetUpdatedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_UpdatedAt_Asc() { regOBA("UPDATED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsAccountsCQ addOrderBy_UpdatedAt_Desc() { regOBD("UPDATED_AT"); return this; }

    // ===================================================================================
    //                                                             SpecifiedDerivedOrderBy
    //                                                             =======================
    /**
     * Add order-by for specified derived column as ascend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] asc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Asc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsAccountsCQ addSpecifiedDerivedOrderBy_Asc(String aliasName) { registerSpecifiedDerivedOrderBy_Asc(aliasName); return this; }

    /**
     * Add order-by for specified derived column as descend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] desc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Desc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsAccountsCQ addSpecifiedDerivedOrderBy_Desc(String aliasName) { registerSpecifiedDerivedOrderBy_Desc(aliasName); return this; }

    // ===================================================================================
    //                                                                         Union Query
    //                                                                         ===========
    public void reflectRelationOnUnionQuery(ConditionQuery bqs, ConditionQuery uqs) {
    }

    // ===================================================================================
    //                                                                       Foreign Query
    //                                                                       =============
    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String property) {
        return null;
    }

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    public Map<String, AccountsCQ> xdfgetScalarCondition() { return xgetSQueMap("scalarCondition"); }
    public String keepScalarCondition(AccountsCQ sq) { return xkeepSQue("scalarCondition", sq); }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public Map<String, AccountsCQ> xdfgetSpecifyMyselfDerived() { return xgetSQueMap("specifyMyselfDerived"); }
    public String keepSpecifyMyselfDerived(AccountsCQ sq) { return xkeepSQue("specifyMyselfDerived", sq); }

    public Map<String, AccountsCQ> xdfgetQueryMyselfDerived() { return xgetSQueMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerived(AccountsCQ sq) { return xkeepSQue("queryMyselfDerived", sq); }
    public Map<String, Object> xdfgetQueryMyselfDerivedParameter() { return xgetSQuePmMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerivedParameter(Object pm) { return xkeepSQuePm("queryMyselfDerived", pm); }

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    protected Map<String, AccountsCQ> _myselfExistsMap;
    public Map<String, AccountsCQ> xdfgetMyselfExists() { return xgetSQueMap("myselfExists"); }
    public String keepMyselfExists(AccountsCQ sq) { return xkeepSQue("myselfExists", sq); }

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    public Map<String, AccountsCQ> xdfgetMyselfInScope() { return xgetSQueMap("myselfInScope"); }
    public String keepMyselfInScope(AccountsCQ sq) { return xkeepSQue("myselfInScope", sq); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xCB() { return AccountsCB.class.getName(); }
    protected String xCQ() { return AccountsCQ.class.getName(); }
    protected String xCHp() { return HpQDRFunction.class.getName(); }
    protected String xCOp() { return ConditionOption.class.getName(); }
    protected String xMap() { return Map.class.getName(); }
}
