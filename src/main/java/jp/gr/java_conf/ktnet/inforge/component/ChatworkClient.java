package jp.gr.java_conf.ktnet.inforge.component;

import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Accounts;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Chatwork関連の操作を行うクラスです.
 * @author tanabe
 *
 */
@Component
@Slf4j
public class ChatworkClient {

  private static final String CHATWORK_API_BASE_URL = "https://api.chatwork.com/v2";
  
  /**
   * Chatworkにメッセージを投稿します.
   * @param message メッセージ.
   * @param roomId 投稿先ルームID.
   * @param account Chatworkアカウント.
   */
  public void postMessage(
      @NonNull final String message,
      @NonNull final String roomId,
      @NonNull final Accounts account
  ) {
    log.debug("postMessage[message={}, roomId={}, account={}]", message, roomId, account);
    
    if (!account.isServiceTypeChatwork()) {
      throw new IllegalArgumentException("指定アカウントが通知先サービスのものと異なります[" + account +  "]");
    }
    
    String url = CHATWORK_API_BASE_URL + "/rooms/" + roomId + "/messages";
    
    HttpHeaders header = new HttpHeaders();
    header.add("X-ChatWorkToken", account.getApiToken());
    
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
    body.add("body", message);
    
    HttpEntity<?> requestEntity = new HttpEntity<Object>(body, header);
    
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response
        = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
    log.debug("Response[" + response + "]");
  }

}
