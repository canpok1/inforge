package jp.gr.java_conf.ktnet.inforge.dbflute.cbean;

import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.bs.BsArticlesCB;

/**
 * The condition-bean of ARTICLES.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ArticlesCB extends BsArticlesCB {
}
