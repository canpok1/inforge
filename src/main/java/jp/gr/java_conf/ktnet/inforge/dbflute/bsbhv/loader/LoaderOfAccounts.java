package jp.gr.java_conf.ktnet.inforge.dbflute.bsbhv.loader;

import java.util.List;

import org.dbflute.bhv.*;
import org.dbflute.bhv.referrer.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;

/**
 * The referrer loader of ACCOUNTS as TABLE. <br>
 * <pre>
 * [primary key]
 *     ACCOUNT_ID
 *
 * [column]
 *     ACCOUNT_ID, DISPLAY_NAME, SERVICE_TYPE, USER_NAME, API_TOKEN, ICON_EMOJI, CREATED_AT, UPDATED_AT
 *
 * [sequence]
 *     
 *
 * [identity]
 *     ACCOUNT_ID
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     
 *
 * [referrer table]
 *     TASK_SETTINGS
 *
 * [foreign property]
 *     
 *
 * [referrer property]
 *     taskSettingsByConditionAccountIdList, taskSettingsByExecutionAccountIdList
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public class LoaderOfAccounts {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected List<Accounts> _selectedList;
    protected BehaviorSelector _selector;
    protected AccountsBhv _myBhv; // lazy-loaded

    // ===================================================================================
    //                                                                   Ready for Loading
    //                                                                   =================
    public LoaderOfAccounts ready(List<Accounts> selectedList, BehaviorSelector selector)
    { _selectedList = selectedList; _selector = selector; return this; }

    protected AccountsBhv myBhv()
    { if (_myBhv != null) { return _myBhv; } else { _myBhv = _selector.select(AccountsBhv.class); return _myBhv; } }

    // ===================================================================================
    //                                                                       Load Referrer
    //                                                                       =============
    protected List<TaskSettings> _referrerTaskSettingsByConditionAccountId;

    /**
     * Load referrer of taskSettingsByConditionAccountIdList by the set-upper of referrer. <br>
     * TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdList'.
     * <pre>
     * <span style="color: #0000C0">accountsBhv</span>.<span style="color: #994747">load</span>(<span style="color: #553000">accountsList</span>, <span style="color: #553000">accountsLoader</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">accountsLoader</span>.<span style="color: #CC4747">loadTaskSettingsByConditionAccountId</span>(<span style="color: #553000">settingsCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *         <span style="color: #553000">settingsCB</span>.setupSelect...
     *         <span style="color: #553000">settingsCB</span>.query().set...
     *         <span style="color: #553000">settingsCB</span>.query().addOrderBy...
     *     }); <span style="color: #3F7E5E">// you can load nested referrer from here</span>
     *     <span style="color: #3F7E5E">//}).withNestedReferrer(<span style="color: #553000">settingsLoader</span> -&gt; {</span>
     *     <span style="color: #3F7E5E">//    settingsLoader.load...</span>
     *     <span style="color: #3F7E5E">//});</span>
     * });
     * for (Accounts accounts : <span style="color: #553000">accountsList</span>) {
     *     ... = accounts.<span style="color: #CC4747">getTaskSettingsByConditionAccountIdList()</span>;
     * }
     * </pre>
     * About internal policy, the value of primary key (and others too) is treated as case-insensitive. <br>
     * The condition-bean, which the set-upper provides, has settings before callback as follows:
     * <pre>
     * cb.query().setConditionAccountId_InScope(pkList);
     * cb.query().addOrderBy_ConditionAccountId_Asc();
     * </pre>
     * @param refCBLambda The callback to set up referrer condition-bean for loading referrer. (NotNull)
     * @return The callback interface which you can load nested referrer by calling withNestedReferrer(). (NotNull)
     */
    public NestedReferrerLoaderGateway<LoaderOfTaskSettings> loadTaskSettingsByConditionAccountId(ReferrerConditionSetupper<TaskSettingsCB> refCBLambda) {
        myBhv().loadTaskSettingsByConditionAccountId(_selectedList, refCBLambda).withNestedReferrer(refLs -> _referrerTaskSettingsByConditionAccountId = refLs);
        return hd -> hd.handle(new LoaderOfTaskSettings().ready(_referrerTaskSettingsByConditionAccountId, _selector));
    }

    protected List<TaskSettings> _referrerTaskSettingsByExecutionAccountId;

    /**
     * Load referrer of taskSettingsByExecutionAccountIdList by the set-upper of referrer. <br>
     * TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdList'.
     * <pre>
     * <span style="color: #0000C0">accountsBhv</span>.<span style="color: #994747">load</span>(<span style="color: #553000">accountsList</span>, <span style="color: #553000">accountsLoader</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">accountsLoader</span>.<span style="color: #CC4747">loadTaskSettingsByExecutionAccountId</span>(<span style="color: #553000">settingsCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *         <span style="color: #553000">settingsCB</span>.setupSelect...
     *         <span style="color: #553000">settingsCB</span>.query().set...
     *         <span style="color: #553000">settingsCB</span>.query().addOrderBy...
     *     }); <span style="color: #3F7E5E">// you can load nested referrer from here</span>
     *     <span style="color: #3F7E5E">//}).withNestedReferrer(<span style="color: #553000">settingsLoader</span> -&gt; {</span>
     *     <span style="color: #3F7E5E">//    settingsLoader.load...</span>
     *     <span style="color: #3F7E5E">//});</span>
     * });
     * for (Accounts accounts : <span style="color: #553000">accountsList</span>) {
     *     ... = accounts.<span style="color: #CC4747">getTaskSettingsByExecutionAccountIdList()</span>;
     * }
     * </pre>
     * About internal policy, the value of primary key (and others too) is treated as case-insensitive. <br>
     * The condition-bean, which the set-upper provides, has settings before callback as follows:
     * <pre>
     * cb.query().setExecutionAccountId_InScope(pkList);
     * cb.query().addOrderBy_ExecutionAccountId_Asc();
     * </pre>
     * @param refCBLambda The callback to set up referrer condition-bean for loading referrer. (NotNull)
     * @return The callback interface which you can load nested referrer by calling withNestedReferrer(). (NotNull)
     */
    public NestedReferrerLoaderGateway<LoaderOfTaskSettings> loadTaskSettingsByExecutionAccountId(ReferrerConditionSetupper<TaskSettingsCB> refCBLambda) {
        myBhv().loadTaskSettingsByExecutionAccountId(_selectedList, refCBLambda).withNestedReferrer(refLs -> _referrerTaskSettingsByExecutionAccountId = refLs);
        return hd -> hd.handle(new LoaderOfTaskSettings().ready(_referrerTaskSettingsByExecutionAccountId, _selector));
    }

    // ===================================================================================
    //                                                                    Pull out Foreign
    //                                                                    ================
    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    public List<Accounts> getSelectedList() { return _selectedList; }
    public BehaviorSelector getSelector() { return _selector; }
}
