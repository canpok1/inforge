package jp.gr.java_conf.ktnet.inforge.dbflute.cbean;

import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.bs.BsAccountsCB;

/**
 * The condition-bean of ACCOUNTS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class AccountsCB extends BsAccountsCB {
}
