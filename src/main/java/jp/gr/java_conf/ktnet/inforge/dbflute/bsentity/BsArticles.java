package jp.gr.java_conf.ktnet.inforge.dbflute.bsentity;

import java.util.List;
import java.util.ArrayList;

import org.dbflute.Entity;
import org.dbflute.dbmeta.DBMeta;
import org.dbflute.dbmeta.AbstractEntity;
import org.dbflute.dbmeta.accessory.DomainEntity;
import org.dbflute.optional.OptionalEntity;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.DBMetaInstanceHandler;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The entity of ARTICLES as TABLE. <br>
 * 記事テーブル
 * <pre>
 * [primary-key]
 *     ARTICLE_ID
 *
 * [column]
 *     ARTICLE_ID, TITLE, URL, TASK_SETTING_ID, NOTIFIED_AT, CREATED_AT, UPDATED_AT
 *
 * [sequence]
 *     
 *
 * [identity]
 *     ARTICLE_ID
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     TASK_SETTINGS
 *
 * [referrer table]
 *     
 *
 * [foreign property]
 *     taskSettings
 *
 * [referrer property]
 *     
 *
 * [get/set template]
 * /= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 * Long articleId = entity.getArticleId();
 * String title = entity.getTitle();
 * String url = entity.getUrl();
 * Long taskSettingId = entity.getTaskSettingId();
 * java.time.LocalDateTime notifiedAt = entity.getNotifiedAt();
 * java.time.LocalDateTime createdAt = entity.getCreatedAt();
 * java.time.LocalDateTime updatedAt = entity.getUpdatedAt();
 * entity.setArticleId(articleId);
 * entity.setTitle(title);
 * entity.setUrl(url);
 * entity.setTaskSettingId(taskSettingId);
 * entity.setNotifiedAt(notifiedAt);
 * entity.setCreatedAt(createdAt);
 * entity.setUpdatedAt(updatedAt);
 * = = = = = = = = = =/
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsArticles extends AbstractEntity implements DomainEntity {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    /** ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)} */
    protected Long _articleId;

    /** TITLE: {NotNull, CLOB(2147483647)} */
    protected String _title;

    /** URL: {CLOB(2147483647)} */
    protected String _url;

    /** TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS} */
    protected Long _taskSettingId;

    /** NOTIFIED_AT: {TIMESTAMP(23, 10)} */
    protected java.time.LocalDateTime _notifiedAt;

    /** CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} */
    protected java.time.LocalDateTime _createdAt;

    /** UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} */
    protected java.time.LocalDateTime _updatedAt;

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    /** {@inheritDoc} */
    public DBMeta asDBMeta() {
        return DBMetaInstanceHandler.findDBMeta(asTableDbName());
    }

    /** {@inheritDoc} */
    public String asTableDbName() {
        return "ARTICLES";
    }

    // ===================================================================================
    //                                                                        Key Handling
    //                                                                        ============
    /** {@inheritDoc} */
    public boolean hasPrimaryKeyValue() {
        if (_articleId == null) { return false; }
        return true;
    }

    // ===================================================================================
    //                                                                    Foreign Property
    //                                                                    ================
    /** TASK_SETTINGS by my TASK_SETTING_ID, named 'taskSettings'. */
    protected OptionalEntity<TaskSettings> _taskSettings;

    /**
     * [get] TASK_SETTINGS by my TASK_SETTING_ID, named 'taskSettings'. <br>
     * Optional: alwaysPresent(), ifPresent().orElse(), get(), ...
     * @return The entity of foreign property 'taskSettings'. (NotNull, EmptyAllowed: when e.g. null FK column, no setupSelect)
     */
    public OptionalEntity<TaskSettings> getTaskSettings() {
        if (_taskSettings == null) { _taskSettings = OptionalEntity.relationEmpty(this, "taskSettings"); }
        return _taskSettings;
    }

    /**
     * [set] TASK_SETTINGS by my TASK_SETTING_ID, named 'taskSettings'.
     * @param taskSettings The entity of foreign property 'taskSettings'. (NullAllowed)
     */
    public void setTaskSettings(OptionalEntity<TaskSettings> taskSettings) {
        _taskSettings = taskSettings;
    }

    // ===================================================================================
    //                                                                   Referrer Property
    //                                                                   =================
    protected <ELEMENT> List<ELEMENT> newReferrerList() { // overriding to import
        return new ArrayList<ELEMENT>();
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    @Override
    protected boolean doEquals(Object obj) {
        if (obj instanceof BsArticles) {
            BsArticles other = (BsArticles)obj;
            if (!xSV(_articleId, other._articleId)) { return false; }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected int doHashCode(int initial) {
        int hs = initial;
        hs = xCH(hs, asTableDbName());
        hs = xCH(hs, _articleId);
        return hs;
    }

    @Override
    protected String doBuildStringWithRelation(String li) {
        StringBuilder sb = new StringBuilder();
        if (_taskSettings != null && _taskSettings.isPresent())
        { sb.append(li).append(xbRDS(_taskSettings, "taskSettings")); }
        return sb.toString();
    }
    protected <ET extends Entity> String xbRDS(org.dbflute.optional.OptionalEntity<ET> et, String name) { // buildRelationDisplayString()
        return et.get().buildDisplayString(name, true, true);
    }

    @Override
    protected String doBuildColumnString(String dm) {
        StringBuilder sb = new StringBuilder();
        sb.append(dm).append(xfND(_articleId));
        sb.append(dm).append(xfND(_title));
        sb.append(dm).append(xfND(_url));
        sb.append(dm).append(xfND(_taskSettingId));
        sb.append(dm).append(xfND(_notifiedAt));
        sb.append(dm).append(xfND(_createdAt));
        sb.append(dm).append(xfND(_updatedAt));
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length());
        }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }

    @Override
    protected String doBuildRelationString(String dm) {
        StringBuilder sb = new StringBuilder();
        if (_taskSettings != null && _taskSettings.isPresent())
        { sb.append(dm).append("taskSettings"); }
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length()).insert(0, "(").append(")");
        }
        return sb.toString();
    }

    @Override
    public Articles clone() {
        return (Articles)super.clone();
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)} <br>
     * ID
     * @return The value of the column 'ARTICLE_ID'. (basically NotNull if selected: for the constraint)
     */
    public Long getArticleId() {
        checkSpecifiedProperty("articleId");
        return _articleId;
    }

    /**
     * [set] ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)} <br>
     * ID
     * @param articleId The value of the column 'ARTICLE_ID'. (basically NotNull if update: for the constraint)
     */
    public void setArticleId(Long articleId) {
        registerModifiedProperty("articleId");
        _articleId = articleId;
    }

    /**
     * [get] TITLE: {NotNull, CLOB(2147483647)} <br>
     * タイトル
     * @return The value of the column 'TITLE'. (basically NotNull if selected: for the constraint)
     */
    public String getTitle() {
        checkSpecifiedProperty("title");
        return _title;
    }

    /**
     * [set] TITLE: {NotNull, CLOB(2147483647)} <br>
     * タイトル
     * @param title The value of the column 'TITLE'. (basically NotNull if update: for the constraint)
     */
    public void setTitle(String title) {
        registerModifiedProperty("title");
        _title = title;
    }

    /**
     * [get] URL: {CLOB(2147483647)} <br>
     * URL
     * @return The value of the column 'URL'. (NullAllowed even if selected: for no constraint)
     */
    public String getUrl() {
        checkSpecifiedProperty("url");
        return _url;
    }

    /**
     * [set] URL: {CLOB(2147483647)} <br>
     * URL
     * @param url The value of the column 'URL'. (NullAllowed: null update allowed for no constraint)
     */
    public void setUrl(String url) {
        registerModifiedProperty("url");
        _url = url;
    }

    /**
     * [get] TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS} <br>
     * タスク設定ID
     * @return The value of the column 'TASK_SETTING_ID'. (basically NotNull if selected: for the constraint)
     */
    public Long getTaskSettingId() {
        checkSpecifiedProperty("taskSettingId");
        return _taskSettingId;
    }

    /**
     * [set] TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS} <br>
     * タスク設定ID
     * @param taskSettingId The value of the column 'TASK_SETTING_ID'. (basically NotNull if update: for the constraint)
     */
    public void setTaskSettingId(Long taskSettingId) {
        registerModifiedProperty("taskSettingId");
        _taskSettingId = taskSettingId;
    }

    /**
     * [get] NOTIFIED_AT: {TIMESTAMP(23, 10)} <br>
     * @return The value of the column 'NOTIFIED_AT'. (NullAllowed even if selected: for no constraint)
     */
    public java.time.LocalDateTime getNotifiedAt() {
        checkSpecifiedProperty("notifiedAt");
        return _notifiedAt;
    }

    /**
     * [set] NOTIFIED_AT: {TIMESTAMP(23, 10)} <br>
     * @param notifiedAt The value of the column 'NOTIFIED_AT'. (NullAllowed: null update allowed for no constraint)
     */
    public void setNotifiedAt(java.time.LocalDateTime notifiedAt) {
        registerModifiedProperty("notifiedAt");
        _notifiedAt = notifiedAt;
    }

    /**
     * [get] CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 作成日時
     * @return The value of the column 'CREATED_AT'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getCreatedAt() {
        checkSpecifiedProperty("createdAt");
        return _createdAt;
    }

    /**
     * [set] CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 作成日時
     * @param createdAt The value of the column 'CREATED_AT'. (basically NotNull if update: for the constraint)
     */
    public void setCreatedAt(java.time.LocalDateTime createdAt) {
        registerModifiedProperty("createdAt");
        _createdAt = createdAt;
    }

    /**
     * [get] UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 更新日時
     * @return The value of the column 'UPDATED_AT'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getUpdatedAt() {
        checkSpecifiedProperty("updatedAt");
        return _updatedAt;
    }

    /**
     * [set] UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 更新日時
     * @param updatedAt The value of the column 'UPDATED_AT'. (basically NotNull if update: for the constraint)
     */
    public void setUpdatedAt(java.time.LocalDateTime updatedAt) {
        registerModifiedProperty("updatedAt");
        _updatedAt = updatedAt;
    }
}
