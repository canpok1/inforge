package jp.gr.java_conf.ktnet.inforge.component;

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Chatworkサポートページの解析するクラスです.
 * @author tanabe
 *
 */
@Component
@Slf4j
public class ChatworkSupportParser {

  /**
   * 記事一覧を取得します.
   * @param url 取得先URL.
   * @return 記事一覧.
   * @throws IOException 取得できなかった場合.
   * @throws URISyntaxException 記事のURLが不正な場合.
   */
  public List<TitleUrlPair> fetchArticle(String url) throws IOException, URISyntaxException {
    return fetchArticle(new URI(url));
  }
    
  /**
   * 記事一覧を取得します.
   * @param uri 取得先URI.
   * @return 記事一覧.
   * @throws IOException 取得できなかった場合.
   */
  private List<TitleUrlPair> fetchArticle(URI uri) throws IOException {
    Document document = Jsoup.connect(uri.toString()).get();
    
    return document.select(".article-list a").stream()
        .map(
          articleElement -> {
            String title = articleElement.text();
            String urlText = articleElement.attr("href");
            
            TitleUrlPair article = null;
            try {
              URI articleUri = uri.resolve(new URI(urlText));
              article = new TitleUrlPair(title, articleUri.toString());
            } catch (URISyntaxException e) {
              log.warn("url resolve error [{}({})]", title, urlText);
              article = new TitleUrlPair(title, urlText);
            }
            log.trace("fetch article [{}]", article);
            return article;
          }
        )
        .collect(Collectors.toList());
  }
  
  /**
   * 記事情報を保持するクラスです.
   * @author tanabe
   *
   */
  @Data
  public static class TitleUrlPair {
    /**
     * タイトル.
     */
    @NonNull
    private String title;
    
    /**
     * URL.
     */
    @NonNull
    private String url;
  }
}
