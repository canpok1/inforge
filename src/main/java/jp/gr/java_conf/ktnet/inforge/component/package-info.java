/**
 * 特定レイヤーに属さないクラス向けパッケージ.
 * @author tanabe
 *
 */
package jp.gr.java_conf.ktnet.inforge.component;