package jp.gr.java_conf.ktnet.inforge.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.dbflute.Entity;
import org.dbflute.dbmeta.AbstractDBMeta;
import org.dbflute.dbmeta.info.*;
import org.dbflute.dbmeta.name.*;
import org.dbflute.dbmeta.property.PropertyGateway;
import org.dbflute.dbway.DBDef;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The DB meta of ACCOUNTS. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class AccountsDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final AccountsDbm _instance = new AccountsDbm();
    private AccountsDbm() {}
    public static AccountsDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public String getProjectName() { return DBCurrent.getInstance().projectName(); }
    public String getProjectPrefix() { return DBCurrent.getInstance().projectPrefix(); }
    public String getGenerationGapBasePrefix() { return DBCurrent.getInstance().generationGapBasePrefix(); }
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    // -----------------------------------------------------
    //                                       Column Property
    //                                       ---------------
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    { xsetupEpg(); }
    protected void xsetupEpg() {
        setupEpg(_epgMap, et -> ((Accounts)et).getAccountId(), (et, vl) -> ((Accounts)et).setAccountId(ctl(vl)), "accountId");
        setupEpg(_epgMap, et -> ((Accounts)et).getDisplayName(), (et, vl) -> ((Accounts)et).setDisplayName((String)vl), "displayName");
        setupEpg(_epgMap, et -> ((Accounts)et).getServiceType(), (et, vl) -> {
            CDef.ServiceType cls = (CDef.ServiceType)gcls(et, columnServiceType(), vl);
            if (cls != null) {
                ((Accounts)et).setServiceTypeAsServiceType(cls);
            } else {
                ((Accounts)et).mynativeMappingServiceType(ctn(vl, Integer.class));
            }
        }, "serviceType");
        setupEpg(_epgMap, et -> ((Accounts)et).getUserName(), (et, vl) -> ((Accounts)et).setUserName((String)vl), "userName");
        setupEpg(_epgMap, et -> ((Accounts)et).getApiToken(), (et, vl) -> ((Accounts)et).setApiToken((String)vl), "apiToken");
        setupEpg(_epgMap, et -> ((Accounts)et).getIconEmoji(), (et, vl) -> ((Accounts)et).setIconEmoji((String)vl), "iconEmoji");
        setupEpg(_epgMap, et -> ((Accounts)et).getCreatedAt(), (et, vl) -> ((Accounts)et).setCreatedAt(ctldt(vl)), "createdAt");
        setupEpg(_epgMap, et -> ((Accounts)et).getUpdatedAt(), (et, vl) -> ((Accounts)et).setUpdatedAt(ctldt(vl)), "updatedAt");
    }
    public PropertyGateway findPropertyGateway(String prop)
    { return doFindEpg(_epgMap, prop); }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "ACCOUNTS";
    protected final String _tableDispName = "ACCOUNTS";
    protected final String _tablePropertyName = "accounts";
    protected final TableSqlName _tableSqlName = new TableSqlName("INFORGE.ACCOUNTS", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTableDispName() { return _tableDispName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnAccountId = cci("ACCOUNT_ID", "ACCOUNT_ID", null, null, Long.class, "accountId", null, true, true, true, "BIGINT", 19, 0, "NEXT VALUE FOR INFORGE.SYSTEM_SEQUENCE_DF04E657_9E16_4CF0_90E5_D8E8C1FC8A72", false, null, null, null, "taskSettingsByConditionAccountIdList,taskSettingsByExecutionAccountIdList", null, false);
    protected final ColumnInfo _columnDisplayName = cci("DISPLAY_NAME", "DISPLAY_NAME", null, null, String.class, "displayName", null, false, false, true, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnServiceType = cci("SERVICE_TYPE", "SERVICE_TYPE", null, null, Integer.class, "serviceType", null, false, false, true, "SMALLINT", 5, 0, null, false, null, null, null, null, CDef.DefMeta.ServiceType, false);
    protected final ColumnInfo _columnUserName = cci("USER_NAME", "USER_NAME", null, null, String.class, "userName", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnApiToken = cci("API_TOKEN", "API_TOKEN", null, null, String.class, "apiToken", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnIconEmoji = cci("ICON_EMOJI", "ICON_EMOJI", null, null, String.class, "iconEmoji", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnCreatedAt = cci("CREATED_AT", "CREATED_AT", null, null, java.time.LocalDateTime.class, "createdAt", null, false, false, true, "TIMESTAMP", 23, 10, "CURRENT_TIMESTAMP()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnUpdatedAt = cci("UPDATED_AT", "UPDATED_AT", null, null, java.time.LocalDateTime.class, "updatedAt", null, false, false, true, "TIMESTAMP", 23, 10, "CURRENT_TIMESTAMP()", false, null, null, null, null, null, false);

    /**
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnAccountId() { return _columnAccountId; }
    /**
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnDisplayName() { return _columnDisplayName; }
    /**
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnServiceType() { return _columnServiceType; }
    /**
     * USER_NAME: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUserName() { return _columnUserName; }
    /**
     * API_TOKEN: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnApiToken() { return _columnApiToken; }
    /**
     * ICON_EMOJI: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnIconEmoji() { return _columnIconEmoji; }
    /**
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnCreatedAt() { return _columnCreatedAt; }
    /**
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUpdatedAt() { return _columnUpdatedAt; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnAccountId());
        ls.add(columnDisplayName());
        ls.add(columnServiceType());
        ls.add(columnUserName());
        ls.add(columnApiToken());
        ls.add(columnIconEmoji());
        ls.add(columnCreatedAt());
        ls.add(columnUpdatedAt());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnAccountId()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // cannot cache because it uses related DB meta instance while booting
    // (instead, cached by super's collection)
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------
    /**
     * TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdList'.
     * @return The information object of referrer property. (NotNull)
     */
    public ReferrerInfo referrerTaskSettingsByConditionAccountIdList() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnAccountId(), TaskSettingsDbm.getInstance().columnConditionAccountId());
        return cri("CONSTRAINT_98", "taskSettingsByConditionAccountIdList", this, TaskSettingsDbm.getInstance(), mp, false, "accountsByConditionAccountId");
    }
    /**
     * TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdList'.
     * @return The information object of referrer property. (NotNull)
     */
    public ReferrerInfo referrerTaskSettingsByExecutionAccountIdList() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnAccountId(), TaskSettingsDbm.getInstance().columnExecutionAccountId());
        return cri("CONSTRAINT_985", "taskSettingsByExecutionAccountIdList", this, TaskSettingsDbm.getInstance(), mp, false, "accountsByExecutionAccountId");
    }

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============
    public boolean hasIdentity() { return true; }

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Accounts"; }
    public String getConditionBeanTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.cbean.AccountsCB"; }
    public String getBehaviorTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.AccountsBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<Accounts> getEntityType() { return Accounts.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public Accounts newEntity() { return new Accounts(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptPrimaryKeyMap((Accounts)et, mp); }
    public void acceptAllColumnMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptAllColumnMap((Accounts)et, mp); }
    public Map<String, Object> extractPrimaryKeyMap(Entity et) { return doExtractPrimaryKeyMap(et); }
    public Map<String, Object> extractAllColumnMap(Entity et) { return doExtractAllColumnMap(et); }
}
