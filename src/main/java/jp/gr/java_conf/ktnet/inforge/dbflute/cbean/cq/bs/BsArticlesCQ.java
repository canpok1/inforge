package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs;

import java.util.Map;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.ciq.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The base condition-query of ARTICLES.
 * @author DBFlute(AutoGenerator)
 */
public class BsArticlesCQ extends AbstractBsArticlesCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected ArticlesCIQ _inlineQuery;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsArticlesCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                 InlineView/OrClause
    //                                                                 ===================
    /**
     * Prepare InlineView query. <br>
     * {select ... from ... left outer join (select * from ARTICLES) where FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">inline()</span>.setFoo...;
     * </pre>
     * @return The condition-query for InlineView query. (NotNull)
     */
    public ArticlesCIQ inline() {
        if (_inlineQuery == null) { _inlineQuery = xcreateCIQ(); }
        _inlineQuery.xsetOnClause(false); return _inlineQuery;
    }

    protected ArticlesCIQ xcreateCIQ() {
        ArticlesCIQ ciq = xnewCIQ();
        ciq.xsetBaseCB(_baseCB);
        return ciq;
    }

    protected ArticlesCIQ xnewCIQ() {
        return new ArticlesCIQ(xgetReferrerQuery(), xgetSqlClause(), xgetAliasName(), xgetNestLevel(), this);
    }

    /**
     * Prepare OnClause query. <br>
     * {select ... from ... left outer join ARTICLES on ... and FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">on()</span>.setFoo...;
     * </pre>
     * @return The condition-query for OnClause query. (NotNull)
     * @throws IllegalConditionBeanOperationException When this condition-query is base query.
     */
    public ArticlesCIQ on() {
        if (isBaseQuery()) { throw new IllegalConditionBeanOperationException("OnClause for local table is unavailable!"); }
        ArticlesCIQ inlineQuery = inline(); inlineQuery.xsetOnClause(true); return inlineQuery;
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    protected ConditionValue _articleId;
    public ConditionValue xdfgetArticleId()
    { if (_articleId == null) { _articleId = nCV(); }
      return _articleId; }
    protected ConditionValue xgetCValueArticleId() { return xdfgetArticleId(); }

    /** 
     * Add order-by as ascend. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_ArticleId_Asc() { regOBA("ARTICLE_ID"); return this; }

    /**
     * Add order-by as descend. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_ArticleId_Desc() { regOBD("ARTICLE_ID"); return this; }

    protected ConditionValue _title;
    public ConditionValue xdfgetTitle()
    { if (_title == null) { _title = nCV(); }
      return _title; }
    protected ConditionValue xgetCValueTitle() { return xdfgetTitle(); }

    /** 
     * Add order-by as ascend. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_Title_Asc() { regOBA("TITLE"); return this; }

    /**
     * Add order-by as descend. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_Title_Desc() { regOBD("TITLE"); return this; }

    protected ConditionValue _url;
    public ConditionValue xdfgetUrl()
    { if (_url == null) { _url = nCV(); }
      return _url; }
    protected ConditionValue xgetCValueUrl() { return xdfgetUrl(); }

    /** 
     * Add order-by as ascend. <br>
     * URL: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_Url_Asc() { regOBA("URL"); return this; }

    /**
     * Add order-by as descend. <br>
     * URL: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_Url_Desc() { regOBD("URL"); return this; }

    protected ConditionValue _taskSettingId;
    public ConditionValue xdfgetTaskSettingId()
    { if (_taskSettingId == null) { _taskSettingId = nCV(); }
      return _taskSettingId; }
    protected ConditionValue xgetCValueTaskSettingId() { return xdfgetTaskSettingId(); }

    /** 
     * Add order-by as ascend. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_TaskSettingId_Asc() { regOBA("TASK_SETTING_ID"); return this; }

    /**
     * Add order-by as descend. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_TaskSettingId_Desc() { regOBD("TASK_SETTING_ID"); return this; }

    protected ConditionValue _notifiedAt;
    public ConditionValue xdfgetNotifiedAt()
    { if (_notifiedAt == null) { _notifiedAt = nCV(); }
      return _notifiedAt; }
    protected ConditionValue xgetCValueNotifiedAt() { return xdfgetNotifiedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_NotifiedAt_Asc() { regOBA("NOTIFIED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_NotifiedAt_Desc() { regOBD("NOTIFIED_AT"); return this; }

    protected ConditionValue _createdAt;
    public ConditionValue xdfgetCreatedAt()
    { if (_createdAt == null) { _createdAt = nCV(); }
      return _createdAt; }
    protected ConditionValue xgetCValueCreatedAt() { return xdfgetCreatedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_CreatedAt_Asc() { regOBA("CREATED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_CreatedAt_Desc() { regOBD("CREATED_AT"); return this; }

    protected ConditionValue _updatedAt;
    public ConditionValue xdfgetUpdatedAt()
    { if (_updatedAt == null) { _updatedAt = nCV(); }
      return _updatedAt; }
    protected ConditionValue xgetCValueUpdatedAt() { return xdfgetUpdatedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_UpdatedAt_Asc() { regOBA("UPDATED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsArticlesCQ addOrderBy_UpdatedAt_Desc() { regOBD("UPDATED_AT"); return this; }

    // ===================================================================================
    //                                                             SpecifiedDerivedOrderBy
    //                                                             =======================
    /**
     * Add order-by for specified derived column as ascend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] asc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Asc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsArticlesCQ addSpecifiedDerivedOrderBy_Asc(String aliasName) { registerSpecifiedDerivedOrderBy_Asc(aliasName); return this; }

    /**
     * Add order-by for specified derived column as descend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] desc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Desc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsArticlesCQ addSpecifiedDerivedOrderBy_Desc(String aliasName) { registerSpecifiedDerivedOrderBy_Desc(aliasName); return this; }

    // ===================================================================================
    //                                                                         Union Query
    //                                                                         ===========
    public void reflectRelationOnUnionQuery(ConditionQuery bqs, ConditionQuery uqs) {
        ArticlesCQ bq = (ArticlesCQ)bqs;
        ArticlesCQ uq = (ArticlesCQ)uqs;
        if (bq.hasConditionQueryTaskSettings()) {
            uq.queryTaskSettings().reflectRelationOnUnionQuery(bq.queryTaskSettings(), uq.queryTaskSettings());
        }
    }

    // ===================================================================================
    //                                                                       Foreign Query
    //                                                                       =============
    /**
     * Get the condition-query for relation table. <br>
     * TASK_SETTINGS by my TASK_SETTING_ID, named 'taskSettings'.
     * @return The instance of condition-query. (NotNull)
     */
    public TaskSettingsCQ queryTaskSettings() {
        return xdfgetConditionQueryTaskSettings();
    }
    public TaskSettingsCQ xdfgetConditionQueryTaskSettings() {
        String prop = "taskSettings";
        if (!xhasQueRlMap(prop)) { xregQueRl(prop, xcreateQueryTaskSettings()); xsetupOuterJoinTaskSettings(); }
        return xgetQueRlMap(prop);
    }
    protected TaskSettingsCQ xcreateQueryTaskSettings() {
        String nrp = xresolveNRP("ARTICLES", "taskSettings"); String jan = xresolveJAN(nrp, xgetNNLvl());
        return xinitRelCQ(new TaskSettingsCQ(this, xgetSqlClause(), jan, xgetNNLvl()), _baseCB, "taskSettings", nrp);
    }
    protected void xsetupOuterJoinTaskSettings() { xregOutJo("taskSettings"); }
    public boolean hasConditionQueryTaskSettings() { return xhasQueRlMap("taskSettings"); }

    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String property) {
        return null;
    }

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    public Map<String, ArticlesCQ> xdfgetScalarCondition() { return xgetSQueMap("scalarCondition"); }
    public String keepScalarCondition(ArticlesCQ sq) { return xkeepSQue("scalarCondition", sq); }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public Map<String, ArticlesCQ> xdfgetSpecifyMyselfDerived() { return xgetSQueMap("specifyMyselfDerived"); }
    public String keepSpecifyMyselfDerived(ArticlesCQ sq) { return xkeepSQue("specifyMyselfDerived", sq); }

    public Map<String, ArticlesCQ> xdfgetQueryMyselfDerived() { return xgetSQueMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerived(ArticlesCQ sq) { return xkeepSQue("queryMyselfDerived", sq); }
    public Map<String, Object> xdfgetQueryMyselfDerivedParameter() { return xgetSQuePmMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerivedParameter(Object pm) { return xkeepSQuePm("queryMyselfDerived", pm); }

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    protected Map<String, ArticlesCQ> _myselfExistsMap;
    public Map<String, ArticlesCQ> xdfgetMyselfExists() { return xgetSQueMap("myselfExists"); }
    public String keepMyselfExists(ArticlesCQ sq) { return xkeepSQue("myselfExists", sq); }

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    public Map<String, ArticlesCQ> xdfgetMyselfInScope() { return xgetSQueMap("myselfInScope"); }
    public String keepMyselfInScope(ArticlesCQ sq) { return xkeepSQue("myselfInScope", sq); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xCB() { return ArticlesCB.class.getName(); }
    protected String xCQ() { return ArticlesCQ.class.getName(); }
    protected String xCHp() { return HpQDRFunction.class.getName(); }
    protected String xCOp() { return ConditionOption.class.getName(); }
    protected String xMap() { return Map.class.getName(); }
}
