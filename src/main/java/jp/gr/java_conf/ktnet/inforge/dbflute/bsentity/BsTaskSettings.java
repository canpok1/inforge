package jp.gr.java_conf.ktnet.inforge.dbflute.bsentity;

import java.util.List;
import java.util.ArrayList;

import org.dbflute.Entity;
import org.dbflute.dbmeta.DBMeta;
import org.dbflute.dbmeta.AbstractEntity;
import org.dbflute.dbmeta.accessory.DomainEntity;
import org.dbflute.optional.OptionalEntity;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.DBMetaInstanceHandler;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.CDef;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The entity of TASK_SETTINGS as TABLE. <br>
 * タスク設定テーブル
 * <pre>
 * [primary-key]
 *     TASK_SETTING_ID
 *
 * [column]
 *     TASK_SETTING_ID, DISPLAY_NAME, CRON_EXPRESSION, CONDITION_TYPE, CONDITION_VALUE, CONDITION_ACCOUNT_ID, EXECUTION_TYPE, EXECUTION_VALUE, EXECUTION_ACCOUNT_ID, CREATED_AT, UPDATED_AT
 *
 * [sequence]
 *     
 *
 * [identity]
 *     TASK_SETTING_ID
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     ACCOUNTS
 *
 * [referrer table]
 *     ARTICLES
 *
 * [foreign property]
 *     accountsByConditionAccountId, accountsByExecutionAccountId
 *
 * [referrer property]
 *     articlesList
 *
 * [get/set template]
 * /= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 * Long taskSettingId = entity.getTaskSettingId();
 * String displayName = entity.getDisplayName();
 * String cronExpression = entity.getCronExpression();
 * Integer conditionType = entity.getConditionType();
 * String conditionValue = entity.getConditionValue();
 * Long conditionAccountId = entity.getConditionAccountId();
 * Integer executionType = entity.getExecutionType();
 * String executionValue = entity.getExecutionValue();
 * Long executionAccountId = entity.getExecutionAccountId();
 * java.time.LocalDateTime createdAt = entity.getCreatedAt();
 * java.time.LocalDateTime updatedAt = entity.getUpdatedAt();
 * entity.setTaskSettingId(taskSettingId);
 * entity.setDisplayName(displayName);
 * entity.setCronExpression(cronExpression);
 * entity.setConditionType(conditionType);
 * entity.setConditionValue(conditionValue);
 * entity.setConditionAccountId(conditionAccountId);
 * entity.setExecutionType(executionType);
 * entity.setExecutionValue(executionValue);
 * entity.setExecutionAccountId(executionAccountId);
 * entity.setCreatedAt(createdAt);
 * entity.setUpdatedAt(updatedAt);
 * = = = = = = = = = =/
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsTaskSettings extends AbstractEntity implements DomainEntity {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    /** TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)} */
    protected Long _taskSettingId;

    /** DISPLAY_NAME: {NotNull, CLOB(2147483647)} */
    protected String _displayName;

    /** CRON_EXPRESSION: {CLOB(2147483647)} */
    protected String _cronExpression;

    /** CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} */
    protected Integer _conditionType;

    /** CONDITION_VALUE: {CLOB(2147483647)} */
    protected String _conditionValue;

    /** CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS} */
    protected Long _conditionAccountId;

    /** EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} */
    protected Integer _executionType;

    /** EXECUTION_VALUE: {CLOB(2147483647)} */
    protected String _executionValue;

    /** EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS} */
    protected Long _executionAccountId;

    /** CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} */
    protected java.time.LocalDateTime _createdAt;

    /** UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} */
    protected java.time.LocalDateTime _updatedAt;

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    /** {@inheritDoc} */
    public DBMeta asDBMeta() {
        return DBMetaInstanceHandler.findDBMeta(asTableDbName());
    }

    /** {@inheritDoc} */
    public String asTableDbName() {
        return "TASK_SETTINGS";
    }

    // ===================================================================================
    //                                                                        Key Handling
    //                                                                        ============
    /** {@inheritDoc} */
    public boolean hasPrimaryKeyValue() {
        if (_taskSettingId == null) { return false; }
        return true;
    }

    // ===================================================================================
    //                                                             Classification Property
    //                                                             =======================
    /**
     * Get the value of conditionType as the classification of ConditionType. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別
     * <p>It's treated as case insensitive and if the code value is null, it returns null.</p>
     * @return The instance of classification definition (as ENUM type). (NullAllowed: when the column value is null)
     */
    public CDef.ConditionType getConditionTypeAsConditionType() {
        return CDef.ConditionType.codeOf(getConditionType());
    }

    /**
     * Set the value of conditionType as the classification of ConditionType. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別
     * @param cdef The instance of classification definition (as ENUM type). (NullAllowed: if null, null value is set to the column)
     */
    public void setConditionTypeAsConditionType(CDef.ConditionType cdef) {
        setConditionType(cdef != null ? toNumber(cdef.code(), Integer.class) : null);
    }

    /**
     * Get the value of executionType as the classification of ExecutionType. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別
     * <p>It's treated as case insensitive and if the code value is null, it returns null.</p>
     * @return The instance of classification definition (as ENUM type). (NullAllowed: when the column value is null)
     */
    public CDef.ExecutionType getExecutionTypeAsExecutionType() {
        return CDef.ExecutionType.codeOf(getExecutionType());
    }

    /**
     * Set the value of executionType as the classification of ExecutionType. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別
     * @param cdef The instance of classification definition (as ENUM type). (NullAllowed: if null, null value is set to the column)
     */
    public void setExecutionTypeAsExecutionType(CDef.ExecutionType cdef) {
        setExecutionType(cdef != null ? toNumber(cdef.code(), Integer.class) : null);
    }

    // ===================================================================================
    //                                                              Classification Setting
    //                                                              ======================
    /**
     * Set the value of conditionType as None (0). <br>
     * 条件なし: 条件なし
     */
    public void setConditionType_None() {
        setConditionTypeAsConditionType(CDef.ConditionType.None);
    }

    /**
     * Set the value of conditionType as ReceivedPostReq (10). <br>
     * Postリクエスト受信時: Postリクエスト受信時
     */
    public void setConditionType_ReceivedPostReq() {
        setConditionTypeAsConditionType(CDef.ConditionType.ReceivedPostReq);
    }

    /**
     * Set the value of conditionType as UpdatedRss (20). <br>
     * RSS更新時: RSS更新時
     */
    public void setConditionType_UpdatedRss() {
        setConditionTypeAsConditionType(CDef.ConditionType.UpdatedRss);
    }

    /**
     * Set the value of conditionType as UpdatedChatworkSupport (30). <br>
     * Webサイト更新時（チャットワークサポート）: チャットワークサポートの更新時
     */
    public void setConditionType_UpdatedChatworkSupport() {
        setConditionTypeAsConditionType(CDef.ConditionType.UpdatedChatworkSupport);
    }

    /**
     * Set the value of conditionType as ExistOverdueTaskChatwork (40). <br>
     * 期限切れタスクあり（チャットワーク）: チャットワークに期限切れタスクあり
     */
    public void setConditionType_ExistOverdueTaskChatwork() {
        setConditionTypeAsConditionType(CDef.ConditionType.ExistOverdueTaskChatwork);
    }

    /**
     * Set the value of conditionType as ExistDueTaskChatwork (50). <br>
     * 本日までタスクあり（チャットワーク）: チャットワークに本日が期限のタスクあり
     */
    public void setConditionType_ExistDueTaskChatwork() {
        setConditionTypeAsConditionType(CDef.ConditionType.ExistDueTaskChatwork);
    }

    /**
     * Set the value of executionType as NotifyChatwork (10). <br>
     * チャットへの通知（Chatwork）: チャットへの通知（Chatwork）
     */
    public void setExecutionType_NotifyChatwork() {
        setExecutionTypeAsExecutionType(CDef.ExecutionType.NotifyChatwork);
    }

    /**
     * Set the value of executionType as NotifySlack (11). <br>
     * チャットへの通知（Slack）: チャットへの通知（Slack）
     */
    public void setExecutionType_NotifySlack() {
        setExecutionTypeAsExecutionType(CDef.ExecutionType.NotifySlack);
    }

    // ===================================================================================
    //                                                        Classification Determination
    //                                                        ============================
    /**
     * Is the value of conditionType None? <br>
     * 条件なし: 条件なし
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isConditionTypeNone() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.equals(CDef.ConditionType.None) : false;
    }

    /**
     * Is the value of conditionType ReceivedPostReq? <br>
     * Postリクエスト受信時: Postリクエスト受信時
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isConditionTypeReceivedPostReq() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.equals(CDef.ConditionType.ReceivedPostReq) : false;
    }

    /**
     * Is the value of conditionType UpdatedRss? <br>
     * RSS更新時: RSS更新時
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isConditionTypeUpdatedRss() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.equals(CDef.ConditionType.UpdatedRss) : false;
    }

    /**
     * Is the value of conditionType UpdatedChatworkSupport? <br>
     * Webサイト更新時（チャットワークサポート）: チャットワークサポートの更新時
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isConditionTypeUpdatedChatworkSupport() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.equals(CDef.ConditionType.UpdatedChatworkSupport) : false;
    }

    /**
     * Is the value of conditionType ExistOverdueTaskChatwork? <br>
     * 期限切れタスクあり（チャットワーク）: チャットワークに期限切れタスクあり
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isConditionTypeExistOverdueTaskChatwork() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.equals(CDef.ConditionType.ExistOverdueTaskChatwork) : false;
    }

    /**
     * Is the value of conditionType ExistDueTaskChatwork? <br>
     * 本日までタスクあり（チャットワーク）: チャットワークに本日が期限のタスクあり
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isConditionTypeExistDueTaskChatwork() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.equals(CDef.ConditionType.ExistDueTaskChatwork) : false;
    }

    /**
     * Is the value of executionType NotifyChatwork? <br>
     * チャットへの通知（Chatwork）: チャットへの通知（Chatwork）
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isExecutionTypeNotifyChatwork() {
        CDef.ExecutionType cdef = getExecutionTypeAsExecutionType();
        return cdef != null ? cdef.equals(CDef.ExecutionType.NotifyChatwork) : false;
    }

    /**
     * Is the value of executionType NotifySlack? <br>
     * チャットへの通知（Slack）: チャットへの通知（Slack）
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isExecutionTypeNotifySlack() {
        CDef.ExecutionType cdef = getExecutionTypeAsExecutionType();
        return cdef != null ? cdef.equals(CDef.ExecutionType.NotifySlack) : false;
    }

    // ===================================================================================
    //                                                           Classification Name/Alias
    //                                                           =========================
    /**
     * Get the value of the column 'conditionType' as classification name.
     * @return The string of classification name. (NullAllowed: when the column value is null)
     */
    public String getConditionTypeName() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.name() : null;
    }

    /**
     * Get the value of the column 'conditionType' as classification alias.
     * @return The string of classification alias. (NullAllowed: when the column value is null)
     */
    public String getConditionTypeAlias() {
        CDef.ConditionType cdef = getConditionTypeAsConditionType();
        return cdef != null ? cdef.alias() : null;
    }

    /**
     * Get the value of the column 'executionType' as classification name.
     * @return The string of classification name. (NullAllowed: when the column value is null)
     */
    public String getExecutionTypeName() {
        CDef.ExecutionType cdef = getExecutionTypeAsExecutionType();
        return cdef != null ? cdef.name() : null;
    }

    /**
     * Get the value of the column 'executionType' as classification alias.
     * @return The string of classification alias. (NullAllowed: when the column value is null)
     */
    public String getExecutionTypeAlias() {
        CDef.ExecutionType cdef = getExecutionTypeAsExecutionType();
        return cdef != null ? cdef.alias() : null;
    }

    // ===================================================================================
    //                                                                    Foreign Property
    //                                                                    ================
    /** ACCOUNTS by my CONDITION_ACCOUNT_ID, named 'accountsByConditionAccountId'. */
    protected OptionalEntity<Accounts> _accountsByConditionAccountId;

    /**
     * [get] ACCOUNTS by my CONDITION_ACCOUNT_ID, named 'accountsByConditionAccountId'. <br>
     * Optional: alwaysPresent(), ifPresent().orElse(), get(), ...
     * @return The entity of foreign property 'accountsByConditionAccountId'. (NotNull, EmptyAllowed: when e.g. null FK column, no setupSelect)
     */
    public OptionalEntity<Accounts> getAccountsByConditionAccountId() {
        if (_accountsByConditionAccountId == null) { _accountsByConditionAccountId = OptionalEntity.relationEmpty(this, "accountsByConditionAccountId"); }
        return _accountsByConditionAccountId;
    }

    /**
     * [set] ACCOUNTS by my CONDITION_ACCOUNT_ID, named 'accountsByConditionAccountId'.
     * @param accountsByConditionAccountId The entity of foreign property 'accountsByConditionAccountId'. (NullAllowed)
     */
    public void setAccountsByConditionAccountId(OptionalEntity<Accounts> accountsByConditionAccountId) {
        _accountsByConditionAccountId = accountsByConditionAccountId;
    }

    /** ACCOUNTS by my EXECUTION_ACCOUNT_ID, named 'accountsByExecutionAccountId'. */
    protected OptionalEntity<Accounts> _accountsByExecutionAccountId;

    /**
     * [get] ACCOUNTS by my EXECUTION_ACCOUNT_ID, named 'accountsByExecutionAccountId'. <br>
     * Optional: alwaysPresent(), ifPresent().orElse(), get(), ...
     * @return The entity of foreign property 'accountsByExecutionAccountId'. (NotNull, EmptyAllowed: when e.g. null FK column, no setupSelect)
     */
    public OptionalEntity<Accounts> getAccountsByExecutionAccountId() {
        if (_accountsByExecutionAccountId == null) { _accountsByExecutionAccountId = OptionalEntity.relationEmpty(this, "accountsByExecutionAccountId"); }
        return _accountsByExecutionAccountId;
    }

    /**
     * [set] ACCOUNTS by my EXECUTION_ACCOUNT_ID, named 'accountsByExecutionAccountId'.
     * @param accountsByExecutionAccountId The entity of foreign property 'accountsByExecutionAccountId'. (NullAllowed)
     */
    public void setAccountsByExecutionAccountId(OptionalEntity<Accounts> accountsByExecutionAccountId) {
        _accountsByExecutionAccountId = accountsByExecutionAccountId;
    }

    // ===================================================================================
    //                                                                   Referrer Property
    //                                                                   =================
    /** ARTICLES by TASK_SETTING_ID, named 'articlesList'. */
    protected List<Articles> _articlesList;

    /**
     * [get] ARTICLES by TASK_SETTING_ID, named 'articlesList'.
     * @return The entity list of referrer property 'articlesList'. (NotNull: even if no loading, returns empty list)
     */
    public List<Articles> getArticlesList() {
        if (_articlesList == null) { _articlesList = newReferrerList(); }
        return _articlesList;
    }

    /**
     * [set] ARTICLES by TASK_SETTING_ID, named 'articlesList'.
     * @param articlesList The entity list of referrer property 'articlesList'. (NullAllowed)
     */
    public void setArticlesList(List<Articles> articlesList) {
        _articlesList = articlesList;
    }

    protected <ELEMENT> List<ELEMENT> newReferrerList() { // overriding to import
        return new ArrayList<ELEMENT>();
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    @Override
    protected boolean doEquals(Object obj) {
        if (obj instanceof BsTaskSettings) {
            BsTaskSettings other = (BsTaskSettings)obj;
            if (!xSV(_taskSettingId, other._taskSettingId)) { return false; }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected int doHashCode(int initial) {
        int hs = initial;
        hs = xCH(hs, asTableDbName());
        hs = xCH(hs, _taskSettingId);
        return hs;
    }

    @Override
    protected String doBuildStringWithRelation(String li) {
        StringBuilder sb = new StringBuilder();
        if (_accountsByConditionAccountId != null && _accountsByConditionAccountId.isPresent())
        { sb.append(li).append(xbRDS(_accountsByConditionAccountId, "accountsByConditionAccountId")); }
        if (_accountsByExecutionAccountId != null && _accountsByExecutionAccountId.isPresent())
        { sb.append(li).append(xbRDS(_accountsByExecutionAccountId, "accountsByExecutionAccountId")); }
        if (_articlesList != null) { for (Articles et : _articlesList)
        { if (et != null) { sb.append(li).append(xbRDS(et, "articlesList")); } } }
        return sb.toString();
    }
    protected <ET extends Entity> String xbRDS(org.dbflute.optional.OptionalEntity<ET> et, String name) { // buildRelationDisplayString()
        return et.get().buildDisplayString(name, true, true);
    }

    @Override
    protected String doBuildColumnString(String dm) {
        StringBuilder sb = new StringBuilder();
        sb.append(dm).append(xfND(_taskSettingId));
        sb.append(dm).append(xfND(_displayName));
        sb.append(dm).append(xfND(_cronExpression));
        sb.append(dm).append(xfND(_conditionType));
        sb.append(dm).append(xfND(_conditionValue));
        sb.append(dm).append(xfND(_conditionAccountId));
        sb.append(dm).append(xfND(_executionType));
        sb.append(dm).append(xfND(_executionValue));
        sb.append(dm).append(xfND(_executionAccountId));
        sb.append(dm).append(xfND(_createdAt));
        sb.append(dm).append(xfND(_updatedAt));
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length());
        }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }

    @Override
    protected String doBuildRelationString(String dm) {
        StringBuilder sb = new StringBuilder();
        if (_accountsByConditionAccountId != null && _accountsByConditionAccountId.isPresent())
        { sb.append(dm).append("accountsByConditionAccountId"); }
        if (_accountsByExecutionAccountId != null && _accountsByExecutionAccountId.isPresent())
        { sb.append(dm).append("accountsByExecutionAccountId"); }
        if (_articlesList != null && !_articlesList.isEmpty())
        { sb.append(dm).append("articlesList"); }
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length()).insert(0, "(").append(")");
        }
        return sb.toString();
    }

    @Override
    public TaskSettings clone() {
        return (TaskSettings)super.clone();
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)} <br>
     * ID
     * @return The value of the column 'TASK_SETTING_ID'. (basically NotNull if selected: for the constraint)
     */
    public Long getTaskSettingId() {
        checkSpecifiedProperty("taskSettingId");
        return _taskSettingId;
    }

    /**
     * [set] TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)} <br>
     * ID
     * @param taskSettingId The value of the column 'TASK_SETTING_ID'. (basically NotNull if update: for the constraint)
     */
    public void setTaskSettingId(Long taskSettingId) {
        registerModifiedProperty("taskSettingId");
        _taskSettingId = taskSettingId;
    }

    /**
     * [get] DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * 表示名
     * @return The value of the column 'DISPLAY_NAME'. (basically NotNull if selected: for the constraint)
     */
    public String getDisplayName() {
        checkSpecifiedProperty("displayName");
        return _displayName;
    }

    /**
     * [set] DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * 表示名
     * @param displayName The value of the column 'DISPLAY_NAME'. (basically NotNull if update: for the constraint)
     */
    public void setDisplayName(String displayName) {
        registerModifiedProperty("displayName");
        _displayName = displayName;
    }

    /**
     * [get] CRON_EXPRESSION: {CLOB(2147483647)} <br>
     * 定期実行設定
     * @return The value of the column 'CRON_EXPRESSION'. (NullAllowed even if selected: for no constraint)
     */
    public String getCronExpression() {
        checkSpecifiedProperty("cronExpression");
        return _cronExpression;
    }

    /**
     * [set] CRON_EXPRESSION: {CLOB(2147483647)} <br>
     * 定期実行設定
     * @param cronExpression The value of the column 'CRON_EXPRESSION'. (NullAllowed: null update allowed for no constraint)
     */
    public void setCronExpression(String cronExpression) {
        registerModifiedProperty("cronExpression");
        _cronExpression = cronExpression;
    }

    /**
     * [get] CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別<br>
     * 00:条件なし<br>
     * 10:POSTリクエスト受信時（Bitbucket）<br>
     * 20:RSS更新時<br>
     * 30:WEBサイト更新時（チャットワークサポート）<br>
     * 40:期限切れタスクあり（チャットワーク）<br>
     * 50:本日までタスクあり（チャットワーク）
     * @return The value of the column 'CONDITION_TYPE'. (basically NotNull if selected: for the constraint)
     */
    public Integer getConditionType() {
        checkSpecifiedProperty("conditionType");
        return _conditionType;
    }

    /**
     * [set] CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別<br>
     * 00:条件なし<br>
     * 10:POSTリクエスト受信時（Bitbucket）<br>
     * 20:RSS更新時<br>
     * 30:WEBサイト更新時（チャットワークサポート）<br>
     * 40:期限切れタスクあり（チャットワーク）<br>
     * 50:本日までタスクあり（チャットワーク）
     * @param conditionType The value of the column 'CONDITION_TYPE'. (basically NotNull if update: for the constraint)
     */
    protected void setConditionType(Integer conditionType) {
        checkClassificationCode("CONDITION_TYPE", CDef.DefMeta.ConditionType, conditionType);
        registerModifiedProperty("conditionType");
        _conditionType = conditionType;
    }

    /**
     * [get] CONDITION_VALUE: {CLOB(2147483647)} <br>
     * 条件値<br>
     * 条件なし:設定不要<br>
     * POSTリクエスト受信時:認証トークン<br>
     * RSS更新:RSSのURL<br>
     * WEBサイト更新:WEBサイトのURL<br>
     * 期限切れタスクあり:タスク確認先のルームID<br>
     * 本日までタスクあり:タスク確認先のルームID
     * @return The value of the column 'CONDITION_VALUE'. (NullAllowed even if selected: for no constraint)
     */
    public String getConditionValue() {
        checkSpecifiedProperty("conditionValue");
        return _conditionValue;
    }

    /**
     * [set] CONDITION_VALUE: {CLOB(2147483647)} <br>
     * 条件値<br>
     * 条件なし:設定不要<br>
     * POSTリクエスト受信時:認証トークン<br>
     * RSS更新:RSSのURL<br>
     * WEBサイト更新:WEBサイトのURL<br>
     * 期限切れタスクあり:タスク確認先のルームID<br>
     * 本日までタスクあり:タスク確認先のルームID
     * @param conditionValue The value of the column 'CONDITION_VALUE'. (NullAllowed: null update allowed for no constraint)
     */
    public void setConditionValue(String conditionValue) {
        registerModifiedProperty("conditionValue");
        _conditionValue = conditionValue;
    }

    /**
     * [get] CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS} <br>
     * 条件用アカウントのID
     * @return The value of the column 'CONDITION_ACCOUNT_ID'. (NullAllowed even if selected: for no constraint)
     */
    public Long getConditionAccountId() {
        checkSpecifiedProperty("conditionAccountId");
        return _conditionAccountId;
    }

    /**
     * [set] CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS} <br>
     * 条件用アカウントのID
     * @param conditionAccountId The value of the column 'CONDITION_ACCOUNT_ID'. (NullAllowed: null update allowed for no constraint)
     */
    public void setConditionAccountId(Long conditionAccountId) {
        registerModifiedProperty("conditionAccountId");
        _conditionAccountId = conditionAccountId;
    }

    /**
     * [get] EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別<br>
     * 10: チャットへの通知（チャットワーク）<br>
     * 11: チャットへの通知（Slack）
     * @return The value of the column 'EXECUTION_TYPE'. (basically NotNull if selected: for the constraint)
     */
    public Integer getExecutionType() {
        checkSpecifiedProperty("executionType");
        return _executionType;
    }

    /**
     * [set] EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別<br>
     * 10: チャットへの通知（チャットワーク）<br>
     * 11: チャットへの通知（Slack）
     * @param executionType The value of the column 'EXECUTION_TYPE'. (basically NotNull if update: for the constraint)
     */
    protected void setExecutionType(Integer executionType) {
        checkClassificationCode("EXECUTION_TYPE", CDef.DefMeta.ExecutionType, executionType);
        registerModifiedProperty("executionType");
        _executionType = executionType;
    }

    /**
     * [get] EXECUTION_VALUE: {CLOB(2147483647)} <br>
     * 実行値<br>
     * チャットへの通知:通知先のルームID/チャネル名
     * @return The value of the column 'EXECUTION_VALUE'. (NullAllowed even if selected: for no constraint)
     */
    public String getExecutionValue() {
        checkSpecifiedProperty("executionValue");
        return _executionValue;
    }

    /**
     * [set] EXECUTION_VALUE: {CLOB(2147483647)} <br>
     * 実行値<br>
     * チャットへの通知:通知先のルームID/チャネル名
     * @param executionValue The value of the column 'EXECUTION_VALUE'. (NullAllowed: null update allowed for no constraint)
     */
    public void setExecutionValue(String executionValue) {
        registerModifiedProperty("executionValue");
        _executionValue = executionValue;
    }

    /**
     * [get] EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS} <br>
     * 実行用アカウントのID
     * @return The value of the column 'EXECUTION_ACCOUNT_ID'. (NullAllowed even if selected: for no constraint)
     */
    public Long getExecutionAccountId() {
        checkSpecifiedProperty("executionAccountId");
        return _executionAccountId;
    }

    /**
     * [set] EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS} <br>
     * 実行用アカウントのID
     * @param executionAccountId The value of the column 'EXECUTION_ACCOUNT_ID'. (NullAllowed: null update allowed for no constraint)
     */
    public void setExecutionAccountId(Long executionAccountId) {
        registerModifiedProperty("executionAccountId");
        _executionAccountId = executionAccountId;
    }

    /**
     * [get] CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 作成日時
     * @return The value of the column 'CREATED_AT'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getCreatedAt() {
        checkSpecifiedProperty("createdAt");
        return _createdAt;
    }

    /**
     * [set] CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 作成日時
     * @param createdAt The value of the column 'CREATED_AT'. (basically NotNull if update: for the constraint)
     */
    public void setCreatedAt(java.time.LocalDateTime createdAt) {
        registerModifiedProperty("createdAt");
        _createdAt = createdAt;
    }

    /**
     * [get] UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 更新日時
     * @return The value of the column 'UPDATED_AT'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getUpdatedAt() {
        checkSpecifiedProperty("updatedAt");
        return _updatedAt;
    }

    /**
     * [set] UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 更新日時
     * @param updatedAt The value of the column 'UPDATED_AT'. (basically NotNull if update: for the constraint)
     */
    public void setUpdatedAt(java.time.LocalDateTime updatedAt) {
        registerModifiedProperty("updatedAt");
        _updatedAt = updatedAt;
    }

    /**
     * For framework so basically DON'T use this method.
     * @param conditionType The value of the column 'CONDITION_TYPE'. (basically NotNull if update: for the constraint)
     */
    public void mynativeMappingConditionType(Integer conditionType) {
        setConditionType(conditionType);
    }

    /**
     * For framework so basically DON'T use this method.
     * @param executionType The value of the column 'EXECUTION_TYPE'. (basically NotNull if update: for the constraint)
     */
    public void mynativeMappingExecutionType(Integer executionType) {
        setExecutionType(executionType);
    }
}
