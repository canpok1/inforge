package jp.gr.java_conf.ktnet.inforge.dbflute.bsentity;

import java.util.List;
import java.util.ArrayList;

import org.dbflute.dbmeta.DBMeta;
import org.dbflute.dbmeta.AbstractEntity;
import org.dbflute.dbmeta.accessory.DomainEntity;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.DBMetaInstanceHandler;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.CDef;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The entity of ACCOUNTS as TABLE. <br>
 * 外部サービス用アカウント情報テーブル
 * <pre>
 * [primary-key]
 *     ACCOUNT_ID
 *
 * [column]
 *     ACCOUNT_ID, DISPLAY_NAME, SERVICE_TYPE, USER_NAME, API_TOKEN, ICON_EMOJI, CREATED_AT, UPDATED_AT
 *
 * [sequence]
 *     
 *
 * [identity]
 *     ACCOUNT_ID
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     
 *
 * [referrer table]
 *     TASK_SETTINGS
 *
 * [foreign property]
 *     
 *
 * [referrer property]
 *     taskSettingsByConditionAccountIdList, taskSettingsByExecutionAccountIdList
 *
 * [get/set template]
 * /= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 * Long accountId = entity.getAccountId();
 * String displayName = entity.getDisplayName();
 * Integer serviceType = entity.getServiceType();
 * String userName = entity.getUserName();
 * String apiToken = entity.getApiToken();
 * String iconEmoji = entity.getIconEmoji();
 * java.time.LocalDateTime createdAt = entity.getCreatedAt();
 * java.time.LocalDateTime updatedAt = entity.getUpdatedAt();
 * entity.setAccountId(accountId);
 * entity.setDisplayName(displayName);
 * entity.setServiceType(serviceType);
 * entity.setUserName(userName);
 * entity.setApiToken(apiToken);
 * entity.setIconEmoji(iconEmoji);
 * entity.setCreatedAt(createdAt);
 * entity.setUpdatedAt(updatedAt);
 * = = = = = = = = = =/
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsAccounts extends AbstractEntity implements DomainEntity {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** The serial version UID for object serialization. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    /** ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)} */
    protected Long _accountId;

    /** DISPLAY_NAME: {NotNull, CLOB(2147483647)} */
    protected String _displayName;

    /** SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} */
    protected Integer _serviceType;

    /** USER_NAME: {CLOB(2147483647)} */
    protected String _userName;

    /** API_TOKEN: {CLOB(2147483647)} */
    protected String _apiToken;

    /** ICON_EMOJI: {CLOB(2147483647)} */
    protected String _iconEmoji;

    /** CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} */
    protected java.time.LocalDateTime _createdAt;

    /** UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} */
    protected java.time.LocalDateTime _updatedAt;

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    /** {@inheritDoc} */
    public DBMeta asDBMeta() {
        return DBMetaInstanceHandler.findDBMeta(asTableDbName());
    }

    /** {@inheritDoc} */
    public String asTableDbName() {
        return "ACCOUNTS";
    }

    // ===================================================================================
    //                                                                        Key Handling
    //                                                                        ============
    /** {@inheritDoc} */
    public boolean hasPrimaryKeyValue() {
        if (_accountId == null) { return false; }
        return true;
    }

    // ===================================================================================
    //                                                             Classification Property
    //                                                             =======================
    /**
     * Get the value of serviceType as the classification of ServiceType. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類
     * <p>It's treated as case insensitive and if the code value is null, it returns null.</p>
     * @return The instance of classification definition (as ENUM type). (NullAllowed: when the column value is null)
     */
    public CDef.ServiceType getServiceTypeAsServiceType() {
        return CDef.ServiceType.codeOf(getServiceType());
    }

    /**
     * Set the value of serviceType as the classification of ServiceType. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類
     * @param cdef The instance of classification definition (as ENUM type). (NullAllowed: if null, null value is set to the column)
     */
    public void setServiceTypeAsServiceType(CDef.ServiceType cdef) {
        setServiceType(cdef != null ? toNumber(cdef.code(), Integer.class) : null);
    }

    // ===================================================================================
    //                                                              Classification Setting
    //                                                              ======================
    /**
     * Set the value of serviceType as Chatwork (0). <br>
     * Chatwork: Chatwork
     */
    public void setServiceType_Chatwork() {
        setServiceTypeAsServiceType(CDef.ServiceType.Chatwork);
    }

    /**
     * Set the value of serviceType as Slack (1). <br>
     * Slack: Slack
     */
    public void setServiceType_Slack() {
        setServiceTypeAsServiceType(CDef.ServiceType.Slack);
    }

    // ===================================================================================
    //                                                        Classification Determination
    //                                                        ============================
    /**
     * Is the value of serviceType Chatwork? <br>
     * Chatwork: Chatwork
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isServiceTypeChatwork() {
        CDef.ServiceType cdef = getServiceTypeAsServiceType();
        return cdef != null ? cdef.equals(CDef.ServiceType.Chatwork) : false;
    }

    /**
     * Is the value of serviceType Slack? <br>
     * Slack: Slack
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isServiceTypeSlack() {
        CDef.ServiceType cdef = getServiceTypeAsServiceType();
        return cdef != null ? cdef.equals(CDef.ServiceType.Slack) : false;
    }

    // ===================================================================================
    //                                                           Classification Name/Alias
    //                                                           =========================
    /**
     * Get the value of the column 'serviceType' as classification name.
     * @return The string of classification name. (NullAllowed: when the column value is null)
     */
    public String getServiceTypeName() {
        CDef.ServiceType cdef = getServiceTypeAsServiceType();
        return cdef != null ? cdef.name() : null;
    }

    // ===================================================================================
    //                                                                    Foreign Property
    //                                                                    ================
    // ===================================================================================
    //                                                                   Referrer Property
    //                                                                   =================
    /** TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdList'. */
    protected List<TaskSettings> _taskSettingsByConditionAccountIdList;

    /**
     * [get] TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdList'.
     * @return The entity list of referrer property 'taskSettingsByConditionAccountIdList'. (NotNull: even if no loading, returns empty list)
     */
    public List<TaskSettings> getTaskSettingsByConditionAccountIdList() {
        if (_taskSettingsByConditionAccountIdList == null) { _taskSettingsByConditionAccountIdList = newReferrerList(); }
        return _taskSettingsByConditionAccountIdList;
    }

    /**
     * [set] TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdList'.
     * @param taskSettingsByConditionAccountIdList The entity list of referrer property 'taskSettingsByConditionAccountIdList'. (NullAllowed)
     */
    public void setTaskSettingsByConditionAccountIdList(List<TaskSettings> taskSettingsByConditionAccountIdList) {
        _taskSettingsByConditionAccountIdList = taskSettingsByConditionAccountIdList;
    }

    /** TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdList'. */
    protected List<TaskSettings> _taskSettingsByExecutionAccountIdList;

    /**
     * [get] TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdList'.
     * @return The entity list of referrer property 'taskSettingsByExecutionAccountIdList'. (NotNull: even if no loading, returns empty list)
     */
    public List<TaskSettings> getTaskSettingsByExecutionAccountIdList() {
        if (_taskSettingsByExecutionAccountIdList == null) { _taskSettingsByExecutionAccountIdList = newReferrerList(); }
        return _taskSettingsByExecutionAccountIdList;
    }

    /**
     * [set] TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdList'.
     * @param taskSettingsByExecutionAccountIdList The entity list of referrer property 'taskSettingsByExecutionAccountIdList'. (NullAllowed)
     */
    public void setTaskSettingsByExecutionAccountIdList(List<TaskSettings> taskSettingsByExecutionAccountIdList) {
        _taskSettingsByExecutionAccountIdList = taskSettingsByExecutionAccountIdList;
    }

    protected <ELEMENT> List<ELEMENT> newReferrerList() { // overriding to import
        return new ArrayList<ELEMENT>();
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    @Override
    protected boolean doEquals(Object obj) {
        if (obj instanceof BsAccounts) {
            BsAccounts other = (BsAccounts)obj;
            if (!xSV(_accountId, other._accountId)) { return false; }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected int doHashCode(int initial) {
        int hs = initial;
        hs = xCH(hs, asTableDbName());
        hs = xCH(hs, _accountId);
        return hs;
    }

    @Override
    protected String doBuildStringWithRelation(String li) {
        StringBuilder sb = new StringBuilder();
        if (_taskSettingsByConditionAccountIdList != null) { for (TaskSettings et : _taskSettingsByConditionAccountIdList)
        { if (et != null) { sb.append(li).append(xbRDS(et, "taskSettingsByConditionAccountIdList")); } } }
        if (_taskSettingsByExecutionAccountIdList != null) { for (TaskSettings et : _taskSettingsByExecutionAccountIdList)
        { if (et != null) { sb.append(li).append(xbRDS(et, "taskSettingsByExecutionAccountIdList")); } } }
        return sb.toString();
    }

    @Override
    protected String doBuildColumnString(String dm) {
        StringBuilder sb = new StringBuilder();
        sb.append(dm).append(xfND(_accountId));
        sb.append(dm).append(xfND(_displayName));
        sb.append(dm).append(xfND(_serviceType));
        sb.append(dm).append(xfND(_userName));
        sb.append(dm).append(xfND(_apiToken));
        sb.append(dm).append(xfND(_iconEmoji));
        sb.append(dm).append(xfND(_createdAt));
        sb.append(dm).append(xfND(_updatedAt));
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length());
        }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }

    @Override
    protected String doBuildRelationString(String dm) {
        StringBuilder sb = new StringBuilder();
        if (_taskSettingsByConditionAccountIdList != null && !_taskSettingsByConditionAccountIdList.isEmpty())
        { sb.append(dm).append("taskSettingsByConditionAccountIdList"); }
        if (_taskSettingsByExecutionAccountIdList != null && !_taskSettingsByExecutionAccountIdList.isEmpty())
        { sb.append(dm).append("taskSettingsByExecutionAccountIdList"); }
        if (sb.length() > dm.length()) {
            sb.delete(0, dm.length()).insert(0, "(").append(")");
        }
        return sb.toString();
    }

    @Override
    public Accounts clone() {
        return (Accounts)super.clone();
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)} <br>
     * ID
     * @return The value of the column 'ACCOUNT_ID'. (basically NotNull if selected: for the constraint)
     */
    public Long getAccountId() {
        checkSpecifiedProperty("accountId");
        return _accountId;
    }

    /**
     * [set] ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)} <br>
     * ID
     * @param accountId The value of the column 'ACCOUNT_ID'. (basically NotNull if update: for the constraint)
     */
    public void setAccountId(Long accountId) {
        registerModifiedProperty("accountId");
        _accountId = accountId;
    }

    /**
     * [get] DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * 表示名
     * @return The value of the column 'DISPLAY_NAME'. (basically NotNull if selected: for the constraint)
     */
    public String getDisplayName() {
        checkSpecifiedProperty("displayName");
        return _displayName;
    }

    /**
     * [set] DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * 表示名
     * @param displayName The value of the column 'DISPLAY_NAME'. (basically NotNull if update: for the constraint)
     */
    public void setDisplayName(String displayName) {
        registerModifiedProperty("displayName");
        _displayName = displayName;
    }

    /**
     * [get] SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類<br>
     * 0: Chatwork<br>
     * 1: Slack
     * @return The value of the column 'SERVICE_TYPE'. (basically NotNull if selected: for the constraint)
     */
    public Integer getServiceType() {
        checkSpecifiedProperty("serviceType");
        return _serviceType;
    }

    /**
     * [set] SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類<br>
     * 0: Chatwork<br>
     * 1: Slack
     * @param serviceType The value of the column 'SERVICE_TYPE'. (basically NotNull if update: for the constraint)
     */
    protected void setServiceType(Integer serviceType) {
        checkClassificationCode("SERVICE_TYPE", CDef.DefMeta.ServiceType, serviceType);
        registerModifiedProperty("serviceType");
        _serviceType = serviceType;
    }

    /**
     * [get] USER_NAME: {CLOB(2147483647)} <br>
     * アカウント名
     * @return The value of the column 'USER_NAME'. (NullAllowed even if selected: for no constraint)
     */
    public String getUserName() {
        checkSpecifiedProperty("userName");
        return _userName;
    }

    /**
     * [set] USER_NAME: {CLOB(2147483647)} <br>
     * アカウント名
     * @param userName The value of the column 'USER_NAME'. (NullAllowed: null update allowed for no constraint)
     */
    public void setUserName(String userName) {
        registerModifiedProperty("userName");
        _userName = userName;
    }

    /**
     * [get] API_TOKEN: {CLOB(2147483647)} <br>
     * APIトークン
     * @return The value of the column 'API_TOKEN'. (NullAllowed even if selected: for no constraint)
     */
    public String getApiToken() {
        checkSpecifiedProperty("apiToken");
        return _apiToken;
    }

    /**
     * [set] API_TOKEN: {CLOB(2147483647)} <br>
     * APIトークン
     * @param apiToken The value of the column 'API_TOKEN'. (NullAllowed: null update allowed for no constraint)
     */
    public void setApiToken(String apiToken) {
        registerModifiedProperty("apiToken");
        _apiToken = apiToken;
    }

    /**
     * [get] ICON_EMOJI: {CLOB(2147483647)} <br>
     * Slackのアイコンとして使用する絵文字
     * @return The value of the column 'ICON_EMOJI'. (NullAllowed even if selected: for no constraint)
     */
    public String getIconEmoji() {
        checkSpecifiedProperty("iconEmoji");
        return _iconEmoji;
    }

    /**
     * [set] ICON_EMOJI: {CLOB(2147483647)} <br>
     * Slackのアイコンとして使用する絵文字
     * @param iconEmoji The value of the column 'ICON_EMOJI'. (NullAllowed: null update allowed for no constraint)
     */
    public void setIconEmoji(String iconEmoji) {
        registerModifiedProperty("iconEmoji");
        _iconEmoji = iconEmoji;
    }

    /**
     * [get] CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 作成日時
     * @return The value of the column 'CREATED_AT'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getCreatedAt() {
        checkSpecifiedProperty("createdAt");
        return _createdAt;
    }

    /**
     * [set] CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 作成日時
     * @param createdAt The value of the column 'CREATED_AT'. (basically NotNull if update: for the constraint)
     */
    public void setCreatedAt(java.time.LocalDateTime createdAt) {
        registerModifiedProperty("createdAt");
        _createdAt = createdAt;
    }

    /**
     * [get] UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 更新日時
     * @return The value of the column 'UPDATED_AT'. (basically NotNull if selected: for the constraint)
     */
    public java.time.LocalDateTime getUpdatedAt() {
        checkSpecifiedProperty("updatedAt");
        return _updatedAt;
    }

    /**
     * [set] UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]} <br>
     * 更新日時
     * @param updatedAt The value of the column 'UPDATED_AT'. (basically NotNull if update: for the constraint)
     */
    public void setUpdatedAt(java.time.LocalDateTime updatedAt) {
        registerModifiedProperty("updatedAt");
        _updatedAt = updatedAt;
    }

    /**
     * For framework so basically DON'T use this method.
     * @param serviceType The value of the column 'SERVICE_TYPE'. (basically NotNull if update: for the constraint)
     */
    public void mynativeMappingServiceType(Integer serviceType) {
        setServiceType(serviceType);
    }
}
