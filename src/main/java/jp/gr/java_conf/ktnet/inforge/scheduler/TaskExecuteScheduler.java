package jp.gr.java_conf.ktnet.inforge.scheduler;

import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.AccountsBhv;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.TaskSettingsBhv;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Accounts;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.TaskSettings;
import jp.gr.java_conf.ktnet.inforge.service.TaskExecuteService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TaskExecuteScheduler {

  @Autowired
  private AccountsBhv accountsBhv;
  
  @Autowired
  private TaskSettingsBhv taskSettingsBhv;
  
  @Autowired
  private TaskExecuteService taskExecuteService;
  
  /**
   * 定期実行タスクを登録します.
   * @param taskRegistrar タスク登録先オブジェクト.
   */
  public void registerScheduledTask(@NonNull final ScheduledTaskRegistrar taskRegistrar) {
    insertInitialRecord();
    
    taskSettingsBhv.selectList(cb -> { }).stream()
        .forEach(
          setting -> {
            taskRegistrar.addTriggerTask(
                new Runnable() {
                  @Override
                  public void run() {
                    execute(setting);
                  }
                },
                new CronTrigger(setting.getCronExpression())
            );
          }
    );
  }
  
  /**
   * 初期データを投入.
   */
  private void insertInitialRecord() {
    log.debug("初期データ投入");
    {
      Accounts account = new Accounts();
      account.setAccountId(1L);
      account.setDisplayName("ChatworkAccount");
      account.setServiceType_Chatwork();
      accountsBhv.insertOrUpdate(account);
    }
    {
      Accounts account = new Accounts();
      account.setAccountId(2L);
      account.setDisplayName("SlackAccount");
      account.setServiceType_Slack();
      account.setUserName("inforge");
      account.setApiToken("");
      account.setIconEmoji(":santa:");
      accountsBhv.insertOrUpdate(account);
    }
    {
      val account = accountsBhv.selectEntity(
          cb -> {
            cb.query().setServiceType_Equal_Chatwork();
            cb.query().addOrderBy_AccountId_Asc().withNullsFirst();
          }
      );
      
      TaskSettings taskSetting = new TaskSettings();
      taskSetting.setTaskSettingId(1L);
      taskSetting.setDisplayName("チャットワークサポート（リリース・お知らせ）の更新監視");
      taskSetting.setCronExpression("*/15 * * * * ?");
      taskSetting.setConditionType_UpdatedChatworkSupport();
      taskSetting.setConditionValue("http://help.chatwork.com/hc/ja/sections/201917121-%E3%83%AA%E3%83%AA%E3%83%BC%E3%82%B9-%E3%81%8A%E7%9F%A5%E3%82%89%E3%81%9B");
      taskSetting.setExecutionType_NotifyChatwork();
      taskSetting.setExecutionValue("111");
      taskSetting.setExecutionAccountId(account.get().getAccountId());
      taskSettingsBhv.insertOrUpdate(taskSetting);
    }
    {
      val account = accountsBhv.selectEntity(
          cb -> {
            cb.query().setServiceType_Equal_Slack();
            cb.query().addOrderBy_AccountId_Asc().withNullsFirst();
          }
      );
      
      TaskSettings taskSetting = new TaskSettings();
      taskSetting.setTaskSettingId(2L);
      taskSetting.setDisplayName("チャットワークサポート（メンテナンス・障害・その他）の更新監視");
      taskSetting.setCronExpression("*/17 * * * * ?");
      taskSetting.setConditionType_UpdatedChatworkSupport();
      taskSetting.setConditionValue("http://help.chatwork.com/hc/ja/sections/201963352-%E3%83%A1%E3%83%B3%E3%83%86%E3%83%8A%E3%83%B3%E3%82%B9-%E9%9A%9C%E5%AE%B3-%E3%81%9D%E3%81%AE%E4%BB%96");
      taskSetting.setExecutionType_NotifySlack();
      taskSetting.setExecutionValue("notification");
      taskSetting.setExecutionAccountId(account.get().getAccountId());
      taskSettingsBhv.insertOrUpdate(taskSetting);
    }
    
  }
  
  
  /**
   * タスクを実行します.
   * @param taskSetting タスク設定.
   */
  public void execute(@NonNull final TaskSettings taskSetting) {
    log.debug("定期実行[{}] 開始", taskSetting.getDisplayName());
    
    try {
      taskExecuteService.execute(taskSetting);
    } catch (Exception ex) {
      log.error("例外発生", ex);
    } finally {
      log.debug("定期実行[{}] 終了", taskSetting.getDisplayName());
    }
  }
}
