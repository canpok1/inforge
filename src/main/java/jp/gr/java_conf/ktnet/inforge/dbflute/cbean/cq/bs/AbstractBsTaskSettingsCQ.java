package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs;

import java.util.*;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.ordering.*;
import org.dbflute.cbean.scoping.*;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.dbmeta.DBMetaProvider;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The abstract condition-query of TASK_SETTINGS.
 * @author DBFlute(AutoGenerator)
 */
public abstract class AbstractBsTaskSettingsCQ extends AbstractConditionQuery {

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AbstractBsTaskSettingsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    @Override
    protected DBMetaProvider xgetDBMetaProvider() {
        return DBMetaInstanceHandler.getProvider();
    }

    public String asTableDbName() {
        return "TASK_SETTINGS";
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingId The value of taskSettingId as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_Equal(Long taskSettingId) {
        doSetTaskSettingId_Equal(taskSettingId);
    }

    protected void doSetTaskSettingId_Equal(Long taskSettingId) {
        regTaskSettingId(CK_EQ, taskSettingId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingId The value of taskSettingId as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_NotEqual(Long taskSettingId) {
        doSetTaskSettingId_NotEqual(taskSettingId);
    }

    protected void doSetTaskSettingId_NotEqual(Long taskSettingId) {
        regTaskSettingId(CK_NES, taskSettingId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingId The value of taskSettingId as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_GreaterThan(Long taskSettingId) {
        regTaskSettingId(CK_GT, taskSettingId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingId The value of taskSettingId as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_LessThan(Long taskSettingId) {
        regTaskSettingId(CK_LT, taskSettingId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingId The value of taskSettingId as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_GreaterEqual(Long taskSettingId) {
        regTaskSettingId(CK_GE, taskSettingId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingId The value of taskSettingId as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_LessEqual(Long taskSettingId) {
        regTaskSettingId(CK_LE, taskSettingId);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param minNumber The min number of taskSettingId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of taskSettingId. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setTaskSettingId_RangeOf(Long minNumber, Long maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setTaskSettingId_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param minNumber The min number of taskSettingId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of taskSettingId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setTaskSettingId_RangeOf(Long minNumber, Long maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueTaskSettingId(), "TASK_SETTING_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingIdList The collection of taskSettingId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setTaskSettingId_InScope(Collection<Long> taskSettingIdList) {
        doSetTaskSettingId_InScope(taskSettingIdList);
    }

    protected void doSetTaskSettingId_InScope(Collection<Long> taskSettingIdList) {
        regINS(CK_INS, cTL(taskSettingIdList), xgetCValueTaskSettingId(), "TASK_SETTING_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param taskSettingIdList The collection of taskSettingId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setTaskSettingId_NotInScope(Collection<Long> taskSettingIdList) {
        doSetTaskSettingId_NotInScope(taskSettingIdList);
    }

    protected void doSetTaskSettingId_NotInScope(Collection<Long> taskSettingIdList) {
        regINS(CK_NINS, cTL(taskSettingIdList), xgetCValueTaskSettingId(), "TASK_SETTING_ID");
    }

    /**
     * Set up ExistsReferrer (correlated sub-query). <br>
     * {exists (select TASK_SETTING_ID from ARTICLES where ...)} <br>
     * ARTICLES by TASK_SETTING_ID, named 'articlesAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">existsArticles</span>(articlesCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     articlesCB.query().set...
     * });
     * </pre>
     * @param subCBLambda The callback for sub-query of ArticlesList for 'exists'. (NotNull)
     */
    public void existsArticles(SubQuery<ArticlesCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForExistsReferrer(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepTaskSettingId_ExistsReferrer_ArticlesList(cb.query());
        registerExistsReferrer(cb.query(), "TASK_SETTING_ID", "TASK_SETTING_ID", pp, "articlesList");
    }
    public abstract String keepTaskSettingId_ExistsReferrer_ArticlesList(ArticlesCQ sq);

    /**
     * Set up NotExistsReferrer (correlated sub-query). <br>
     * {not exists (select TASK_SETTING_ID from ARTICLES where ...)} <br>
     * ARTICLES by TASK_SETTING_ID, named 'articlesAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">notExistsArticles</span>(articlesCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     articlesCB.query().set...
     * });
     * </pre>
     * @param subCBLambda The callback for sub-query of TaskSettingId_NotExistsReferrer_ArticlesList for 'not exists'. (NotNull)
     */
    public void notExistsArticles(SubQuery<ArticlesCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForExistsReferrer(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepTaskSettingId_NotExistsReferrer_ArticlesList(cb.query());
        registerNotExistsReferrer(cb.query(), "TASK_SETTING_ID", "TASK_SETTING_ID", pp, "articlesList");
    }
    public abstract String keepTaskSettingId_NotExistsReferrer_ArticlesList(ArticlesCQ sq);

    public void xsderiveArticlesList(String fn, SubQuery<ArticlesCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepTaskSettingId_SpecifyDerivedReferrer_ArticlesList(cb.query());
        registerSpecifyDerivedReferrer(fn, cb.query(), "TASK_SETTING_ID", "TASK_SETTING_ID", pp, "articlesList", al, op);
    }
    public abstract String keepTaskSettingId_SpecifyDerivedReferrer_ArticlesList(ArticlesCQ sq);

    /**
     * Prepare for (Query)DerivedReferrer (correlated sub-query). <br>
     * {FOO &lt;= (select max(BAR) from ARTICLES where ...)} <br>
     * ARTICLES by TASK_SETTING_ID, named 'articlesAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">derivedArticles()</span>.<span style="color: #CC4747">max</span>(articlesCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     articlesCB.specify().<span style="color: #CC4747">columnFoo...</span> <span style="color: #3F7E5E">// derived column by function</span>
     *     articlesCB.query().setBar... <span style="color: #3F7E5E">// referrer condition</span>
     * }).<span style="color: #CC4747">greaterEqual</span>(123); <span style="color: #3F7E5E">// condition to derived column</span>
     * </pre>
     * @return The object to set up a function for referrer table. (NotNull)
     */
    public HpQDRFunction<ArticlesCB> derivedArticles() {
        return xcreateQDRFunctionArticlesList();
    }
    protected HpQDRFunction<ArticlesCB> xcreateQDRFunctionArticlesList() {
        return xcQDRFunc((fn, sq, rd, vl, op) -> xqderiveArticlesList(fn, sq, rd, vl, op));
    }
    public void xqderiveArticlesList(String fn, SubQuery<ArticlesCB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String sqpp = keepTaskSettingId_QueryDerivedReferrer_ArticlesList(cb.query()); String prpp = keepTaskSettingId_QueryDerivedReferrer_ArticlesListParameter(vl);
        registerQueryDerivedReferrer(fn, cb.query(), "TASK_SETTING_ID", "TASK_SETTING_ID", sqpp, "articlesList", rd, vl, prpp, op);
    }
    public abstract String keepTaskSettingId_QueryDerivedReferrer_ArticlesList(ArticlesCQ sq);
    public abstract String keepTaskSettingId_QueryDerivedReferrer_ArticlesListParameter(Object vl);

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     */
    public void setTaskSettingId_IsNull() { regTaskSettingId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     */
    public void setTaskSettingId_IsNotNull() { regTaskSettingId(CK_ISNN, DOBJ); }

    protected void regTaskSettingId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueTaskSettingId(), "TASK_SETTING_ID"); }
    protected abstract ConditionValue xgetCValueTaskSettingId();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_Equal(String displayName) {
        doSetDisplayName_Equal(fRES(displayName));
    }

    protected void doSetDisplayName_Equal(String displayName) {
        regDisplayName(CK_EQ, displayName);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_NotEqual(String displayName) {
        doSetDisplayName_NotEqual(fRES(displayName));
    }

    protected void doSetDisplayName_NotEqual(String displayName) {
        regDisplayName(CK_NES, displayName);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_GreaterThan(String displayName) {
        regDisplayName(CK_GT, fRES(displayName));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_LessThan(String displayName) {
        regDisplayName(CK_LT, fRES(displayName));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_GreaterEqual(String displayName) {
        regDisplayName(CK_GE, fRES(displayName));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_LessEqual(String displayName) {
        regDisplayName(CK_LE, fRES(displayName));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayNameList The collection of displayName as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_InScope(Collection<String> displayNameList) {
        doSetDisplayName_InScope(displayNameList);
    }

    protected void doSetDisplayName_InScope(Collection<String> displayNameList) {
        regINS(CK_INS, cTL(displayNameList), xgetCValueDisplayName(), "DISPLAY_NAME");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayNameList The collection of displayName as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_NotInScope(Collection<String> displayNameList) {
        doSetDisplayName_NotInScope(displayNameList);
    }

    protected void doSetDisplayName_NotInScope(Collection<String> displayNameList) {
        regINS(CK_NINS, cTL(displayNameList), xgetCValueDisplayName(), "DISPLAY_NAME");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * <pre>e.g. setDisplayName_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param displayName The value of displayName as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setDisplayName_LikeSearch(String displayName, ConditionOptionCall<LikeSearchOption> opLambda) {
        setDisplayName_LikeSearch(displayName, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * <pre>e.g. setDisplayName_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param displayName The value of displayName as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setDisplayName_LikeSearch(String displayName, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(displayName), xgetCValueDisplayName(), "DISPLAY_NAME", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setDisplayName_NotLikeSearch(String displayName, ConditionOptionCall<LikeSearchOption> opLambda) {
        setDisplayName_NotLikeSearch(displayName, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setDisplayName_NotLikeSearch(String displayName, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(displayName), xgetCValueDisplayName(), "DISPLAY_NAME", likeSearchOption);
    }

    protected void regDisplayName(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueDisplayName(), "DISPLAY_NAME"); }
    protected abstract ConditionValue xgetCValueDisplayName();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_Equal(String cronExpression) {
        doSetCronExpression_Equal(fRES(cronExpression));
    }

    protected void doSetCronExpression_Equal(String cronExpression) {
        regCronExpression(CK_EQ, cronExpression);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_NotEqual(String cronExpression) {
        doSetCronExpression_NotEqual(fRES(cronExpression));
    }

    protected void doSetCronExpression_NotEqual(String cronExpression) {
        regCronExpression(CK_NES, cronExpression);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_GreaterThan(String cronExpression) {
        regCronExpression(CK_GT, fRES(cronExpression));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_LessThan(String cronExpression) {
        regCronExpression(CK_LT, fRES(cronExpression));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_GreaterEqual(String cronExpression) {
        regCronExpression(CK_GE, fRES(cronExpression));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_LessEqual(String cronExpression) {
        regCronExpression(CK_LE, fRES(cronExpression));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpressionList The collection of cronExpression as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_InScope(Collection<String> cronExpressionList) {
        doSetCronExpression_InScope(cronExpressionList);
    }

    protected void doSetCronExpression_InScope(Collection<String> cronExpressionList) {
        regINS(CK_INS, cTL(cronExpressionList), xgetCValueCronExpression(), "CRON_EXPRESSION");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpressionList The collection of cronExpression as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setCronExpression_NotInScope(Collection<String> cronExpressionList) {
        doSetCronExpression_NotInScope(cronExpressionList);
    }

    protected void doSetCronExpression_NotInScope(Collection<String> cronExpressionList) {
        regINS(CK_NINS, cTL(cronExpressionList), xgetCValueCronExpression(), "CRON_EXPRESSION");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)} <br>
     * <pre>e.g. setCronExpression_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param cronExpression The value of cronExpression as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setCronExpression_LikeSearch(String cronExpression, ConditionOptionCall<LikeSearchOption> opLambda) {
        setCronExpression_LikeSearch(cronExpression, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)} <br>
     * <pre>e.g. setCronExpression_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param cronExpression The value of cronExpression as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setCronExpression_LikeSearch(String cronExpression, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(cronExpression), xgetCValueCronExpression(), "CRON_EXPRESSION", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setCronExpression_NotLikeSearch(String cronExpression, ConditionOptionCall<LikeSearchOption> opLambda) {
        setCronExpression_NotLikeSearch(cronExpression, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @param cronExpression The value of cronExpression as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setCronExpression_NotLikeSearch(String cronExpression, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(cronExpression), xgetCValueCronExpression(), "CRON_EXPRESSION", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     */
    public void setCronExpression_IsNull() { regCronExpression(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     */
    public void setCronExpression_IsNullOrEmpty() { regCronExpression(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     */
    public void setCronExpression_IsNotNull() { regCronExpression(CK_ISNN, DOBJ); }

    protected void regCronExpression(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueCronExpression(), "CRON_EXPRESSION"); }
    protected abstract ConditionValue xgetCValueCronExpression();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @param conditionType The value of conditionType as equal. (basically NotNull: error as default, or no condition as option)
     */
    protected void setConditionType_Equal(Integer conditionType) {
        doSetConditionType_Equal(conditionType);
    }

    /**
     * Equal(=). As ConditionType. And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別
     * @param cdef The instance of classification definition (as ENUM type). (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionType_Equal_AsConditionType(CDef.ConditionType cdef) {
        doSetConditionType_Equal(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * Equal(=). As None (0). And NullIgnored, OnlyOnceRegistered. <br>
     * 条件なし: 条件なし
     */
    public void setConditionType_Equal_None() {
        setConditionType_Equal_AsConditionType(CDef.ConditionType.None);
    }

    /**
     * Equal(=). As ReceivedPostReq (10). And NullIgnored, OnlyOnceRegistered. <br>
     * Postリクエスト受信時: Postリクエスト受信時
     */
    public void setConditionType_Equal_ReceivedPostReq() {
        setConditionType_Equal_AsConditionType(CDef.ConditionType.ReceivedPostReq);
    }

    /**
     * Equal(=). As UpdatedRss (20). And NullIgnored, OnlyOnceRegistered. <br>
     * RSS更新時: RSS更新時
     */
    public void setConditionType_Equal_UpdatedRss() {
        setConditionType_Equal_AsConditionType(CDef.ConditionType.UpdatedRss);
    }

    /**
     * Equal(=). As UpdatedChatworkSupport (30). And NullIgnored, OnlyOnceRegistered. <br>
     * Webサイト更新時（チャットワークサポート）: チャットワークサポートの更新時
     */
    public void setConditionType_Equal_UpdatedChatworkSupport() {
        setConditionType_Equal_AsConditionType(CDef.ConditionType.UpdatedChatworkSupport);
    }

    /**
     * Equal(=). As ExistOverdueTaskChatwork (40). And NullIgnored, OnlyOnceRegistered. <br>
     * 期限切れタスクあり（チャットワーク）: チャットワークに期限切れタスクあり
     */
    public void setConditionType_Equal_ExistOverdueTaskChatwork() {
        setConditionType_Equal_AsConditionType(CDef.ConditionType.ExistOverdueTaskChatwork);
    }

    /**
     * Equal(=). As ExistDueTaskChatwork (50). And NullIgnored, OnlyOnceRegistered. <br>
     * 本日までタスクあり（チャットワーク）: チャットワークに本日が期限のタスクあり
     */
    public void setConditionType_Equal_ExistDueTaskChatwork() {
        setConditionType_Equal_AsConditionType(CDef.ConditionType.ExistDueTaskChatwork);
    }

    protected void doSetConditionType_Equal(Integer conditionType) {
        regConditionType(CK_EQ, conditionType);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @param conditionType The value of conditionType as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    protected void setConditionType_NotEqual(Integer conditionType) {
        doSetConditionType_NotEqual(conditionType);
    }

    /**
     * NotEqual(&lt;&gt;). As ConditionType. And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別
     * @param cdef The instance of classification definition (as ENUM type). (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionType_NotEqual_AsConditionType(CDef.ConditionType cdef) {
        doSetConditionType_NotEqual(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * NotEqual(&lt;&gt;). As None (0). And NullIgnored, OnlyOnceRegistered. <br>
     * 条件なし: 条件なし
     */
    public void setConditionType_NotEqual_None() {
        setConditionType_NotEqual_AsConditionType(CDef.ConditionType.None);
    }

    /**
     * NotEqual(&lt;&gt;). As ReceivedPostReq (10). And NullIgnored, OnlyOnceRegistered. <br>
     * Postリクエスト受信時: Postリクエスト受信時
     */
    public void setConditionType_NotEqual_ReceivedPostReq() {
        setConditionType_NotEqual_AsConditionType(CDef.ConditionType.ReceivedPostReq);
    }

    /**
     * NotEqual(&lt;&gt;). As UpdatedRss (20). And NullIgnored, OnlyOnceRegistered. <br>
     * RSS更新時: RSS更新時
     */
    public void setConditionType_NotEqual_UpdatedRss() {
        setConditionType_NotEqual_AsConditionType(CDef.ConditionType.UpdatedRss);
    }

    /**
     * NotEqual(&lt;&gt;). As UpdatedChatworkSupport (30). And NullIgnored, OnlyOnceRegistered. <br>
     * Webサイト更新時（チャットワークサポート）: チャットワークサポートの更新時
     */
    public void setConditionType_NotEqual_UpdatedChatworkSupport() {
        setConditionType_NotEqual_AsConditionType(CDef.ConditionType.UpdatedChatworkSupport);
    }

    /**
     * NotEqual(&lt;&gt;). As ExistOverdueTaskChatwork (40). And NullIgnored, OnlyOnceRegistered. <br>
     * 期限切れタスクあり（チャットワーク）: チャットワークに期限切れタスクあり
     */
    public void setConditionType_NotEqual_ExistOverdueTaskChatwork() {
        setConditionType_NotEqual_AsConditionType(CDef.ConditionType.ExistOverdueTaskChatwork);
    }

    /**
     * NotEqual(&lt;&gt;). As ExistDueTaskChatwork (50). And NullIgnored, OnlyOnceRegistered. <br>
     * 本日までタスクあり（チャットワーク）: チャットワークに本日が期限のタスクあり
     */
    public void setConditionType_NotEqual_ExistDueTaskChatwork() {
        setConditionType_NotEqual_AsConditionType(CDef.ConditionType.ExistDueTaskChatwork);
    }

    protected void doSetConditionType_NotEqual(Integer conditionType) {
        regConditionType(CK_NES, conditionType);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @param conditionTypeList The collection of conditionType as inScope. (NullAllowed: if null (or empty), no condition)
     */
    protected void setConditionType_InScope(Collection<Integer> conditionTypeList) {
        doSetConditionType_InScope(conditionTypeList);
    }

    /**
     * InScope {in (1, 2)}. As ConditionType. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionType_InScope_AsConditionType(Collection<CDef.ConditionType> cdefList) {
        doSetConditionType_InScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetConditionType_InScope(Collection<Integer> conditionTypeList) {
        regINS(CK_INS, cTL(conditionTypeList), xgetCValueConditionType(), "CONDITION_TYPE");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @param conditionTypeList The collection of conditionType as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    protected void setConditionType_NotInScope(Collection<Integer> conditionTypeList) {
        doSetConditionType_NotInScope(conditionTypeList);
    }

    /**
     * NotInScope {not in (1, 2)}. As ConditionType. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType} <br>
     * 条件種別
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionType_NotInScope_AsConditionType(Collection<CDef.ConditionType> cdefList) {
        doSetConditionType_NotInScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetConditionType_NotInScope(Collection<Integer> conditionTypeList) {
        regINS(CK_NINS, cTL(conditionTypeList), xgetCValueConditionType(), "CONDITION_TYPE");
    }

    protected void regConditionType(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueConditionType(), "CONDITION_TYPE"); }
    protected abstract ConditionValue xgetCValueConditionType();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_Equal(String conditionValue) {
        doSetConditionValue_Equal(fRES(conditionValue));
    }

    protected void doSetConditionValue_Equal(String conditionValue) {
        regConditionValue(CK_EQ, conditionValue);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_NotEqual(String conditionValue) {
        doSetConditionValue_NotEqual(fRES(conditionValue));
    }

    protected void doSetConditionValue_NotEqual(String conditionValue) {
        regConditionValue(CK_NES, conditionValue);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_GreaterThan(String conditionValue) {
        regConditionValue(CK_GT, fRES(conditionValue));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_LessThan(String conditionValue) {
        regConditionValue(CK_LT, fRES(conditionValue));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_GreaterEqual(String conditionValue) {
        regConditionValue(CK_GE, fRES(conditionValue));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_LessEqual(String conditionValue) {
        regConditionValue(CK_LE, fRES(conditionValue));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValueList The collection of conditionValue as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_InScope(Collection<String> conditionValueList) {
        doSetConditionValue_InScope(conditionValueList);
    }

    protected void doSetConditionValue_InScope(Collection<String> conditionValueList) {
        regINS(CK_INS, cTL(conditionValueList), xgetCValueConditionValue(), "CONDITION_VALUE");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValueList The collection of conditionValue as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionValue_NotInScope(Collection<String> conditionValueList) {
        doSetConditionValue_NotInScope(conditionValueList);
    }

    protected void doSetConditionValue_NotInScope(Collection<String> conditionValueList) {
        regINS(CK_NINS, cTL(conditionValueList), xgetCValueConditionValue(), "CONDITION_VALUE");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)} <br>
     * <pre>e.g. setConditionValue_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param conditionValue The value of conditionValue as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setConditionValue_LikeSearch(String conditionValue, ConditionOptionCall<LikeSearchOption> opLambda) {
        setConditionValue_LikeSearch(conditionValue, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)} <br>
     * <pre>e.g. setConditionValue_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param conditionValue The value of conditionValue as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setConditionValue_LikeSearch(String conditionValue, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(conditionValue), xgetCValueConditionValue(), "CONDITION_VALUE", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setConditionValue_NotLikeSearch(String conditionValue, ConditionOptionCall<LikeSearchOption> opLambda) {
        setConditionValue_NotLikeSearch(conditionValue, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @param conditionValue The value of conditionValue as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setConditionValue_NotLikeSearch(String conditionValue, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(conditionValue), xgetCValueConditionValue(), "CONDITION_VALUE", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     */
    public void setConditionValue_IsNull() { regConditionValue(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     */
    public void setConditionValue_IsNullOrEmpty() { regConditionValue(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     */
    public void setConditionValue_IsNotNull() { regConditionValue(CK_ISNN, DOBJ); }

    protected void regConditionValue(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueConditionValue(), "CONDITION_VALUE"); }
    protected abstract ConditionValue xgetCValueConditionValue();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountId The value of conditionAccountId as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionAccountId_Equal(Long conditionAccountId) {
        doSetConditionAccountId_Equal(conditionAccountId);
    }

    protected void doSetConditionAccountId_Equal(Long conditionAccountId) {
        regConditionAccountId(CK_EQ, conditionAccountId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountId The value of conditionAccountId as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionAccountId_NotEqual(Long conditionAccountId) {
        doSetConditionAccountId_NotEqual(conditionAccountId);
    }

    protected void doSetConditionAccountId_NotEqual(Long conditionAccountId) {
        regConditionAccountId(CK_NES, conditionAccountId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountId The value of conditionAccountId as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionAccountId_GreaterThan(Long conditionAccountId) {
        regConditionAccountId(CK_GT, conditionAccountId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountId The value of conditionAccountId as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionAccountId_LessThan(Long conditionAccountId) {
        regConditionAccountId(CK_LT, conditionAccountId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountId The value of conditionAccountId as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionAccountId_GreaterEqual(Long conditionAccountId) {
        regConditionAccountId(CK_GE, conditionAccountId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountId The value of conditionAccountId as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setConditionAccountId_LessEqual(Long conditionAccountId) {
        regConditionAccountId(CK_LE, conditionAccountId);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param minNumber The min number of conditionAccountId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of conditionAccountId. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setConditionAccountId_RangeOf(Long minNumber, Long maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setConditionAccountId_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param minNumber The min number of conditionAccountId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of conditionAccountId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setConditionAccountId_RangeOf(Long minNumber, Long maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueConditionAccountId(), "CONDITION_ACCOUNT_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountIdList The collection of conditionAccountId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionAccountId_InScope(Collection<Long> conditionAccountIdList) {
        doSetConditionAccountId_InScope(conditionAccountIdList);
    }

    protected void doSetConditionAccountId_InScope(Collection<Long> conditionAccountIdList) {
        regINS(CK_INS, cTL(conditionAccountIdList), xgetCValueConditionAccountId(), "CONDITION_ACCOUNT_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param conditionAccountIdList The collection of conditionAccountId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setConditionAccountId_NotInScope(Collection<Long> conditionAccountIdList) {
        doSetConditionAccountId_NotInScope(conditionAccountIdList);
    }

    protected void doSetConditionAccountId_NotInScope(Collection<Long> conditionAccountIdList) {
        regINS(CK_NINS, cTL(conditionAccountIdList), xgetCValueConditionAccountId(), "CONDITION_ACCOUNT_ID");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     */
    public void setConditionAccountId_IsNull() { regConditionAccountId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     */
    public void setConditionAccountId_IsNotNull() { regConditionAccountId(CK_ISNN, DOBJ); }

    protected void regConditionAccountId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueConditionAccountId(), "CONDITION_ACCOUNT_ID"); }
    protected abstract ConditionValue xgetCValueConditionAccountId();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @param executionType The value of executionType as equal. (basically NotNull: error as default, or no condition as option)
     */
    protected void setExecutionType_Equal(Integer executionType) {
        doSetExecutionType_Equal(executionType);
    }

    /**
     * Equal(=). As ExecutionType. And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別
     * @param cdef The instance of classification definition (as ENUM type). (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionType_Equal_AsExecutionType(CDef.ExecutionType cdef) {
        doSetExecutionType_Equal(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * Equal(=). As NotifyChatwork (10). And NullIgnored, OnlyOnceRegistered. <br>
     * チャットへの通知（Chatwork）: チャットへの通知（Chatwork）
     */
    public void setExecutionType_Equal_NotifyChatwork() {
        setExecutionType_Equal_AsExecutionType(CDef.ExecutionType.NotifyChatwork);
    }

    /**
     * Equal(=). As NotifySlack (11). And NullIgnored, OnlyOnceRegistered. <br>
     * チャットへの通知（Slack）: チャットへの通知（Slack）
     */
    public void setExecutionType_Equal_NotifySlack() {
        setExecutionType_Equal_AsExecutionType(CDef.ExecutionType.NotifySlack);
    }

    protected void doSetExecutionType_Equal(Integer executionType) {
        regExecutionType(CK_EQ, executionType);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @param executionType The value of executionType as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    protected void setExecutionType_NotEqual(Integer executionType) {
        doSetExecutionType_NotEqual(executionType);
    }

    /**
     * NotEqual(&lt;&gt;). As ExecutionType. And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別
     * @param cdef The instance of classification definition (as ENUM type). (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionType_NotEqual_AsExecutionType(CDef.ExecutionType cdef) {
        doSetExecutionType_NotEqual(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * NotEqual(&lt;&gt;). As NotifyChatwork (10). And NullIgnored, OnlyOnceRegistered. <br>
     * チャットへの通知（Chatwork）: チャットへの通知（Chatwork）
     */
    public void setExecutionType_NotEqual_NotifyChatwork() {
        setExecutionType_NotEqual_AsExecutionType(CDef.ExecutionType.NotifyChatwork);
    }

    /**
     * NotEqual(&lt;&gt;). As NotifySlack (11). And NullIgnored, OnlyOnceRegistered. <br>
     * チャットへの通知（Slack）: チャットへの通知（Slack）
     */
    public void setExecutionType_NotEqual_NotifySlack() {
        setExecutionType_NotEqual_AsExecutionType(CDef.ExecutionType.NotifySlack);
    }

    protected void doSetExecutionType_NotEqual(Integer executionType) {
        regExecutionType(CK_NES, executionType);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @param executionTypeList The collection of executionType as inScope. (NullAllowed: if null (or empty), no condition)
     */
    protected void setExecutionType_InScope(Collection<Integer> executionTypeList) {
        doSetExecutionType_InScope(executionTypeList);
    }

    /**
     * InScope {in (1, 2)}. As ExecutionType. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionType_InScope_AsExecutionType(Collection<CDef.ExecutionType> cdefList) {
        doSetExecutionType_InScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetExecutionType_InScope(Collection<Integer> executionTypeList) {
        regINS(CK_INS, cTL(executionTypeList), xgetCValueExecutionType(), "EXECUTION_TYPE");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @param executionTypeList The collection of executionType as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    protected void setExecutionType_NotInScope(Collection<Integer> executionTypeList) {
        doSetExecutionType_NotInScope(executionTypeList);
    }

    /**
     * NotInScope {not in (1, 2)}. As ExecutionType. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType} <br>
     * 実行種別
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionType_NotInScope_AsExecutionType(Collection<CDef.ExecutionType> cdefList) {
        doSetExecutionType_NotInScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetExecutionType_NotInScope(Collection<Integer> executionTypeList) {
        regINS(CK_NINS, cTL(executionTypeList), xgetCValueExecutionType(), "EXECUTION_TYPE");
    }

    protected void regExecutionType(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueExecutionType(), "EXECUTION_TYPE"); }
    protected abstract ConditionValue xgetCValueExecutionType();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_Equal(String executionValue) {
        doSetExecutionValue_Equal(fRES(executionValue));
    }

    protected void doSetExecutionValue_Equal(String executionValue) {
        regExecutionValue(CK_EQ, executionValue);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_NotEqual(String executionValue) {
        doSetExecutionValue_NotEqual(fRES(executionValue));
    }

    protected void doSetExecutionValue_NotEqual(String executionValue) {
        regExecutionValue(CK_NES, executionValue);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_GreaterThan(String executionValue) {
        regExecutionValue(CK_GT, fRES(executionValue));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_LessThan(String executionValue) {
        regExecutionValue(CK_LT, fRES(executionValue));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_GreaterEqual(String executionValue) {
        regExecutionValue(CK_GE, fRES(executionValue));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_LessEqual(String executionValue) {
        regExecutionValue(CK_LE, fRES(executionValue));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValueList The collection of executionValue as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_InScope(Collection<String> executionValueList) {
        doSetExecutionValue_InScope(executionValueList);
    }

    protected void doSetExecutionValue_InScope(Collection<String> executionValueList) {
        regINS(CK_INS, cTL(executionValueList), xgetCValueExecutionValue(), "EXECUTION_VALUE");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValueList The collection of executionValue as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionValue_NotInScope(Collection<String> executionValueList) {
        doSetExecutionValue_NotInScope(executionValueList);
    }

    protected void doSetExecutionValue_NotInScope(Collection<String> executionValueList) {
        regINS(CK_NINS, cTL(executionValueList), xgetCValueExecutionValue(), "EXECUTION_VALUE");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)} <br>
     * <pre>e.g. setExecutionValue_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param executionValue The value of executionValue as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setExecutionValue_LikeSearch(String executionValue, ConditionOptionCall<LikeSearchOption> opLambda) {
        setExecutionValue_LikeSearch(executionValue, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)} <br>
     * <pre>e.g. setExecutionValue_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param executionValue The value of executionValue as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setExecutionValue_LikeSearch(String executionValue, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(executionValue), xgetCValueExecutionValue(), "EXECUTION_VALUE", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setExecutionValue_NotLikeSearch(String executionValue, ConditionOptionCall<LikeSearchOption> opLambda) {
        setExecutionValue_NotLikeSearch(executionValue, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @param executionValue The value of executionValue as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setExecutionValue_NotLikeSearch(String executionValue, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(executionValue), xgetCValueExecutionValue(), "EXECUTION_VALUE", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     */
    public void setExecutionValue_IsNull() { regExecutionValue(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     */
    public void setExecutionValue_IsNullOrEmpty() { regExecutionValue(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     */
    public void setExecutionValue_IsNotNull() { regExecutionValue(CK_ISNN, DOBJ); }

    protected void regExecutionValue(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueExecutionValue(), "EXECUTION_VALUE"); }
    protected abstract ConditionValue xgetCValueExecutionValue();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountId The value of executionAccountId as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionAccountId_Equal(Long executionAccountId) {
        doSetExecutionAccountId_Equal(executionAccountId);
    }

    protected void doSetExecutionAccountId_Equal(Long executionAccountId) {
        regExecutionAccountId(CK_EQ, executionAccountId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountId The value of executionAccountId as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionAccountId_NotEqual(Long executionAccountId) {
        doSetExecutionAccountId_NotEqual(executionAccountId);
    }

    protected void doSetExecutionAccountId_NotEqual(Long executionAccountId) {
        regExecutionAccountId(CK_NES, executionAccountId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountId The value of executionAccountId as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionAccountId_GreaterThan(Long executionAccountId) {
        regExecutionAccountId(CK_GT, executionAccountId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountId The value of executionAccountId as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionAccountId_LessThan(Long executionAccountId) {
        regExecutionAccountId(CK_LT, executionAccountId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountId The value of executionAccountId as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionAccountId_GreaterEqual(Long executionAccountId) {
        regExecutionAccountId(CK_GE, executionAccountId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountId The value of executionAccountId as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setExecutionAccountId_LessEqual(Long executionAccountId) {
        regExecutionAccountId(CK_LE, executionAccountId);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param minNumber The min number of executionAccountId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of executionAccountId. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setExecutionAccountId_RangeOf(Long minNumber, Long maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setExecutionAccountId_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param minNumber The min number of executionAccountId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of executionAccountId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setExecutionAccountId_RangeOf(Long minNumber, Long maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueExecutionAccountId(), "EXECUTION_ACCOUNT_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountIdList The collection of executionAccountId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionAccountId_InScope(Collection<Long> executionAccountIdList) {
        doSetExecutionAccountId_InScope(executionAccountIdList);
    }

    protected void doSetExecutionAccountId_InScope(Collection<Long> executionAccountIdList) {
        regINS(CK_INS, cTL(executionAccountIdList), xgetCValueExecutionAccountId(), "EXECUTION_ACCOUNT_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @param executionAccountIdList The collection of executionAccountId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setExecutionAccountId_NotInScope(Collection<Long> executionAccountIdList) {
        doSetExecutionAccountId_NotInScope(executionAccountIdList);
    }

    protected void doSetExecutionAccountId_NotInScope(Collection<Long> executionAccountIdList) {
        regINS(CK_NINS, cTL(executionAccountIdList), xgetCValueExecutionAccountId(), "EXECUTION_ACCOUNT_ID");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     */
    public void setExecutionAccountId_IsNull() { regExecutionAccountId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     */
    public void setExecutionAccountId_IsNotNull() { regExecutionAccountId(CK_ISNN, DOBJ); }

    protected void regExecutionAccountId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueExecutionAccountId(), "EXECUTION_ACCOUNT_ID"); }
    protected abstract ConditionValue xgetCValueExecutionAccountId();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_Equal(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_EQ,  createdAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_GreaterThan(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_GT,  createdAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_LessThan(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_LT,  createdAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_GreaterEqual(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_GE,  createdAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_LessEqual(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_LE, createdAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setCreatedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setCreatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setCreatedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setCreatedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setCreatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "CREATED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueCreatedAt(), nm, op);
    }

    protected void regCreatedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueCreatedAt(), "CREATED_AT"); }
    protected abstract ConditionValue xgetCValueCreatedAt();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_Equal(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_EQ,  updatedAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_GreaterThan(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_GT,  updatedAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_LessThan(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_LT,  updatedAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_GreaterEqual(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_GE,  updatedAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_LessEqual(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_LE, updatedAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setUpdatedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setUpdatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setUpdatedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setUpdatedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setUpdatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "UPDATED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueUpdatedAt(), nm, op);
    }

    protected void regUpdatedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueUpdatedAt(), "UPDATED_AT"); }
    protected abstract ConditionValue xgetCValueUpdatedAt();

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO = (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<TaskSettingsCB> scalar_Equal() {
        return xcreateSLCFunction(CK_EQ, TaskSettingsCB.class);
    }

    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO &lt;&gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<TaskSettingsCB> scalar_NotEqual() {
        return xcreateSLCFunction(CK_NES, TaskSettingsCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterThan. <br>
     * {where FOO &gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<TaskSettingsCB> scalar_GreaterThan() {
        return xcreateSLCFunction(CK_GT, TaskSettingsCB.class);
    }

    /**
     * Prepare ScalarCondition as lessThan. <br>
     * {where FOO &lt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<TaskSettingsCB> scalar_LessThan() {
        return xcreateSLCFunction(CK_LT, TaskSettingsCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterEqual. <br>
     * {where FOO &gt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<TaskSettingsCB> scalar_GreaterEqual() {
        return xcreateSLCFunction(CK_GE, TaskSettingsCB.class);
    }

    /**
     * Prepare ScalarCondition as lessEqual. <br>
     * {where FOO &lt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().<span style="color: #CC4747">scalar_LessEqual()</span>.max(new SubQuery&lt;TaskSettingsCB&gt;() {
     *     public void query(TaskSettingsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<TaskSettingsCB> scalar_LessEqual() {
        return xcreateSLCFunction(CK_LE, TaskSettingsCB.class);
    }

    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xscalarCondition(String fn, SubQuery<CB> sq, String rd, HpSLCCustomized<CB> cs, ScalarConditionOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = xcreateScalarConditionCB(); sq.query((CB)cb);
        String pp = keepScalarCondition(cb.query()); // for saving query-value
        cs.setPartitionByCBean((CB)xcreateScalarConditionPartitionByCB()); // for using partition-by
        registerScalarCondition(fn, cb.query(), pp, rd, cs, op);
    }
    public abstract String keepScalarCondition(TaskSettingsCQ sq);

    protected TaskSettingsCB xcreateScalarConditionCB() {
        TaskSettingsCB cb = newMyCB(); cb.xsetupForScalarCondition(this); return cb;
    }

    protected TaskSettingsCB xcreateScalarConditionPartitionByCB() {
        TaskSettingsCB cb = newMyCB(); cb.xsetupForScalarConditionPartitionBy(this); return cb;
    }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public void xsmyselfDerive(String fn, SubQuery<TaskSettingsCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepSpecifyMyselfDerived(cb.query()); String pk = "TASK_SETTING_ID";
        registerSpecifyMyselfDerived(fn, cb.query(), pk, pk, pp, "myselfDerived", al, op);
    }
    public abstract String keepSpecifyMyselfDerived(TaskSettingsCQ sq);

    /**
     * Prepare for (Query)MyselfDerived (correlated sub-query).
     * @return The object to set up a function for myself table. (NotNull)
     */
    public HpQDRFunction<TaskSettingsCB> myselfDerived() {
        return xcreateQDRFunctionMyselfDerived(TaskSettingsCB.class);
    }
    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xqderiveMyselfDerived(String fn, SubQuery<CB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForDerivedReferrer(this); sq.query((CB)cb);
        String pk = "TASK_SETTING_ID";
        String sqpp = keepQueryMyselfDerived(cb.query()); // for saving query-value.
        String prpp = keepQueryMyselfDerivedParameter(vl);
        registerQueryMyselfDerived(fn, cb.query(), pk, pk, sqpp, "myselfDerived", rd, vl, prpp, op);
    }
    public abstract String keepQueryMyselfDerived(TaskSettingsCQ sq);
    public abstract String keepQueryMyselfDerivedParameter(Object vl);

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    /**
     * Prepare for MyselfExists (correlated sub-query).
     * @param subCBLambda The implementation of sub-query. (NotNull)
     */
    public void myselfExists(SubQuery<TaskSettingsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForMyselfExists(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepMyselfExists(cb.query());
        registerMyselfExists(cb.query(), pp);
    }
    public abstract String keepMyselfExists(TaskSettingsCQ sq);

    // ===================================================================================
    //                                                                        Manual Order
    //                                                                        ============
    /**
     * Order along manual ordering information.
     * <pre>
     * cb.query().addOrderBy_Birthdate_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when BIRTHDATE &gt;= '2000/01/01' then 0</span>
     * <span style="color: #3F7E5E">//     else 1</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     *
     * cb.query().addOrderBy_MemberStatusCode_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Withdrawal);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Formalized);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Provisional);
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'WDL' then 0</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'FML' then 1</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'PRV' then 2</span>
     * <span style="color: #3F7E5E">//     else 3</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     * </pre>
     * <p>This function with Union is unsupported!</p>
     * <p>The order values are bound (treated as bind parameter).</p>
     * @param opLambda The callback for option of manual-order containing order values. (NotNull)
     */
    public void withManualOrder(ManualOrderOptionCall opLambda) { // is user public!
        xdoWithManualOrder(cMOO(opLambda));
    }

    // ===================================================================================
    //                                                                    Small Adjustment
    //                                                                    ================
    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    protected TaskSettingsCB newMyCB() {
        return new TaskSettingsCB();
    }
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xabUDT() { return Date.class.getName(); }
    protected String xabCQ() { return TaskSettingsCQ.class.getName(); }
    protected String xabLSO() { return LikeSearchOption.class.getName(); }
    protected String xabSLCS() { return HpSLCSetupper.class.getName(); }
    protected String xabSCP() { return SubQuery.class.getName(); }
}
