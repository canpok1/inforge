package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.ciq;

import java.util.Map;
import org.dbflute.cbean.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.ConditionOption;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The condition-query for in-line of ACCOUNTS.
 * @author DBFlute(AutoGenerator)
 */
public class AccountsCIQ extends AbstractBsAccountsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected BsAccountsCQ _myCQ;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AccountsCIQ(ConditionQuery referrerQuery, SqlClause sqlClause
                        , String aliasName, int nestLevel, BsAccountsCQ myCQ) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
        _myCQ = myCQ;
        _foreignPropertyName = _myCQ.xgetForeignPropertyName(); // accept foreign property name
        _relationPath = _myCQ.xgetRelationPath(); // accept relation path
        _inline = true;
    }

    // ===================================================================================
    //                                                             Override about Register
    //                                                             =======================
    protected void reflectRelationOnUnionQuery(ConditionQuery bq, ConditionQuery uq)
    { throw new IllegalConditionBeanOperationException("InlineView cannot use Union: " + bq + " : " + uq); }

    @Override
    protected void setupConditionValueAndRegisterWhereClause(ConditionKey k, Object v, ConditionValue cv, String col)
    { regIQ(k, v, cv, col); }

    @Override
    protected void setupConditionValueAndRegisterWhereClause(ConditionKey k, Object v, ConditionValue cv, String col, ConditionOption op)
    { regIQ(k, v, cv, col, op); }

    @Override
    protected void registerWhereClause(String wc)
    { registerInlineWhereClause(wc); }

    @Override
    protected boolean isInScopeRelationSuppressLocalAliasName() {
        if (_onClause) { throw new IllegalConditionBeanOperationException("InScopeRelation on OnClause is unsupported."); }
        return true;
    }

    // ===================================================================================
    //                                                                Override about Query
    //                                                                ====================
    protected ConditionValue xgetCValueAccountId() { return _myCQ.xdfgetAccountId(); }
    public String keepAccountId_ExistsReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("ExistsReferrer"); return null; }
    public String keepAccountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("ExistsReferrer"); return null; }
    public String keepAccountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("NotExistsReferrer"); return null; }
    public String keepAccountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("NotExistsReferrer"); return null; }
    public String keepAccountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("(Specify)DerivedReferrer"); return null; }
    public String keepAccountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("(Specify)DerivedReferrer"); return null; }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("(Query)DerivedReferrer"); return null; }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdListParameter(Object vl)
    { throwIICBOE("(Query)DerivedReferrer"); return null; }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq)
    { throwIICBOE("(Query)DerivedReferrer"); return null; }
    public String keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdListParameter(Object vl)
    { throwIICBOE("(Query)DerivedReferrer"); return null; }
    protected ConditionValue xgetCValueDisplayName() { return _myCQ.xdfgetDisplayName(); }
    protected ConditionValue xgetCValueServiceType() { return _myCQ.xdfgetServiceType(); }
    protected ConditionValue xgetCValueUserName() { return _myCQ.xdfgetUserName(); }
    protected ConditionValue xgetCValueApiToken() { return _myCQ.xdfgetApiToken(); }
    protected ConditionValue xgetCValueIconEmoji() { return _myCQ.xdfgetIconEmoji(); }
    protected ConditionValue xgetCValueCreatedAt() { return _myCQ.xdfgetCreatedAt(); }
    protected ConditionValue xgetCValueUpdatedAt() { return _myCQ.xdfgetUpdatedAt(); }
    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String pp) { return null; }
    public String keepScalarCondition(AccountsCQ sq)
    { throwIICBOE("ScalarCondition"); return null; }
    public String keepSpecifyMyselfDerived(AccountsCQ sq)
    { throwIICBOE("(Specify)MyselfDerived"); return null;}
    public String keepQueryMyselfDerived(AccountsCQ sq)
    { throwIICBOE("(Query)MyselfDerived"); return null;}
    public String keepQueryMyselfDerivedParameter(Object vl)
    { throwIICBOE("(Query)MyselfDerived"); return null;}
    public String keepMyselfExists(AccountsCQ sq)
    { throwIICBOE("MyselfExists"); return null;}

    protected void throwIICBOE(String name)
    { throw new IllegalConditionBeanOperationException(name + " at InlineView is unsupported."); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xinCB() { return AccountsCB.class.getName(); }
    protected String xinCQ() { return AccountsCQ.class.getName(); }
}
