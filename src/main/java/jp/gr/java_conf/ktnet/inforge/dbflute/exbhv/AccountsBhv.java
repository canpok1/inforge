package jp.gr.java_conf.ktnet.inforge.dbflute.exbhv;

import jp.gr.java_conf.ktnet.inforge.dbflute.bsbhv.BsAccountsBhv;

/**
 * The behavior of ACCOUNTS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@org.springframework.stereotype.Component("accountsBhv")
public class AccountsBhv extends BsAccountsBhv {
}
