package jp.gr.java_conf.ktnet.inforge.dbflute.exbhv;

import jp.gr.java_conf.ktnet.inforge.dbflute.bsbhv.BsArticlesBhv;

/**
 * The behavior of ARTICLES.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@org.springframework.stereotype.Component("articlesBhv")
public class ArticlesBhv extends BsArticlesBhv {
}
