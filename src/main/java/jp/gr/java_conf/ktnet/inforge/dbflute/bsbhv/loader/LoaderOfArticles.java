package jp.gr.java_conf.ktnet.inforge.dbflute.bsbhv.loader;

import java.util.List;

import org.dbflute.bhv.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The referrer loader of ARTICLES as TABLE. <br>
 * <pre>
 * [primary key]
 *     ARTICLE_ID
 *
 * [column]
 *     ARTICLE_ID, TITLE, URL, TASK_SETTING_ID, NOTIFIED_AT, CREATED_AT, UPDATED_AT
 *
 * [sequence]
 *     
 *
 * [identity]
 *     ARTICLE_ID
 *
 * [version-no]
 *     
 *
 * [foreign table]
 *     TASK_SETTINGS
 *
 * [referrer table]
 *     
 *
 * [foreign property]
 *     taskSettings
 *
 * [referrer property]
 *     
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public class LoaderOfArticles {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected List<Articles> _selectedList;
    protected BehaviorSelector _selector;
    protected ArticlesBhv _myBhv; // lazy-loaded

    // ===================================================================================
    //                                                                   Ready for Loading
    //                                                                   =================
    public LoaderOfArticles ready(List<Articles> selectedList, BehaviorSelector selector)
    { _selectedList = selectedList; _selector = selector; return this; }

    protected ArticlesBhv myBhv()
    { if (_myBhv != null) { return _myBhv; } else { _myBhv = _selector.select(ArticlesBhv.class); return _myBhv; } }

    // ===================================================================================
    //                                                                    Pull out Foreign
    //                                                                    ================
    protected LoaderOfTaskSettings _foreignTaskSettingsLoader;
    public LoaderOfTaskSettings pulloutTaskSettings() {
        if (_foreignTaskSettingsLoader == null)
        { _foreignTaskSettingsLoader = new LoaderOfTaskSettings().ready(myBhv().pulloutTaskSettings(_selectedList), _selector); }
        return _foreignTaskSettingsLoader;
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    public List<Articles> getSelectedList() { return _selectedList; }
    public BehaviorSelector getSelector() { return _selector; }
}
