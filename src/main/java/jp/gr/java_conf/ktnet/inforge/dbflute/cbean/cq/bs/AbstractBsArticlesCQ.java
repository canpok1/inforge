package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs;

import java.util.*;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.ordering.*;
import org.dbflute.cbean.scoping.*;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.dbmeta.DBMetaProvider;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The abstract condition-query of ARTICLES.
 * @author DBFlute(AutoGenerator)
 */
public abstract class AbstractBsArticlesCQ extends AbstractConditionQuery {

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AbstractBsArticlesCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    @Override
    protected DBMetaProvider xgetDBMetaProvider() {
        return DBMetaInstanceHandler.getProvider();
    }

    public String asTableDbName() {
        return "ARTICLES";
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleId The value of articleId as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setArticleId_Equal(Long articleId) {
        doSetArticleId_Equal(articleId);
    }

    protected void doSetArticleId_Equal(Long articleId) {
        regArticleId(CK_EQ, articleId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleId The value of articleId as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setArticleId_NotEqual(Long articleId) {
        doSetArticleId_NotEqual(articleId);
    }

    protected void doSetArticleId_NotEqual(Long articleId) {
        regArticleId(CK_NES, articleId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleId The value of articleId as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setArticleId_GreaterThan(Long articleId) {
        regArticleId(CK_GT, articleId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleId The value of articleId as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setArticleId_LessThan(Long articleId) {
        regArticleId(CK_LT, articleId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleId The value of articleId as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setArticleId_GreaterEqual(Long articleId) {
        regArticleId(CK_GE, articleId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleId The value of articleId as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setArticleId_LessEqual(Long articleId) {
        regArticleId(CK_LE, articleId);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param minNumber The min number of articleId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of articleId. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setArticleId_RangeOf(Long minNumber, Long maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setArticleId_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param minNumber The min number of articleId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of articleId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setArticleId_RangeOf(Long minNumber, Long maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueArticleId(), "ARTICLE_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleIdList The collection of articleId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setArticleId_InScope(Collection<Long> articleIdList) {
        doSetArticleId_InScope(articleIdList);
    }

    protected void doSetArticleId_InScope(Collection<Long> articleIdList) {
        regINS(CK_INS, cTL(articleIdList), xgetCValueArticleId(), "ARTICLE_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param articleIdList The collection of articleId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setArticleId_NotInScope(Collection<Long> articleIdList) {
        doSetArticleId_NotInScope(articleIdList);
    }

    protected void doSetArticleId_NotInScope(Collection<Long> articleIdList) {
        regINS(CK_NINS, cTL(articleIdList), xgetCValueArticleId(), "ARTICLE_ID");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     */
    public void setArticleId_IsNull() { regArticleId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     */
    public void setArticleId_IsNotNull() { regArticleId(CK_ISNN, DOBJ); }

    protected void regArticleId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueArticleId(), "ARTICLE_ID"); }
    protected abstract ConditionValue xgetCValueArticleId();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_Equal(String title) {
        doSetTitle_Equal(fRES(title));
    }

    protected void doSetTitle_Equal(String title) {
        regTitle(CK_EQ, title);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_NotEqual(String title) {
        doSetTitle_NotEqual(fRES(title));
    }

    protected void doSetTitle_NotEqual(String title) {
        regTitle(CK_NES, title);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_GreaterThan(String title) {
        regTitle(CK_GT, fRES(title));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_LessThan(String title) {
        regTitle(CK_LT, fRES(title));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_GreaterEqual(String title) {
        regTitle(CK_GE, fRES(title));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_LessEqual(String title) {
        regTitle(CK_LE, fRES(title));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param titleList The collection of title as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_InScope(Collection<String> titleList) {
        doSetTitle_InScope(titleList);
    }

    protected void doSetTitle_InScope(Collection<String> titleList) {
        regINS(CK_INS, cTL(titleList), xgetCValueTitle(), "TITLE");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param titleList The collection of title as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setTitle_NotInScope(Collection<String> titleList) {
        doSetTitle_NotInScope(titleList);
    }

    protected void doSetTitle_NotInScope(Collection<String> titleList) {
        regINS(CK_NINS, cTL(titleList), xgetCValueTitle(), "TITLE");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)} <br>
     * <pre>e.g. setTitle_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param title The value of title as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setTitle_LikeSearch(String title, ConditionOptionCall<LikeSearchOption> opLambda) {
        setTitle_LikeSearch(title, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)} <br>
     * <pre>e.g. setTitle_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param title The value of title as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setTitle_LikeSearch(String title, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(title), xgetCValueTitle(), "TITLE", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setTitle_NotLikeSearch(String title, ConditionOptionCall<LikeSearchOption> opLambda) {
        setTitle_NotLikeSearch(title, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * TITLE: {NotNull, CLOB(2147483647)}
     * @param title The value of title as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setTitle_NotLikeSearch(String title, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(title), xgetCValueTitle(), "TITLE", likeSearchOption);
    }

    protected void regTitle(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueTitle(), "TITLE"); }
    protected abstract ConditionValue xgetCValueTitle();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_Equal(String url) {
        doSetUrl_Equal(fRES(url));
    }

    protected void doSetUrl_Equal(String url) {
        regUrl(CK_EQ, url);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_NotEqual(String url) {
        doSetUrl_NotEqual(fRES(url));
    }

    protected void doSetUrl_NotEqual(String url) {
        regUrl(CK_NES, url);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_GreaterThan(String url) {
        regUrl(CK_GT, fRES(url));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_LessThan(String url) {
        regUrl(CK_LT, fRES(url));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_GreaterEqual(String url) {
        regUrl(CK_GE, fRES(url));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_LessEqual(String url) {
        regUrl(CK_LE, fRES(url));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param urlList The collection of url as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_InScope(Collection<String> urlList) {
        doSetUrl_InScope(urlList);
    }

    protected void doSetUrl_InScope(Collection<String> urlList) {
        regINS(CK_INS, cTL(urlList), xgetCValueUrl(), "URL");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param urlList The collection of url as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setUrl_NotInScope(Collection<String> urlList) {
        doSetUrl_NotInScope(urlList);
    }

    protected void doSetUrl_NotInScope(Collection<String> urlList) {
        regINS(CK_NINS, cTL(urlList), xgetCValueUrl(), "URL");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * URL: {CLOB(2147483647)} <br>
     * <pre>e.g. setUrl_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param url The value of url as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setUrl_LikeSearch(String url, ConditionOptionCall<LikeSearchOption> opLambda) {
        setUrl_LikeSearch(url, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * URL: {CLOB(2147483647)} <br>
     * <pre>e.g. setUrl_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param url The value of url as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setUrl_LikeSearch(String url, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(url), xgetCValueUrl(), "URL", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setUrl_NotLikeSearch(String url, ConditionOptionCall<LikeSearchOption> opLambda) {
        setUrl_NotLikeSearch(url, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * URL: {CLOB(2147483647)}
     * @param url The value of url as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setUrl_NotLikeSearch(String url, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(url), xgetCValueUrl(), "URL", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     */
    public void setUrl_IsNull() { regUrl(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     */
    public void setUrl_IsNullOrEmpty() { regUrl(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * URL: {CLOB(2147483647)}
     */
    public void setUrl_IsNotNull() { regUrl(CK_ISNN, DOBJ); }

    protected void regUrl(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueUrl(), "URL"); }
    protected abstract ConditionValue xgetCValueUrl();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingId The value of taskSettingId as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_Equal(Long taskSettingId) {
        doSetTaskSettingId_Equal(taskSettingId);
    }

    protected void doSetTaskSettingId_Equal(Long taskSettingId) {
        regTaskSettingId(CK_EQ, taskSettingId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingId The value of taskSettingId as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_NotEqual(Long taskSettingId) {
        doSetTaskSettingId_NotEqual(taskSettingId);
    }

    protected void doSetTaskSettingId_NotEqual(Long taskSettingId) {
        regTaskSettingId(CK_NES, taskSettingId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingId The value of taskSettingId as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_GreaterThan(Long taskSettingId) {
        regTaskSettingId(CK_GT, taskSettingId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingId The value of taskSettingId as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_LessThan(Long taskSettingId) {
        regTaskSettingId(CK_LT, taskSettingId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingId The value of taskSettingId as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_GreaterEqual(Long taskSettingId) {
        regTaskSettingId(CK_GE, taskSettingId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingId The value of taskSettingId as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setTaskSettingId_LessEqual(Long taskSettingId) {
        regTaskSettingId(CK_LE, taskSettingId);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param minNumber The min number of taskSettingId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of taskSettingId. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setTaskSettingId_RangeOf(Long minNumber, Long maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setTaskSettingId_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param minNumber The min number of taskSettingId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of taskSettingId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setTaskSettingId_RangeOf(Long minNumber, Long maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueTaskSettingId(), "TASK_SETTING_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingIdList The collection of taskSettingId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setTaskSettingId_InScope(Collection<Long> taskSettingIdList) {
        doSetTaskSettingId_InScope(taskSettingIdList);
    }

    protected void doSetTaskSettingId_InScope(Collection<Long> taskSettingIdList) {
        regINS(CK_INS, cTL(taskSettingIdList), xgetCValueTaskSettingId(), "TASK_SETTING_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @param taskSettingIdList The collection of taskSettingId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setTaskSettingId_NotInScope(Collection<Long> taskSettingIdList) {
        doSetTaskSettingId_NotInScope(taskSettingIdList);
    }

    protected void doSetTaskSettingId_NotInScope(Collection<Long> taskSettingIdList) {
        regINS(CK_NINS, cTL(taskSettingIdList), xgetCValueTaskSettingId(), "TASK_SETTING_ID");
    }

    protected void regTaskSettingId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueTaskSettingId(), "TASK_SETTING_ID"); }
    protected abstract ConditionValue xgetCValueTaskSettingId();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @param notifiedAt The value of notifiedAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setNotifiedAt_Equal(java.time.LocalDateTime notifiedAt) {
        regNotifiedAt(CK_EQ,  notifiedAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @param notifiedAt The value of notifiedAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setNotifiedAt_GreaterThan(java.time.LocalDateTime notifiedAt) {
        regNotifiedAt(CK_GT,  notifiedAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @param notifiedAt The value of notifiedAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setNotifiedAt_LessThan(java.time.LocalDateTime notifiedAt) {
        regNotifiedAt(CK_LT,  notifiedAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @param notifiedAt The value of notifiedAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setNotifiedAt_GreaterEqual(java.time.LocalDateTime notifiedAt) {
        regNotifiedAt(CK_GE,  notifiedAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @param notifiedAt The value of notifiedAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setNotifiedAt_LessEqual(java.time.LocalDateTime notifiedAt) {
        regNotifiedAt(CK_LE, notifiedAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * <pre>e.g. setNotifiedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of notifiedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of notifiedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setNotifiedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setNotifiedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * <pre>e.g. setNotifiedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of notifiedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of notifiedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setNotifiedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "NOTIFIED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueNotifiedAt(), nm, op);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     */
    public void setNotifiedAt_IsNull() { regNotifiedAt(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     */
    public void setNotifiedAt_IsNotNull() { regNotifiedAt(CK_ISNN, DOBJ); }

    protected void regNotifiedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueNotifiedAt(), "NOTIFIED_AT"); }
    protected abstract ConditionValue xgetCValueNotifiedAt();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_Equal(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_EQ,  createdAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_GreaterThan(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_GT,  createdAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_LessThan(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_LT,  createdAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_GreaterEqual(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_GE,  createdAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_LessEqual(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_LE, createdAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setCreatedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setCreatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setCreatedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setCreatedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setCreatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "CREATED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueCreatedAt(), nm, op);
    }

    protected void regCreatedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueCreatedAt(), "CREATED_AT"); }
    protected abstract ConditionValue xgetCValueCreatedAt();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_Equal(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_EQ,  updatedAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_GreaterThan(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_GT,  updatedAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_LessThan(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_LT,  updatedAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_GreaterEqual(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_GE,  updatedAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_LessEqual(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_LE, updatedAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setUpdatedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setUpdatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setUpdatedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setUpdatedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setUpdatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "UPDATED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueUpdatedAt(), nm, op);
    }

    protected void regUpdatedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueUpdatedAt(), "UPDATED_AT"); }
    protected abstract ConditionValue xgetCValueUpdatedAt();

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO = (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<ArticlesCB> scalar_Equal() {
        return xcreateSLCFunction(CK_EQ, ArticlesCB.class);
    }

    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO &lt;&gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<ArticlesCB> scalar_NotEqual() {
        return xcreateSLCFunction(CK_NES, ArticlesCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterThan. <br>
     * {where FOO &gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<ArticlesCB> scalar_GreaterThan() {
        return xcreateSLCFunction(CK_GT, ArticlesCB.class);
    }

    /**
     * Prepare ScalarCondition as lessThan. <br>
     * {where FOO &lt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<ArticlesCB> scalar_LessThan() {
        return xcreateSLCFunction(CK_LT, ArticlesCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterEqual. <br>
     * {where FOO &gt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<ArticlesCB> scalar_GreaterEqual() {
        return xcreateSLCFunction(CK_GE, ArticlesCB.class);
    }

    /**
     * Prepare ScalarCondition as lessEqual. <br>
     * {where FOO &lt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().<span style="color: #CC4747">scalar_LessEqual()</span>.max(new SubQuery&lt;ArticlesCB&gt;() {
     *     public void query(ArticlesCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<ArticlesCB> scalar_LessEqual() {
        return xcreateSLCFunction(CK_LE, ArticlesCB.class);
    }

    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xscalarCondition(String fn, SubQuery<CB> sq, String rd, HpSLCCustomized<CB> cs, ScalarConditionOption op) {
        assertObjectNotNull("subQuery", sq);
        ArticlesCB cb = xcreateScalarConditionCB(); sq.query((CB)cb);
        String pp = keepScalarCondition(cb.query()); // for saving query-value
        cs.setPartitionByCBean((CB)xcreateScalarConditionPartitionByCB()); // for using partition-by
        registerScalarCondition(fn, cb.query(), pp, rd, cs, op);
    }
    public abstract String keepScalarCondition(ArticlesCQ sq);

    protected ArticlesCB xcreateScalarConditionCB() {
        ArticlesCB cb = newMyCB(); cb.xsetupForScalarCondition(this); return cb;
    }

    protected ArticlesCB xcreateScalarConditionPartitionByCB() {
        ArticlesCB cb = newMyCB(); cb.xsetupForScalarConditionPartitionBy(this); return cb;
    }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public void xsmyselfDerive(String fn, SubQuery<ArticlesCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepSpecifyMyselfDerived(cb.query()); String pk = "ARTICLE_ID";
        registerSpecifyMyselfDerived(fn, cb.query(), pk, pk, pp, "myselfDerived", al, op);
    }
    public abstract String keepSpecifyMyselfDerived(ArticlesCQ sq);

    /**
     * Prepare for (Query)MyselfDerived (correlated sub-query).
     * @return The object to set up a function for myself table. (NotNull)
     */
    public HpQDRFunction<ArticlesCB> myselfDerived() {
        return xcreateQDRFunctionMyselfDerived(ArticlesCB.class);
    }
    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xqderiveMyselfDerived(String fn, SubQuery<CB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForDerivedReferrer(this); sq.query((CB)cb);
        String pk = "ARTICLE_ID";
        String sqpp = keepQueryMyselfDerived(cb.query()); // for saving query-value.
        String prpp = keepQueryMyselfDerivedParameter(vl);
        registerQueryMyselfDerived(fn, cb.query(), pk, pk, sqpp, "myselfDerived", rd, vl, prpp, op);
    }
    public abstract String keepQueryMyselfDerived(ArticlesCQ sq);
    public abstract String keepQueryMyselfDerivedParameter(Object vl);

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    /**
     * Prepare for MyselfExists (correlated sub-query).
     * @param subCBLambda The implementation of sub-query. (NotNull)
     */
    public void myselfExists(SubQuery<ArticlesCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        ArticlesCB cb = new ArticlesCB(); cb.xsetupForMyselfExists(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepMyselfExists(cb.query());
        registerMyselfExists(cb.query(), pp);
    }
    public abstract String keepMyselfExists(ArticlesCQ sq);

    // ===================================================================================
    //                                                                        Manual Order
    //                                                                        ============
    /**
     * Order along manual ordering information.
     * <pre>
     * cb.query().addOrderBy_Birthdate_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when BIRTHDATE &gt;= '2000/01/01' then 0</span>
     * <span style="color: #3F7E5E">//     else 1</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     *
     * cb.query().addOrderBy_MemberStatusCode_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Withdrawal);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Formalized);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Provisional);
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'WDL' then 0</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'FML' then 1</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'PRV' then 2</span>
     * <span style="color: #3F7E5E">//     else 3</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     * </pre>
     * <p>This function with Union is unsupported!</p>
     * <p>The order values are bound (treated as bind parameter).</p>
     * @param opLambda The callback for option of manual-order containing order values. (NotNull)
     */
    public void withManualOrder(ManualOrderOptionCall opLambda) { // is user public!
        xdoWithManualOrder(cMOO(opLambda));
    }

    // ===================================================================================
    //                                                                    Small Adjustment
    //                                                                    ================
    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    protected ArticlesCB newMyCB() {
        return new ArticlesCB();
    }
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xabUDT() { return Date.class.getName(); }
    protected String xabCQ() { return ArticlesCQ.class.getName(); }
    protected String xabLSO() { return LikeSearchOption.class.getName(); }
    protected String xabSLCS() { return HpSLCSetupper.class.getName(); }
    protected String xabSCP() { return SubQuery.class.getName(); }
}
