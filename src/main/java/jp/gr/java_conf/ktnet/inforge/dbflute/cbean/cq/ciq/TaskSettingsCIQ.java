package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.ciq;

import java.util.Map;
import org.dbflute.cbean.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.ConditionOption;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The condition-query for in-line of TASK_SETTINGS.
 * @author DBFlute(AutoGenerator)
 */
public class TaskSettingsCIQ extends AbstractBsTaskSettingsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected BsTaskSettingsCQ _myCQ;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public TaskSettingsCIQ(ConditionQuery referrerQuery, SqlClause sqlClause
                        , String aliasName, int nestLevel, BsTaskSettingsCQ myCQ) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
        _myCQ = myCQ;
        _foreignPropertyName = _myCQ.xgetForeignPropertyName(); // accept foreign property name
        _relationPath = _myCQ.xgetRelationPath(); // accept relation path
        _inline = true;
    }

    // ===================================================================================
    //                                                             Override about Register
    //                                                             =======================
    protected void reflectRelationOnUnionQuery(ConditionQuery bq, ConditionQuery uq)
    { throw new IllegalConditionBeanOperationException("InlineView cannot use Union: " + bq + " : " + uq); }

    @Override
    protected void setupConditionValueAndRegisterWhereClause(ConditionKey k, Object v, ConditionValue cv, String col)
    { regIQ(k, v, cv, col); }

    @Override
    protected void setupConditionValueAndRegisterWhereClause(ConditionKey k, Object v, ConditionValue cv, String col, ConditionOption op)
    { regIQ(k, v, cv, col, op); }

    @Override
    protected void registerWhereClause(String wc)
    { registerInlineWhereClause(wc); }

    @Override
    protected boolean isInScopeRelationSuppressLocalAliasName() {
        if (_onClause) { throw new IllegalConditionBeanOperationException("InScopeRelation on OnClause is unsupported."); }
        return true;
    }

    // ===================================================================================
    //                                                                Override about Query
    //                                                                ====================
    protected ConditionValue xgetCValueTaskSettingId() { return _myCQ.xdfgetTaskSettingId(); }
    public String keepTaskSettingId_ExistsReferrer_ArticlesList(ArticlesCQ sq)
    { throwIICBOE("ExistsReferrer"); return null; }
    public String keepTaskSettingId_NotExistsReferrer_ArticlesList(ArticlesCQ sq)
    { throwIICBOE("NotExistsReferrer"); return null; }
    public String keepTaskSettingId_SpecifyDerivedReferrer_ArticlesList(ArticlesCQ sq)
    { throwIICBOE("(Specify)DerivedReferrer"); return null; }
    public String keepTaskSettingId_QueryDerivedReferrer_ArticlesList(ArticlesCQ sq)
    { throwIICBOE("(Query)DerivedReferrer"); return null; }
    public String keepTaskSettingId_QueryDerivedReferrer_ArticlesListParameter(Object vl)
    { throwIICBOE("(Query)DerivedReferrer"); return null; }
    protected ConditionValue xgetCValueDisplayName() { return _myCQ.xdfgetDisplayName(); }
    protected ConditionValue xgetCValueCronExpression() { return _myCQ.xdfgetCronExpression(); }
    protected ConditionValue xgetCValueConditionType() { return _myCQ.xdfgetConditionType(); }
    protected ConditionValue xgetCValueConditionValue() { return _myCQ.xdfgetConditionValue(); }
    protected ConditionValue xgetCValueConditionAccountId() { return _myCQ.xdfgetConditionAccountId(); }
    protected ConditionValue xgetCValueExecutionType() { return _myCQ.xdfgetExecutionType(); }
    protected ConditionValue xgetCValueExecutionValue() { return _myCQ.xdfgetExecutionValue(); }
    protected ConditionValue xgetCValueExecutionAccountId() { return _myCQ.xdfgetExecutionAccountId(); }
    protected ConditionValue xgetCValueCreatedAt() { return _myCQ.xdfgetCreatedAt(); }
    protected ConditionValue xgetCValueUpdatedAt() { return _myCQ.xdfgetUpdatedAt(); }
    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String pp) { return null; }
    public String keepScalarCondition(TaskSettingsCQ sq)
    { throwIICBOE("ScalarCondition"); return null; }
    public String keepSpecifyMyselfDerived(TaskSettingsCQ sq)
    { throwIICBOE("(Specify)MyselfDerived"); return null;}
    public String keepQueryMyselfDerived(TaskSettingsCQ sq)
    { throwIICBOE("(Query)MyselfDerived"); return null;}
    public String keepQueryMyselfDerivedParameter(Object vl)
    { throwIICBOE("(Query)MyselfDerived"); return null;}
    public String keepMyselfExists(TaskSettingsCQ sq)
    { throwIICBOE("MyselfExists"); return null;}

    protected void throwIICBOE(String name)
    { throw new IllegalConditionBeanOperationException(name + " at InlineView is unsupported."); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xinCB() { return TaskSettingsCB.class.getName(); }
    protected String xinCQ() { return TaskSettingsCQ.class.getName(); }
}
