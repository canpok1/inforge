package jp.gr.java_conf.ktnet.inforge.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.dbflute.Entity;
import org.dbflute.optional.OptionalEntity;
import org.dbflute.dbmeta.AbstractDBMeta;
import org.dbflute.dbmeta.info.*;
import org.dbflute.dbmeta.name.*;
import org.dbflute.dbmeta.property.PropertyGateway;
import org.dbflute.dbway.DBDef;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The DB meta of TASK_SETTINGS. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class TaskSettingsDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final TaskSettingsDbm _instance = new TaskSettingsDbm();
    private TaskSettingsDbm() {}
    public static TaskSettingsDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public String getProjectName() { return DBCurrent.getInstance().projectName(); }
    public String getProjectPrefix() { return DBCurrent.getInstance().projectPrefix(); }
    public String getGenerationGapBasePrefix() { return DBCurrent.getInstance().generationGapBasePrefix(); }
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    // -----------------------------------------------------
    //                                       Column Property
    //                                       ---------------
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    { xsetupEpg(); }
    protected void xsetupEpg() {
        setupEpg(_epgMap, et -> ((TaskSettings)et).getTaskSettingId(), (et, vl) -> ((TaskSettings)et).setTaskSettingId(ctl(vl)), "taskSettingId");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getDisplayName(), (et, vl) -> ((TaskSettings)et).setDisplayName((String)vl), "displayName");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getCronExpression(), (et, vl) -> ((TaskSettings)et).setCronExpression((String)vl), "cronExpression");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getConditionType(), (et, vl) -> {
            CDef.ConditionType cls = (CDef.ConditionType)gcls(et, columnConditionType(), vl);
            if (cls != null) {
                ((TaskSettings)et).setConditionTypeAsConditionType(cls);
            } else {
                ((TaskSettings)et).mynativeMappingConditionType(ctn(vl, Integer.class));
            }
        }, "conditionType");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getConditionValue(), (et, vl) -> ((TaskSettings)et).setConditionValue((String)vl), "conditionValue");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getConditionAccountId(), (et, vl) -> ((TaskSettings)et).setConditionAccountId(ctl(vl)), "conditionAccountId");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getExecutionType(), (et, vl) -> {
            CDef.ExecutionType cls = (CDef.ExecutionType)gcls(et, columnExecutionType(), vl);
            if (cls != null) {
                ((TaskSettings)et).setExecutionTypeAsExecutionType(cls);
            } else {
                ((TaskSettings)et).mynativeMappingExecutionType(ctn(vl, Integer.class));
            }
        }, "executionType");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getExecutionValue(), (et, vl) -> ((TaskSettings)et).setExecutionValue((String)vl), "executionValue");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getExecutionAccountId(), (et, vl) -> ((TaskSettings)et).setExecutionAccountId(ctl(vl)), "executionAccountId");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getCreatedAt(), (et, vl) -> ((TaskSettings)et).setCreatedAt(ctldt(vl)), "createdAt");
        setupEpg(_epgMap, et -> ((TaskSettings)et).getUpdatedAt(), (et, vl) -> ((TaskSettings)et).setUpdatedAt(ctldt(vl)), "updatedAt");
    }
    public PropertyGateway findPropertyGateway(String prop)
    { return doFindEpg(_epgMap, prop); }

    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------
    protected final Map<String, PropertyGateway> _efpgMap = newHashMap();
    { xsetupEfpg(); }
    @SuppressWarnings("unchecked")
    protected void xsetupEfpg() {
        setupEfpg(_efpgMap, et -> ((TaskSettings)et).getAccountsByConditionAccountId(), (et, vl) -> ((TaskSettings)et).setAccountsByConditionAccountId((OptionalEntity<Accounts>)vl), "accountsByConditionAccountId");
        setupEfpg(_efpgMap, et -> ((TaskSettings)et).getAccountsByExecutionAccountId(), (et, vl) -> ((TaskSettings)et).setAccountsByExecutionAccountId((OptionalEntity<Accounts>)vl), "accountsByExecutionAccountId");
    }
    public PropertyGateway findForeignPropertyGateway(String prop)
    { return doFindEfpg(_efpgMap, prop); }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "TASK_SETTINGS";
    protected final String _tableDispName = "TASK_SETTINGS";
    protected final String _tablePropertyName = "taskSettings";
    protected final TableSqlName _tableSqlName = new TableSqlName("INFORGE.TASK_SETTINGS", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTableDispName() { return _tableDispName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnTaskSettingId = cci("TASK_SETTING_ID", "TASK_SETTING_ID", null, null, Long.class, "taskSettingId", null, true, true, true, "BIGINT", 19, 0, "NEXT VALUE FOR INFORGE.SYSTEM_SEQUENCE_7E534576_00FF_4318_A46E_41B98DF45C65", false, null, null, null, "articlesList", null, false);
    protected final ColumnInfo _columnDisplayName = cci("DISPLAY_NAME", "DISPLAY_NAME", null, null, String.class, "displayName", null, false, false, true, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnCronExpression = cci("CRON_EXPRESSION", "CRON_EXPRESSION", null, null, String.class, "cronExpression", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnConditionType = cci("CONDITION_TYPE", "CONDITION_TYPE", null, null, Integer.class, "conditionType", null, false, false, true, "SMALLINT", 5, 0, null, false, null, null, null, null, CDef.DefMeta.ConditionType, false);
    protected final ColumnInfo _columnConditionValue = cci("CONDITION_VALUE", "CONDITION_VALUE", null, null, String.class, "conditionValue", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnConditionAccountId = cci("CONDITION_ACCOUNT_ID", "CONDITION_ACCOUNT_ID", null, null, Long.class, "conditionAccountId", null, false, false, false, "BIGINT", 19, 0, null, false, null, null, "accountsByConditionAccountId", null, null, false);
    protected final ColumnInfo _columnExecutionType = cci("EXECUTION_TYPE", "EXECUTION_TYPE", null, null, Integer.class, "executionType", null, false, false, true, "SMALLINT", 5, 0, null, false, null, null, null, null, CDef.DefMeta.ExecutionType, false);
    protected final ColumnInfo _columnExecutionValue = cci("EXECUTION_VALUE", "EXECUTION_VALUE", null, null, String.class, "executionValue", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnExecutionAccountId = cci("EXECUTION_ACCOUNT_ID", "EXECUTION_ACCOUNT_ID", null, null, Long.class, "executionAccountId", null, false, false, false, "BIGINT", 19, 0, null, false, null, null, "accountsByExecutionAccountId", null, null, false);
    protected final ColumnInfo _columnCreatedAt = cci("CREATED_AT", "CREATED_AT", null, null, java.time.LocalDateTime.class, "createdAt", null, false, false, true, "TIMESTAMP", 23, 10, "CURRENT_TIMESTAMP()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnUpdatedAt = cci("UPDATED_AT", "UPDATED_AT", null, null, java.time.LocalDateTime.class, "updatedAt", null, false, false, true, "TIMESTAMP", 23, 10, "CURRENT_TIMESTAMP()", false, null, null, null, null, null, false);

    /**
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnTaskSettingId() { return _columnTaskSettingId; }
    /**
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnDisplayName() { return _columnDisplayName; }
    /**
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnCronExpression() { return _columnCronExpression; }
    /**
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnConditionType() { return _columnConditionType; }
    /**
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnConditionValue() { return _columnConditionValue; }
    /**
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnConditionAccountId() { return _columnConditionAccountId; }
    /**
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnExecutionType() { return _columnExecutionType; }
    /**
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnExecutionValue() { return _columnExecutionValue; }
    /**
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnExecutionAccountId() { return _columnExecutionAccountId; }
    /**
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnCreatedAt() { return _columnCreatedAt; }
    /**
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUpdatedAt() { return _columnUpdatedAt; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnTaskSettingId());
        ls.add(columnDisplayName());
        ls.add(columnCronExpression());
        ls.add(columnConditionType());
        ls.add(columnConditionValue());
        ls.add(columnConditionAccountId());
        ls.add(columnExecutionType());
        ls.add(columnExecutionValue());
        ls.add(columnExecutionAccountId());
        ls.add(columnCreatedAt());
        ls.add(columnUpdatedAt());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnTaskSettingId()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // cannot cache because it uses related DB meta instance while booting
    // (instead, cached by super's collection)
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------
    /**
     * ACCOUNTS by my CONDITION_ACCOUNT_ID, named 'accountsByConditionAccountId'.
     * @return The information object of foreign property. (NotNull)
     */
    public ForeignInfo foreignAccountsByConditionAccountId() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnConditionAccountId(), AccountsDbm.getInstance().columnAccountId());
        return cfi("CONSTRAINT_98", "accountsByConditionAccountId", this, AccountsDbm.getInstance(), mp, 0, org.dbflute.optional.OptionalEntity.class, false, false, false, false, null, null, false, "taskSettingsByConditionAccountIdList", false);
    }
    /**
     * ACCOUNTS by my EXECUTION_ACCOUNT_ID, named 'accountsByExecutionAccountId'.
     * @return The information object of foreign property. (NotNull)
     */
    public ForeignInfo foreignAccountsByExecutionAccountId() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnExecutionAccountId(), AccountsDbm.getInstance().columnAccountId());
        return cfi("CONSTRAINT_985", "accountsByExecutionAccountId", this, AccountsDbm.getInstance(), mp, 1, org.dbflute.optional.OptionalEntity.class, false, false, false, false, null, null, false, "taskSettingsByExecutionAccountIdList", false);
    }

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------
    /**
     * ARTICLES by TASK_SETTING_ID, named 'articlesList'.
     * @return The information object of referrer property. (NotNull)
     */
    public ReferrerInfo referrerArticlesList() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnTaskSettingId(), ArticlesDbm.getInstance().columnTaskSettingId());
        return cri("CONSTRAINT_E5", "articlesList", this, ArticlesDbm.getInstance(), mp, false, "taskSettings");
    }

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============
    public boolean hasIdentity() { return true; }

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.exentity.TaskSettings"; }
    public String getConditionBeanTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.cbean.TaskSettingsCB"; }
    public String getBehaviorTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.TaskSettingsBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<TaskSettings> getEntityType() { return TaskSettings.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public TaskSettings newEntity() { return new TaskSettings(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptPrimaryKeyMap((TaskSettings)et, mp); }
    public void acceptAllColumnMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptAllColumnMap((TaskSettings)et, mp); }
    public Map<String, Object> extractPrimaryKeyMap(Entity et) { return doExtractPrimaryKeyMap(et); }
    public Map<String, Object> extractAllColumnMap(Entity et) { return doExtractAllColumnMap(et); }
}
