package jp.gr.java_conf.ktnet.inforge.dbflute.exbhv;

import jp.gr.java_conf.ktnet.inforge.dbflute.bsbhv.BsTaskSettingsBhv;

/**
 * The behavior of TASK_SETTINGS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
@org.springframework.stereotype.Component("taskSettingsBhv")
public class TaskSettingsBhv extends BsTaskSettingsBhv {
}
