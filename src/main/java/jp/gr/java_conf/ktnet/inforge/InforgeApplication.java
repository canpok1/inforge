package jp.gr.java_conf.ktnet.inforge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class InforgeApplication {

  public static void main(String[] args) {
    SpringApplication.run(InforgeApplication.class, args);
  }
}
