package jp.gr.java_conf.ktnet.inforge.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.dbflute.Entity;
import org.dbflute.optional.OptionalEntity;
import org.dbflute.dbmeta.AbstractDBMeta;
import org.dbflute.dbmeta.info.*;
import org.dbflute.dbmeta.name.*;
import org.dbflute.dbmeta.property.PropertyGateway;
import org.dbflute.dbway.DBDef;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.*;

/**
 * The DB meta of ARTICLES. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class ArticlesDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final ArticlesDbm _instance = new ArticlesDbm();
    private ArticlesDbm() {}
    public static ArticlesDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public String getProjectName() { return DBCurrent.getInstance().projectName(); }
    public String getProjectPrefix() { return DBCurrent.getInstance().projectPrefix(); }
    public String getGenerationGapBasePrefix() { return DBCurrent.getInstance().generationGapBasePrefix(); }
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    // -----------------------------------------------------
    //                                       Column Property
    //                                       ---------------
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    { xsetupEpg(); }
    protected void xsetupEpg() {
        setupEpg(_epgMap, et -> ((Articles)et).getArticleId(), (et, vl) -> ((Articles)et).setArticleId(ctl(vl)), "articleId");
        setupEpg(_epgMap, et -> ((Articles)et).getTitle(), (et, vl) -> ((Articles)et).setTitle((String)vl), "title");
        setupEpg(_epgMap, et -> ((Articles)et).getUrl(), (et, vl) -> ((Articles)et).setUrl((String)vl), "url");
        setupEpg(_epgMap, et -> ((Articles)et).getTaskSettingId(), (et, vl) -> ((Articles)et).setTaskSettingId(ctl(vl)), "taskSettingId");
        setupEpg(_epgMap, et -> ((Articles)et).getNotifiedAt(), (et, vl) -> ((Articles)et).setNotifiedAt(ctldt(vl)), "notifiedAt");
        setupEpg(_epgMap, et -> ((Articles)et).getCreatedAt(), (et, vl) -> ((Articles)et).setCreatedAt(ctldt(vl)), "createdAt");
        setupEpg(_epgMap, et -> ((Articles)et).getUpdatedAt(), (et, vl) -> ((Articles)et).setUpdatedAt(ctldt(vl)), "updatedAt");
    }
    public PropertyGateway findPropertyGateway(String prop)
    { return doFindEpg(_epgMap, prop); }

    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------
    protected final Map<String, PropertyGateway> _efpgMap = newHashMap();
    { xsetupEfpg(); }
    @SuppressWarnings("unchecked")
    protected void xsetupEfpg() {
        setupEfpg(_efpgMap, et -> ((Articles)et).getTaskSettings(), (et, vl) -> ((Articles)et).setTaskSettings((OptionalEntity<TaskSettings>)vl), "taskSettings");
    }
    public PropertyGateway findForeignPropertyGateway(String prop)
    { return doFindEfpg(_efpgMap, prop); }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "ARTICLES";
    protected final String _tableDispName = "ARTICLES";
    protected final String _tablePropertyName = "articles";
    protected final TableSqlName _tableSqlName = new TableSqlName("INFORGE.ARTICLES", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTableDispName() { return _tableDispName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnArticleId = cci("ARTICLE_ID", "ARTICLE_ID", null, null, Long.class, "articleId", null, true, true, true, "BIGINT", 19, 0, "NEXT VALUE FOR INFORGE.SYSTEM_SEQUENCE_686113E1_D7F6_4B3A_9156_088C052CF7B4", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnTitle = cci("TITLE", "TITLE", null, null, String.class, "title", null, false, false, true, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnUrl = cci("URL", "URL", null, null, String.class, "url", null, false, false, false, "CLOB", 2147483647, 0, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnTaskSettingId = cci("TASK_SETTING_ID", "TASK_SETTING_ID", null, null, Long.class, "taskSettingId", null, false, false, true, "BIGINT", 19, 0, null, false, null, null, "taskSettings", null, null, false);
    protected final ColumnInfo _columnNotifiedAt = cci("NOTIFIED_AT", "NOTIFIED_AT", null, null, java.time.LocalDateTime.class, "notifiedAt", null, false, false, false, "TIMESTAMP", 23, 10, null, false, null, null, null, null, null, false);
    protected final ColumnInfo _columnCreatedAt = cci("CREATED_AT", "CREATED_AT", null, null, java.time.LocalDateTime.class, "createdAt", null, false, false, true, "TIMESTAMP", 23, 10, "CURRENT_TIMESTAMP()", false, null, null, null, null, null, false);
    protected final ColumnInfo _columnUpdatedAt = cci("UPDATED_AT", "UPDATED_AT", null, null, java.time.LocalDateTime.class, "updatedAt", null, false, false, true, "TIMESTAMP", 23, 10, "CURRENT_TIMESTAMP()", false, null, null, null, null, null, false);

    /**
     * ARTICLE_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnArticleId() { return _columnArticleId; }
    /**
     * TITLE: {NotNull, CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnTitle() { return _columnTitle; }
    /**
     * URL: {CLOB(2147483647)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUrl() { return _columnUrl; }
    /**
     * TASK_SETTING_ID: {IX, NotNull, BIGINT(19), FK to TASK_SETTINGS}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnTaskSettingId() { return _columnTaskSettingId; }
    /**
     * NOTIFIED_AT: {TIMESTAMP(23, 10)}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnNotifiedAt() { return _columnNotifiedAt; }
    /**
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnCreatedAt() { return _columnCreatedAt; }
    /**
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return The information object of specified column. (NotNull)
     */
    public ColumnInfo columnUpdatedAt() { return _columnUpdatedAt; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnArticleId());
        ls.add(columnTitle());
        ls.add(columnUrl());
        ls.add(columnTaskSettingId());
        ls.add(columnNotifiedAt());
        ls.add(columnCreatedAt());
        ls.add(columnUpdatedAt());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnArticleId()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // cannot cache because it uses related DB meta instance while booting
    // (instead, cached by super's collection)
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------
    /**
     * TASK_SETTINGS by my TASK_SETTING_ID, named 'taskSettings'.
     * @return The information object of foreign property. (NotNull)
     */
    public ForeignInfo foreignTaskSettings() {
        Map<ColumnInfo, ColumnInfo> mp = newLinkedHashMap(columnTaskSettingId(), TaskSettingsDbm.getInstance().columnTaskSettingId());
        return cfi("CONSTRAINT_E5", "taskSettings", this, TaskSettingsDbm.getInstance(), mp, 0, org.dbflute.optional.OptionalEntity.class, false, false, false, false, null, null, false, "articlesList", false);
    }

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============
    public boolean hasIdentity() { return true; }

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Articles"; }
    public String getConditionBeanTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.cbean.ArticlesCB"; }
    public String getBehaviorTypeName() { return "jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.ArticlesBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<Articles> getEntityType() { return Articles.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public Articles newEntity() { return new Articles(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptPrimaryKeyMap((Articles)et, mp); }
    public void acceptAllColumnMap(Entity et, Map<String, ? extends Object> mp)
    { doAcceptAllColumnMap((Articles)et, mp); }
    public Map<String, Object> extractPrimaryKeyMap(Entity et) { return doExtractPrimaryKeyMap(et); }
    public Map<String, Object> extractAllColumnMap(Entity et) { return doExtractAllColumnMap(et); }
}
