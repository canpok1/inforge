package jp.gr.java_conf.ktnet.inforge.service;

import jp.gr.java_conf.ktnet.inforge.component.ChatworkClient;
import jp.gr.java_conf.ktnet.inforge.component.ChatworkSupportParser;
import jp.gr.java_conf.ktnet.inforge.component.NotifyMessageMaker;
import jp.gr.java_conf.ktnet.inforge.component.SlackClient;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.AccountsBhv;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.ArticlesBhv;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Accounts;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Articles;
import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.TaskSettings;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import org.dbflute.optional.OptionalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * タスク実行を行うクラスです.
 * @author tanabe
 *
 */
@Service
@Slf4j
public class TaskExecuteService {

  @Autowired
  private ChatworkSupportParser chatworkSupportParser;
  
  @Autowired
  private AccountsBhv accountsBhv;
  
  @Autowired
  private ArticlesBhv articlesBhv;
  
  @Autowired
  private SlackClient slackClient;
  
  @Autowired
  private ChatworkClient chatworkClient;
  
  @Autowired
  private NotifyMessageMaker notifyMessageMaker;
    
  
  /**
   * タスクを実行します.
   * @param taskSetting タスク設定.
   * @exception Exception 例外発生.
   */
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
  public void execute(@NonNull final TaskSettings taskSetting) throws Exception {
    if (taskSetting.isConditionTypeUpdatedChatworkSupport()) {
      // 新着記事取得
      List<Articles> newArticles = fetchNewChatworkSupportArticle(taskSetting);
      log.info("定期実行[{}] 新着記事 {}件", taskSetting.getDisplayName(), newArticles.size());
      newArticles.stream()
          .forEach(
            article -> {
              log.info("定期実行[{}] 新着記事[{}]", taskSetting.getDisplayName(), article.getTitle());
            }
        );
      if (newArticles.isEmpty()) {
        return;
      }
      
      // 通知
      notify(taskSetting, newArticles);
      newArticles.stream()
          .forEach(
            article -> {
              article.setNotifiedAt(LocalDateTime.now());
            }
        );
      articlesBhv.batchUpdate(newArticles);
    }
  }

  /**
   * チャットワークサポートの新着記事を取得します.
   * @param taskSetting タスク設定.
   * @return 通知が必要な記事.
   * @throws IOException 記事取得に失敗した場合.
   * @throws URISyntaxException URIが不正な場合.
   */
  private List<Articles> fetchNewChatworkSupportArticle(
      @NonNull final TaskSettings taskSetting
  ) throws IOException, URISyntaxException {
    
    // 記事取得
    List<ChatworkSupportParser.TitleUrlPair> titleUrlPairList
        = chatworkSupportParser.fetchArticle(taskSetting.getConditionValue());
    if (titleUrlPairList.isEmpty()) {
      return new ArrayList<>();
    }

    // 保存済みかつ通知済みの記事を抽出
    val existArticles = articlesBhv.selectList(
        cb -> {
          val titleList = titleUrlPairList.stream()
              .map(titleUrlPair -> titleUrlPair.getTitle())
              .collect(Collectors.toList());

          cb.query().setTitle_InScope(titleList);
        }
    );
        
    List<Articles> notifiedArticles = existArticles.stream()
        .filter(
          article -> {
            return article.getNotifiedAt() != null;
          }
        )
        .collect(Collectors.toList());
    
    // 新着記事もしくは未通知だけに絞り込む
    List<ChatworkSupportParser.TitleUrlPair> newTitleUrlPairList = titleUrlPairList.stream()
        .filter(
          titleUrlPair -> {
            return notifiedArticles.stream()
                .allMatch(
                  existArticle -> {
                    return !existArticle.getTitle().equals(titleUrlPair.getTitle());
                  }
                );
          }
        )
        .collect(Collectors.toList());
    
    // 未登録の新着記事を保存
    articlesBhv.batchInsert(
        newTitleUrlPairList.stream()
            .filter( // 未登録のものに絞り込み
              titleUrlPair -> {
                return existArticles.stream()
                    .allMatch(
                      existArticle -> {
                        return !existArticle.getTitle().equals(titleUrlPair.getTitle());
                      }
                    );
              }
            )
            .map(
              newTitleUrlPair -> {
                Articles article = new Articles();
                article.setTitle(newTitleUrlPair.getTitle());
                article.setUrl(newTitleUrlPair.getUrl());
                article.setTaskSettingId(taskSetting.getTaskSettingId());
                return article;
              }
            )
            .collect(Collectors.toList())
    );
    
    return articlesBhv.selectList(
        cb -> {
          val titleList = titleUrlPairList.stream()
              .map(
                titleUrlPair -> {
                  return titleUrlPair.getTitle();
                }
              )
              .collect(Collectors.toList());
          
          cb.query().setTitle_InScope(titleList);
          cb.query().setNotifiedAt_IsNull();
        }
    );
  }
  
  /**
   * 通知
   * @param setting タスク設定.
   * @param newArticles 新着記事.
   */
  private void notify(
      @NonNull final TaskSettings setting,
      @NonNull final List<Articles> newArticles
  ) throws URISyntaxException {
    OptionalEntity<Accounts> accountOpt = accountsBhv.selectByPK(setting.getExecutionAccountId());
   
    if (setting.isExecutionTypeNotifyChatwork()) {
      notifyMessageMaker.makeForChatwork("【新着】" + setting.getDisplayName(), newArticles).stream()
          .forEach(
            message -> {
              chatworkClient.postMessage(message, setting.getExecutionValue(), accountOpt.get());
            }
      );
      return;
    }
    if (setting.isExecutionTypeNotifySlack()) {
      notifyMessageMaker.makeForSlack("【新着】" + setting.getDisplayName(), newArticles).stream()
          .forEach(
            message -> {
              slackClient.postMessage(message, setting.getExecutionValue(), accountOpt.get());
            }
      );
      return;
    }
    
    log.warn("通知先が非対応のため通知をスキップ [executeType={}][{}]", setting.getExecutionType(), setting);
  }
  
}
