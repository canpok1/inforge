package jp.gr.java_conf.ktnet.inforge.service.api;

import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.AccountsBhv;
import jp.gr.java_conf.ktnet.inforge.dbflute.exbhv.TaskSettingsBhv;
import jp.gr.java_conf.ktnet.inforge.entity.api.TaskSetting;
import jp.gr.java_conf.ktnet.inforge.entity.api.TaskSetting.Account;
import jp.gr.java_conf.ktnet.inforge.entity.api.TaskSetting.Condition;
import jp.gr.java_conf.ktnet.inforge.entity.api.TaskSetting.Execution;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class TaskSettingService {

  @Autowired
  private TaskSettingsBhv taskSettingsBhv;
  
  @Autowired
  private AccountsBhv accountsBhv;
  
  /**
   * すべてのタスク設定を取得します.
   * @return タスク設定.
   */
  public GetAllResponse getAll() {
    GetAllResponse response = new GetAllResponse();
    
    taskSettingsBhv.selectList(cb -> { }).stream()
        .forEach(
          taskSetting -> {
            val accountBuilder = Account.builder();
            accountsBhv.selectByPK(taskSetting.getExecutionAccountId())
                .ifPresent(
                  storedAccount -> {
                    accountBuilder
                      .name(storedAccount.getUserName())
                      .iconEmoji(storedAccount.getIconEmoji());
                  }
              );
            
            response.taskSettings.add(
                TaskSetting.builder()
                    .cronExpression(taskSetting.getCronExpression())
                    .condition(Condition.builder()
                        .type(taskSetting.getConditionType().toString())
                        .url(taskSetting.getConditionValue())
                        .build()
                      )
                    .execution(Execution.builder()
                        .type(taskSetting.getExecutionType().toString())
                        .roomId(taskSetting.getExecutionValue())
                        .channelName(taskSetting.getExecutionValue())
                        .account(accountBuilder.build())
                        .build()
                      )
                    .memo(taskSetting.getDisplayName())
                    .build()
            );
          }
    );
    
    return response;
  }
  
  @Data
  public static class GetAllResponse {
    private List<TaskSetting> taskSettings = new ArrayList<>();
  }
}
