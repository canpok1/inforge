package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.nss;

import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.TaskSettingsCQ;

/**
 * The nest select set-upper of TASK_SETTINGS.
 * @author DBFlute(AutoGenerator)
 */
public class TaskSettingsNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected final TaskSettingsCQ _query;
    public TaskSettingsNss(TaskSettingsCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============
    /**
     * With nested relation columns to select clause. <br>
     * ACCOUNTS by my CONDITION_ACCOUNT_ID, named 'accountsByConditionAccountId'.
     */
    public void withAccountsByConditionAccountId() {
        _query.xdoNss(() -> _query.queryAccountsByConditionAccountId());
    }
    /**
     * With nested relation columns to select clause. <br>
     * ACCOUNTS by my EXECUTION_ACCOUNT_ID, named 'accountsByExecutionAccountId'.
     */
    public void withAccountsByExecutionAccountId() {
        _query.xdoNss(() -> _query.queryAccountsByExecutionAccountId());
    }
}
