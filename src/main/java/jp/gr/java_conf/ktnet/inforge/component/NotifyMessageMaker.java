package jp.gr.java_conf.ktnet.inforge.component;

import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Articles;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class NotifyMessageMaker {

  /**
   * チャットワーク向けメッセージを組み立てます.
   * @param title タイトル.
   * @param newArticles 新着記事.
   * @return メッセージ.
   */
  public List<String> makeForChatwork(
      @NonNull final String title,
      @NonNull final List<Articles> newArticles) {
    StringBuilder messageBuilder = new StringBuilder();
    newArticles.stream()
        .forEach(
          article -> {
            if (messageBuilder.length() > 0) {
              messageBuilder.append("[hr]");
            }
            messageBuilder.append(article.getTitle());
            messageBuilder.append("\n");
            messageBuilder.append(article.getUrl());
          }
      );
    messageBuilder.insert(0, "[info][title]" + title + "[/title]");
    messageBuilder.append("[/info]");
    
    ArrayList<String> messages = new ArrayList<>();
    messages.add(messageBuilder.toString());
    return messages;
  }
  
  /**
   * Slack向けメッセージを組み立てます.
   * @param title タイトル.
   * @param newArticles 新着記事.
   * @return メッセージ.
   */
  public List<String> makeForSlack(
      @NonNull final String title,
      @NonNull final List<Articles> newArticles) {
    ArrayList<String> messages = new ArrayList<>();
    StringBuilder messageBuilder = new StringBuilder();
    int maxSize = 10;
    
    int storedCount = 0;
    for (Articles article : newArticles) {
      if (messageBuilder.length() > 0) {
        messageBuilder.append("\n");
      }
      messageBuilder.append(article.getTitle());
      messageBuilder.append("\n");
      messageBuilder.append(article.getUrl());
      storedCount++;
      
      if (storedCount > maxSize) {
        messages.add(title + "\n>>> " + messageBuilder.toString());
        
        messageBuilder.delete(0, messageBuilder.length());
        storedCount = 0;
      }
    }
    if (messageBuilder.length() > 0) {
      messages.add(title + "\n>>> " + messageBuilder.toString());
    }
    
    return messages;
  }
}
