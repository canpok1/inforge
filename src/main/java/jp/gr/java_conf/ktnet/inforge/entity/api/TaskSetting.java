package jp.gr.java_conf.ktnet.inforge.entity.api;

import lombok.Builder;
import lombok.Data;

/**
 * タスク設定.
 * @author tanabe
 *
 */
@Data
@Builder
public class TaskSetting {

  /**
   * 定期実行設定.
   */
  private String cronExpression;
  
  /**
   * 条件設定.
   */
  private Condition condition;
  
  /**
   * 実行設定.
   */
  private Execution execution;
  
  /**
   * 備考.
   */
  private String memo;
  
  @Data
  @Builder
  public static class Condition {
    private String type;
    private String url;
  }
  
  @Data
  @Builder
  public static class Execution {
    private String type;
    private String roomId;
    private String channelName;
    private Account account;
  }
  
  @Data
  @Builder
  public static class Account {
    private String name;
    private String iconEmoji;
  }
}
