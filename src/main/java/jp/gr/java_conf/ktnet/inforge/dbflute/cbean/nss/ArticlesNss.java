package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.nss;

import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.ArticlesCQ;

/**
 * The nest select set-upper of ARTICLES.
 * @author DBFlute(AutoGenerator)
 */
public class ArticlesNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected final ArticlesCQ _query;
    public ArticlesNss(ArticlesCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============
    /**
     * With nested relation columns to select clause. <br>
     * TASK_SETTINGS by my TASK_SETTING_ID, named 'taskSettings'.
     * @return The set-upper of more nested relation. {...with[nested-relation].with[more-nested-relation]} (NotNull)
     */
    public TaskSettingsNss withTaskSettings() {
        _query.xdoNss(() -> _query.queryTaskSettings());
        return new TaskSettingsNss(_query.queryTaskSettings());
    }
}
