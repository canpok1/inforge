package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs;

import java.util.*;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.ckey.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.ordering.*;
import org.dbflute.cbean.scoping.*;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.dbmeta.DBMetaProvider;
import jp.gr.java_conf.ktnet.inforge.dbflute.allcommon.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The abstract condition-query of ACCOUNTS.
 * @author DBFlute(AutoGenerator)
 */
public abstract class AbstractBsAccountsCQ extends AbstractConditionQuery {

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AbstractBsAccountsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                             DB Meta
    //                                                                             =======
    @Override
    protected DBMetaProvider xgetDBMetaProvider() {
        return DBMetaInstanceHandler.getProvider();
    }

    public String asTableDbName() {
        return "ACCOUNTS";
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountId The value of accountId as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setAccountId_Equal(Long accountId) {
        doSetAccountId_Equal(accountId);
    }

    protected void doSetAccountId_Equal(Long accountId) {
        regAccountId(CK_EQ, accountId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountId The value of accountId as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setAccountId_NotEqual(Long accountId) {
        doSetAccountId_NotEqual(accountId);
    }

    protected void doSetAccountId_NotEqual(Long accountId) {
        regAccountId(CK_NES, accountId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountId The value of accountId as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setAccountId_GreaterThan(Long accountId) {
        regAccountId(CK_GT, accountId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountId The value of accountId as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setAccountId_LessThan(Long accountId) {
        regAccountId(CK_LT, accountId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountId The value of accountId as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setAccountId_GreaterEqual(Long accountId) {
        regAccountId(CK_GE, accountId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountId The value of accountId as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setAccountId_LessEqual(Long accountId) {
        regAccountId(CK_LE, accountId);
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param minNumber The min number of accountId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of accountId. (NullAllowed: if null, no to-condition)
     * @param opLambda The callback for option of range-of. (NotNull)
     */
    public void setAccountId_RangeOf(Long minNumber, Long maxNumber, ConditionOptionCall<RangeOfOption> opLambda) {
        setAccountId_RangeOf(minNumber, maxNumber, xcROOP(opLambda));
    }

    /**
     * RangeOf with various options. (versatile) <br>
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param minNumber The min number of accountId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of accountId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    protected void setAccountId_RangeOf(Long minNumber, Long maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, xgetCValueAccountId(), "ACCOUNT_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountIdList The collection of accountId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setAccountId_InScope(Collection<Long> accountIdList) {
        doSetAccountId_InScope(accountIdList);
    }

    protected void doSetAccountId_InScope(Collection<Long> accountIdList) {
        regINS(CK_INS, cTL(accountIdList), xgetCValueAccountId(), "ACCOUNT_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     * @param accountIdList The collection of accountId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setAccountId_NotInScope(Collection<Long> accountIdList) {
        doSetAccountId_NotInScope(accountIdList);
    }

    protected void doSetAccountId_NotInScope(Collection<Long> accountIdList) {
        regINS(CK_NINS, cTL(accountIdList), xgetCValueAccountId(), "ACCOUNT_ID");
    }

    /**
     * Set up ExistsReferrer (correlated sub-query). <br>
     * {exists (select CONDITION_ACCOUNT_ID from TASK_SETTINGS where ...)} <br>
     * TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">existsTaskSettingsByConditionAccountId</span>(settingsCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     settingsCB.query().set...
     * });
     * </pre>
     * @param subCBLambda The callback for sub-query of TaskSettingsByConditionAccountIdList for 'exists'. (NotNull)
     */
    public void existsTaskSettingsByConditionAccountId(SubQuery<TaskSettingsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForExistsReferrer(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepAccountId_ExistsReferrer_TaskSettingsByConditionAccountIdList(cb.query());
        registerExistsReferrer(cb.query(), "ACCOUNT_ID", "CONDITION_ACCOUNT_ID", pp, "taskSettingsByConditionAccountIdList");
    }
    public abstract String keepAccountId_ExistsReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq);

    /**
     * Set up ExistsReferrer (correlated sub-query). <br>
     * {exists (select EXECUTION_ACCOUNT_ID from TASK_SETTINGS where ...)} <br>
     * TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">existsTaskSettingsByExecutionAccountId</span>(settingsCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     settingsCB.query().set...
     * });
     * </pre>
     * @param subCBLambda The callback for sub-query of TaskSettingsByExecutionAccountIdList for 'exists'. (NotNull)
     */
    public void existsTaskSettingsByExecutionAccountId(SubQuery<TaskSettingsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForExistsReferrer(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepAccountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList(cb.query());
        registerExistsReferrer(cb.query(), "ACCOUNT_ID", "EXECUTION_ACCOUNT_ID", pp, "taskSettingsByExecutionAccountIdList");
    }
    public abstract String keepAccountId_ExistsReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq);

    /**
     * Set up NotExistsReferrer (correlated sub-query). <br>
     * {not exists (select CONDITION_ACCOUNT_ID from TASK_SETTINGS where ...)} <br>
     * TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">notExistsTaskSettingsByConditionAccountId</span>(settingsCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     settingsCB.query().set...
     * });
     * </pre>
     * @param subCBLambda The callback for sub-query of AccountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList for 'not exists'. (NotNull)
     */
    public void notExistsTaskSettingsByConditionAccountId(SubQuery<TaskSettingsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForExistsReferrer(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepAccountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList(cb.query());
        registerNotExistsReferrer(cb.query(), "ACCOUNT_ID", "CONDITION_ACCOUNT_ID", pp, "taskSettingsByConditionAccountIdList");
    }
    public abstract String keepAccountId_NotExistsReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq);

    /**
     * Set up NotExistsReferrer (correlated sub-query). <br>
     * {not exists (select EXECUTION_ACCOUNT_ID from TASK_SETTINGS where ...)} <br>
     * TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">notExistsTaskSettingsByExecutionAccountId</span>(settingsCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     settingsCB.query().set...
     * });
     * </pre>
     * @param subCBLambda The callback for sub-query of AccountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList for 'not exists'. (NotNull)
     */
    public void notExistsTaskSettingsByExecutionAccountId(SubQuery<TaskSettingsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForExistsReferrer(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepAccountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList(cb.query());
        registerNotExistsReferrer(cb.query(), "ACCOUNT_ID", "EXECUTION_ACCOUNT_ID", pp, "taskSettingsByExecutionAccountIdList");
    }
    public abstract String keepAccountId_NotExistsReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq);

    public void xsderiveTaskSettingsByConditionAccountIdList(String fn, SubQuery<TaskSettingsCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepAccountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList(cb.query());
        registerSpecifyDerivedReferrer(fn, cb.query(), "ACCOUNT_ID", "CONDITION_ACCOUNT_ID", pp, "taskSettingsByConditionAccountIdList", al, op);
    }
    public abstract String keepAccountId_SpecifyDerivedReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq);

    public void xsderiveTaskSettingsByExecutionAccountIdList(String fn, SubQuery<TaskSettingsCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepAccountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList(cb.query());
        registerSpecifyDerivedReferrer(fn, cb.query(), "ACCOUNT_ID", "EXECUTION_ACCOUNT_ID", pp, "taskSettingsByExecutionAccountIdList", al, op);
    }
    public abstract String keepAccountId_SpecifyDerivedReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq);

    /**
     * Prepare for (Query)DerivedReferrer (correlated sub-query). <br>
     * {FOO &lt;= (select max(BAR) from TASK_SETTINGS where ...)} <br>
     * TASK_SETTINGS by CONDITION_ACCOUNT_ID, named 'taskSettingsByConditionAccountIdAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">derivedTaskSettingsByConditionAccountId()</span>.<span style="color: #CC4747">max</span>(settingsCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     settingsCB.specify().<span style="color: #CC4747">columnFoo...</span> <span style="color: #3F7E5E">// derived column by function</span>
     *     settingsCB.query().setBar... <span style="color: #3F7E5E">// referrer condition</span>
     * }).<span style="color: #CC4747">greaterEqual</span>(123); <span style="color: #3F7E5E">// condition to derived column</span>
     * </pre>
     * @return The object to set up a function for referrer table. (NotNull)
     */
    public HpQDRFunction<TaskSettingsCB> derivedTaskSettingsByConditionAccountId() {
        return xcreateQDRFunctionTaskSettingsByConditionAccountIdList();
    }
    protected HpQDRFunction<TaskSettingsCB> xcreateQDRFunctionTaskSettingsByConditionAccountIdList() {
        return xcQDRFunc((fn, sq, rd, vl, op) -> xqderiveTaskSettingsByConditionAccountIdList(fn, sq, rd, vl, op));
    }
    public void xqderiveTaskSettingsByConditionAccountIdList(String fn, SubQuery<TaskSettingsCB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String sqpp = keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList(cb.query()); String prpp = keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdListParameter(vl);
        registerQueryDerivedReferrer(fn, cb.query(), "ACCOUNT_ID", "CONDITION_ACCOUNT_ID", sqpp, "taskSettingsByConditionAccountIdList", rd, vl, prpp, op);
    }
    public abstract String keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdList(TaskSettingsCQ sq);
    public abstract String keepAccountId_QueryDerivedReferrer_TaskSettingsByConditionAccountIdListParameter(Object vl);

    /**
     * Prepare for (Query)DerivedReferrer (correlated sub-query). <br>
     * {FOO &lt;= (select max(BAR) from TASK_SETTINGS where ...)} <br>
     * TASK_SETTINGS by EXECUTION_ACCOUNT_ID, named 'taskSettingsByExecutionAccountIdAsOne'.
     * <pre>
     * cb.query().<span style="color: #CC4747">derivedTaskSettingsByExecutionAccountId()</span>.<span style="color: #CC4747">max</span>(settingsCB <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     settingsCB.specify().<span style="color: #CC4747">columnFoo...</span> <span style="color: #3F7E5E">// derived column by function</span>
     *     settingsCB.query().setBar... <span style="color: #3F7E5E">// referrer condition</span>
     * }).<span style="color: #CC4747">greaterEqual</span>(123); <span style="color: #3F7E5E">// condition to derived column</span>
     * </pre>
     * @return The object to set up a function for referrer table. (NotNull)
     */
    public HpQDRFunction<TaskSettingsCB> derivedTaskSettingsByExecutionAccountId() {
        return xcreateQDRFunctionTaskSettingsByExecutionAccountIdList();
    }
    protected HpQDRFunction<TaskSettingsCB> xcreateQDRFunctionTaskSettingsByExecutionAccountIdList() {
        return xcQDRFunc((fn, sq, rd, vl, op) -> xqderiveTaskSettingsByExecutionAccountIdList(fn, sq, rd, vl, op));
    }
    public void xqderiveTaskSettingsByExecutionAccountIdList(String fn, SubQuery<TaskSettingsCB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        TaskSettingsCB cb = new TaskSettingsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String sqpp = keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList(cb.query()); String prpp = keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdListParameter(vl);
        registerQueryDerivedReferrer(fn, cb.query(), "ACCOUNT_ID", "EXECUTION_ACCOUNT_ID", sqpp, "taskSettingsByExecutionAccountIdList", rd, vl, prpp, op);
    }
    public abstract String keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdList(TaskSettingsCQ sq);
    public abstract String keepAccountId_QueryDerivedReferrer_TaskSettingsByExecutionAccountIdListParameter(Object vl);

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     */
    public void setAccountId_IsNull() { regAccountId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * ACCOUNT_ID: {PK, ID, NotNull, BIGINT(19)}
     */
    public void setAccountId_IsNotNull() { regAccountId(CK_ISNN, DOBJ); }

    protected void regAccountId(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueAccountId(), "ACCOUNT_ID"); }
    protected abstract ConditionValue xgetCValueAccountId();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_Equal(String displayName) {
        doSetDisplayName_Equal(fRES(displayName));
    }

    protected void doSetDisplayName_Equal(String displayName) {
        regDisplayName(CK_EQ, displayName);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_NotEqual(String displayName) {
        doSetDisplayName_NotEqual(fRES(displayName));
    }

    protected void doSetDisplayName_NotEqual(String displayName) {
        regDisplayName(CK_NES, displayName);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_GreaterThan(String displayName) {
        regDisplayName(CK_GT, fRES(displayName));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_LessThan(String displayName) {
        regDisplayName(CK_LT, fRES(displayName));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_GreaterEqual(String displayName) {
        regDisplayName(CK_GE, fRES(displayName));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_LessEqual(String displayName) {
        regDisplayName(CK_LE, fRES(displayName));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayNameList The collection of displayName as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_InScope(Collection<String> displayNameList) {
        doSetDisplayName_InScope(displayNameList);
    }

    protected void doSetDisplayName_InScope(Collection<String> displayNameList) {
        regINS(CK_INS, cTL(displayNameList), xgetCValueDisplayName(), "DISPLAY_NAME");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayNameList The collection of displayName as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setDisplayName_NotInScope(Collection<String> displayNameList) {
        doSetDisplayName_NotInScope(displayNameList);
    }

    protected void doSetDisplayName_NotInScope(Collection<String> displayNameList) {
        regINS(CK_NINS, cTL(displayNameList), xgetCValueDisplayName(), "DISPLAY_NAME");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * <pre>e.g. setDisplayName_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param displayName The value of displayName as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setDisplayName_LikeSearch(String displayName, ConditionOptionCall<LikeSearchOption> opLambda) {
        setDisplayName_LikeSearch(displayName, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)} <br>
     * <pre>e.g. setDisplayName_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param displayName The value of displayName as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setDisplayName_LikeSearch(String displayName, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(displayName), xgetCValueDisplayName(), "DISPLAY_NAME", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setDisplayName_NotLikeSearch(String displayName, ConditionOptionCall<LikeSearchOption> opLambda) {
        setDisplayName_NotLikeSearch(displayName, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @param displayName The value of displayName as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setDisplayName_NotLikeSearch(String displayName, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(displayName), xgetCValueDisplayName(), "DISPLAY_NAME", likeSearchOption);
    }

    protected void regDisplayName(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueDisplayName(), "DISPLAY_NAME"); }
    protected abstract ConditionValue xgetCValueDisplayName();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @param serviceType The value of serviceType as equal. (basically NotNull: error as default, or no condition as option)
     */
    protected void setServiceType_Equal(Integer serviceType) {
        doSetServiceType_Equal(serviceType);
    }

    /**
     * Equal(=). As ServiceType. And NullIgnored, OnlyOnceRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類
     * @param cdef The instance of classification definition (as ENUM type). (basically NotNull: error as default, or no condition as option)
     */
    public void setServiceType_Equal_AsServiceType(CDef.ServiceType cdef) {
        doSetServiceType_Equal(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * Equal(=). As Chatwork (0). And NullIgnored, OnlyOnceRegistered. <br>
     * Chatwork: Chatwork
     */
    public void setServiceType_Equal_Chatwork() {
        setServiceType_Equal_AsServiceType(CDef.ServiceType.Chatwork);
    }

    /**
     * Equal(=). As Slack (1). And NullIgnored, OnlyOnceRegistered. <br>
     * Slack: Slack
     */
    public void setServiceType_Equal_Slack() {
        setServiceType_Equal_AsServiceType(CDef.ServiceType.Slack);
    }

    protected void doSetServiceType_Equal(Integer serviceType) {
        regServiceType(CK_EQ, serviceType);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @param serviceType The value of serviceType as notEqual. (basically NotNull: error as default, or no condition as option)
     */
    protected void setServiceType_NotEqual(Integer serviceType) {
        doSetServiceType_NotEqual(serviceType);
    }

    /**
     * NotEqual(&lt;&gt;). As ServiceType. And NullIgnored, OnlyOnceRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類
     * @param cdef The instance of classification definition (as ENUM type). (basically NotNull: error as default, or no condition as option)
     */
    public void setServiceType_NotEqual_AsServiceType(CDef.ServiceType cdef) {
        doSetServiceType_NotEqual(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * NotEqual(&lt;&gt;). As Chatwork (0). And NullIgnored, OnlyOnceRegistered. <br>
     * Chatwork: Chatwork
     */
    public void setServiceType_NotEqual_Chatwork() {
        setServiceType_NotEqual_AsServiceType(CDef.ServiceType.Chatwork);
    }

    /**
     * NotEqual(&lt;&gt;). As Slack (1). And NullIgnored, OnlyOnceRegistered. <br>
     * Slack: Slack
     */
    public void setServiceType_NotEqual_Slack() {
        setServiceType_NotEqual_AsServiceType(CDef.ServiceType.Slack);
    }

    protected void doSetServiceType_NotEqual(Integer serviceType) {
        regServiceType(CK_NES, serviceType);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @param serviceTypeList The collection of serviceType as inScope. (NullAllowed: if null (or empty), no condition)
     */
    protected void setServiceType_InScope(Collection<Integer> serviceTypeList) {
        doSetServiceType_InScope(serviceTypeList);
    }

    /**
     * InScope {in (1, 2)}. As ServiceType. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setServiceType_InScope_AsServiceType(Collection<CDef.ServiceType> cdefList) {
        doSetServiceType_InScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetServiceType_InScope(Collection<Integer> serviceTypeList) {
        regINS(CK_INS, cTL(serviceTypeList), xgetCValueServiceType(), "SERVICE_TYPE");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType}
     * @param serviceTypeList The collection of serviceType as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    protected void setServiceType_NotInScope(Collection<Integer> serviceTypeList) {
        doSetServiceType_NotInScope(serviceTypeList);
    }

    /**
     * NotInScope {not in (1, 2)}. As ServiceType. And NullIgnored, NullElementIgnored, SeveralRegistered. <br>
     * SERVICE_TYPE: {NotNull, SMALLINT(5), classification=ServiceType} <br>
     * 外部サービスの種類
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setServiceType_NotInScope_AsServiceType(Collection<CDef.ServiceType> cdefList) {
        doSetServiceType_NotInScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetServiceType_NotInScope(Collection<Integer> serviceTypeList) {
        regINS(CK_NINS, cTL(serviceTypeList), xgetCValueServiceType(), "SERVICE_TYPE");
    }

    protected void regServiceType(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueServiceType(), "SERVICE_TYPE"); }
    protected abstract ConditionValue xgetCValueServiceType();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_Equal(String userName) {
        doSetUserName_Equal(fRES(userName));
    }

    protected void doSetUserName_Equal(String userName) {
        regUserName(CK_EQ, userName);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_NotEqual(String userName) {
        doSetUserName_NotEqual(fRES(userName));
    }

    protected void doSetUserName_NotEqual(String userName) {
        regUserName(CK_NES, userName);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_GreaterThan(String userName) {
        regUserName(CK_GT, fRES(userName));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_LessThan(String userName) {
        regUserName(CK_LT, fRES(userName));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_GreaterEqual(String userName) {
        regUserName(CK_GE, fRES(userName));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_LessEqual(String userName) {
        regUserName(CK_LE, fRES(userName));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userNameList The collection of userName as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_InScope(Collection<String> userNameList) {
        doSetUserName_InScope(userNameList);
    }

    protected void doSetUserName_InScope(Collection<String> userNameList) {
        regINS(CK_INS, cTL(userNameList), xgetCValueUserName(), "USER_NAME");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userNameList The collection of userName as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setUserName_NotInScope(Collection<String> userNameList) {
        doSetUserName_NotInScope(userNameList);
    }

    protected void doSetUserName_NotInScope(Collection<String> userNameList) {
        regINS(CK_NINS, cTL(userNameList), xgetCValueUserName(), "USER_NAME");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * USER_NAME: {CLOB(2147483647)} <br>
     * <pre>e.g. setUserName_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param userName The value of userName as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setUserName_LikeSearch(String userName, ConditionOptionCall<LikeSearchOption> opLambda) {
        setUserName_LikeSearch(userName, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * USER_NAME: {CLOB(2147483647)} <br>
     * <pre>e.g. setUserName_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param userName The value of userName as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setUserName_LikeSearch(String userName, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(userName), xgetCValueUserName(), "USER_NAME", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setUserName_NotLikeSearch(String userName, ConditionOptionCall<LikeSearchOption> opLambda) {
        setUserName_NotLikeSearch(userName, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     * @param userName The value of userName as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setUserName_NotLikeSearch(String userName, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(userName), xgetCValueUserName(), "USER_NAME", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     */
    public void setUserName_IsNull() { regUserName(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     */
    public void setUserName_IsNullOrEmpty() { regUserName(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * USER_NAME: {CLOB(2147483647)}
     */
    public void setUserName_IsNotNull() { regUserName(CK_ISNN, DOBJ); }

    protected void regUserName(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueUserName(), "USER_NAME"); }
    protected abstract ConditionValue xgetCValueUserName();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_Equal(String apiToken) {
        doSetApiToken_Equal(fRES(apiToken));
    }

    protected void doSetApiToken_Equal(String apiToken) {
        regApiToken(CK_EQ, apiToken);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_NotEqual(String apiToken) {
        doSetApiToken_NotEqual(fRES(apiToken));
    }

    protected void doSetApiToken_NotEqual(String apiToken) {
        regApiToken(CK_NES, apiToken);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_GreaterThan(String apiToken) {
        regApiToken(CK_GT, fRES(apiToken));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_LessThan(String apiToken) {
        regApiToken(CK_LT, fRES(apiToken));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_GreaterEqual(String apiToken) {
        regApiToken(CK_GE, fRES(apiToken));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_LessEqual(String apiToken) {
        regApiToken(CK_LE, fRES(apiToken));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiTokenList The collection of apiToken as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_InScope(Collection<String> apiTokenList) {
        doSetApiToken_InScope(apiTokenList);
    }

    protected void doSetApiToken_InScope(Collection<String> apiTokenList) {
        regINS(CK_INS, cTL(apiTokenList), xgetCValueApiToken(), "API_TOKEN");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiTokenList The collection of apiToken as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setApiToken_NotInScope(Collection<String> apiTokenList) {
        doSetApiToken_NotInScope(apiTokenList);
    }

    protected void doSetApiToken_NotInScope(Collection<String> apiTokenList) {
        regINS(CK_NINS, cTL(apiTokenList), xgetCValueApiToken(), "API_TOKEN");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)} <br>
     * <pre>e.g. setApiToken_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param apiToken The value of apiToken as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setApiToken_LikeSearch(String apiToken, ConditionOptionCall<LikeSearchOption> opLambda) {
        setApiToken_LikeSearch(apiToken, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)} <br>
     * <pre>e.g. setApiToken_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param apiToken The value of apiToken as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setApiToken_LikeSearch(String apiToken, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(apiToken), xgetCValueApiToken(), "API_TOKEN", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setApiToken_NotLikeSearch(String apiToken, ConditionOptionCall<LikeSearchOption> opLambda) {
        setApiToken_NotLikeSearch(apiToken, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     * @param apiToken The value of apiToken as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setApiToken_NotLikeSearch(String apiToken, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(apiToken), xgetCValueApiToken(), "API_TOKEN", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     */
    public void setApiToken_IsNull() { regApiToken(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     */
    public void setApiToken_IsNullOrEmpty() { regApiToken(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * API_TOKEN: {CLOB(2147483647)}
     */
    public void setApiToken_IsNotNull() { regApiToken(CK_ISNN, DOBJ); }

    protected void regApiToken(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueApiToken(), "API_TOKEN"); }
    protected abstract ConditionValue xgetCValueApiToken();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_Equal(String iconEmoji) {
        doSetIconEmoji_Equal(fRES(iconEmoji));
    }

    protected void doSetIconEmoji_Equal(String iconEmoji) {
        regIconEmoji(CK_EQ, iconEmoji);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_NotEqual(String iconEmoji) {
        doSetIconEmoji_NotEqual(fRES(iconEmoji));
    }

    protected void doSetIconEmoji_NotEqual(String iconEmoji) {
        regIconEmoji(CK_NES, iconEmoji);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_GreaterThan(String iconEmoji) {
        regIconEmoji(CK_GT, fRES(iconEmoji));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_LessThan(String iconEmoji) {
        regIconEmoji(CK_LT, fRES(iconEmoji));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_GreaterEqual(String iconEmoji) {
        regIconEmoji(CK_GE, fRES(iconEmoji));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_LessEqual(String iconEmoji) {
        regIconEmoji(CK_LE, fRES(iconEmoji));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmojiList The collection of iconEmoji as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_InScope(Collection<String> iconEmojiList) {
        doSetIconEmoji_InScope(iconEmojiList);
    }

    protected void doSetIconEmoji_InScope(Collection<String> iconEmojiList) {
        regINS(CK_INS, cTL(iconEmojiList), xgetCValueIconEmoji(), "ICON_EMOJI");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmojiList The collection of iconEmoji as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setIconEmoji_NotInScope(Collection<String> iconEmojiList) {
        doSetIconEmoji_NotInScope(iconEmojiList);
    }

    protected void doSetIconEmoji_NotInScope(Collection<String> iconEmojiList) {
        regINS(CK_NINS, cTL(iconEmojiList), xgetCValueIconEmoji(), "ICON_EMOJI");
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)} <br>
     * <pre>e.g. setIconEmoji_LikeSearch("xxx", op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">likeContain()</span>);</pre>
     * @param iconEmoji The value of iconEmoji as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setIconEmoji_LikeSearch(String iconEmoji, ConditionOptionCall<LikeSearchOption> opLambda) {
        setIconEmoji_LikeSearch(iconEmoji, xcLSOP(opLambda));
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)} <br>
     * <pre>e.g. setIconEmoji_LikeSearch("xxx", new <span style="color: #CC4747">LikeSearchOption</span>().likeContain());</pre>
     * @param iconEmoji The value of iconEmoji as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    protected void setIconEmoji_LikeSearch(String iconEmoji, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(iconEmoji), xgetCValueIconEmoji(), "ICON_EMOJI", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param opLambda The callback for option of like-search. (NotNull)
     */
    public void setIconEmoji_NotLikeSearch(String iconEmoji, ConditionOptionCall<LikeSearchOption> opLambda) {
        setIconEmoji_NotLikeSearch(iconEmoji, xcLSOP(opLambda));
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br>
     * And NullOrEmptyIgnored, SeveralRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     * @param iconEmoji The value of iconEmoji as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    protected void setIconEmoji_NotLikeSearch(String iconEmoji, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(iconEmoji), xgetCValueIconEmoji(), "ICON_EMOJI", likeSearchOption);
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     */
    public void setIconEmoji_IsNull() { regIconEmoji(CK_ISN, DOBJ); }

    /**
     * IsNullOrEmpty {is null or empty}. And OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     */
    public void setIconEmoji_IsNullOrEmpty() { regIconEmoji(CK_ISNOE, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br>
     * ICON_EMOJI: {CLOB(2147483647)}
     */
    public void setIconEmoji_IsNotNull() { regIconEmoji(CK_ISNN, DOBJ); }

    protected void regIconEmoji(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueIconEmoji(), "ICON_EMOJI"); }
    protected abstract ConditionValue xgetCValueIconEmoji();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_Equal(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_EQ,  createdAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_GreaterThan(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_GT,  createdAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_LessThan(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_LT,  createdAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_GreaterEqual(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_GE,  createdAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param createdAt The value of createdAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setCreatedAt_LessEqual(java.time.LocalDateTime createdAt) {
        regCreatedAt(CK_LE, createdAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setCreatedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setCreatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setCreatedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setCreatedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of createdAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setCreatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "CREATED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueCreatedAt(), nm, op);
    }

    protected void regCreatedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueCreatedAt(), "CREATED_AT"); }
    protected abstract ConditionValue xgetCValueCreatedAt();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as equal. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_Equal(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_EQ,  updatedAt);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as greaterThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_GreaterThan(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_GT,  updatedAt);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as lessThan. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_LessThan(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_LT,  updatedAt);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as greaterEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_GreaterEqual(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_GE,  updatedAt);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @param updatedAt The value of updatedAt as lessEqual. (basically NotNull: error as default, or no condition as option)
     */
    public void setUpdatedAt_LessEqual(java.time.LocalDateTime updatedAt) {
        regUpdatedAt(CK_LE, updatedAt);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setUpdatedAt_FromTo(fromDate, toDate, op <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> op.<span style="color: #CC4747">compareAsDate()</span>);</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param opLambda The callback for option of from-to. (NotNull)
     */
    public void setUpdatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, ConditionOptionCall<FromToOption> opLambda) {
        setUpdatedAt_FromTo(fromDatetime, toDatetime, xcFTOP(opLambda));
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br>
     * And NullIgnored, OnlyOnceRegistered. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * <pre>e.g. setUpdatedAt_FromTo(fromDate, toDate, new <span style="color: #CC4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of updatedAt. (basically NotNull: if op.allowOneSide(), null allowed)
     * @param fromToOption The option of from-to. (NotNull)
     */
    protected void setUpdatedAt_FromTo(java.time.LocalDateTime fromDatetime, java.time.LocalDateTime toDatetime, FromToOption fromToOption) {
        String nm = "UPDATED_AT"; FromToOption op = fromToOption;
        regFTQ(xfFTHD(fromDatetime, nm, op), xfFTHD(toDatetime, nm, op), xgetCValueUpdatedAt(), nm, op);
    }

    protected void regUpdatedAt(ConditionKey ky, Object vl) { regQ(ky, vl, xgetCValueUpdatedAt(), "UPDATED_AT"); }
    protected abstract ConditionValue xgetCValueUpdatedAt();

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO = (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<AccountsCB> scalar_Equal() {
        return xcreateSLCFunction(CK_EQ, AccountsCB.class);
    }

    /**
     * Prepare ScalarCondition as equal. <br>
     * {where FOO &lt;&gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<AccountsCB> scalar_NotEqual() {
        return xcreateSLCFunction(CK_NES, AccountsCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterThan. <br>
     * {where FOO &gt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<AccountsCB> scalar_GreaterThan() {
        return xcreateSLCFunction(CK_GT, AccountsCB.class);
    }

    /**
     * Prepare ScalarCondition as lessThan. <br>
     * {where FOO &lt; (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<AccountsCB> scalar_LessThan() {
        return xcreateSLCFunction(CK_LT, AccountsCB.class);
    }

    /**
     * Prepare ScalarCondition as greaterEqual. <br>
     * {where FOO &gt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().scalar_Equal().<span style="color: #CC4747">avg</span>(<span style="color: #553000">purchaseCB</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">purchaseCB</span>.specify().<span style="color: #CC4747">columnPurchasePrice</span>(); <span style="color: #3F7E5E">// *Point!</span>
     *     <span style="color: #553000">purchaseCB</span>.query().setPaymentCompleteFlg_Equal_True();
     * });
     * </pre> 
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<AccountsCB> scalar_GreaterEqual() {
        return xcreateSLCFunction(CK_GE, AccountsCB.class);
    }

    /**
     * Prepare ScalarCondition as lessEqual. <br>
     * {where FOO &lt;= (select max(BAR) from ...)}
     * <pre>
     * cb.query().<span style="color: #CC4747">scalar_LessEqual()</span>.max(new SubQuery&lt;AccountsCB&gt;() {
     *     public void query(AccountsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSLCFunction<AccountsCB> scalar_LessEqual() {
        return xcreateSLCFunction(CK_LE, AccountsCB.class);
    }

    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xscalarCondition(String fn, SubQuery<CB> sq, String rd, HpSLCCustomized<CB> cs, ScalarConditionOption op) {
        assertObjectNotNull("subQuery", sq);
        AccountsCB cb = xcreateScalarConditionCB(); sq.query((CB)cb);
        String pp = keepScalarCondition(cb.query()); // for saving query-value
        cs.setPartitionByCBean((CB)xcreateScalarConditionPartitionByCB()); // for using partition-by
        registerScalarCondition(fn, cb.query(), pp, rd, cs, op);
    }
    public abstract String keepScalarCondition(AccountsCQ sq);

    protected AccountsCB xcreateScalarConditionCB() {
        AccountsCB cb = newMyCB(); cb.xsetupForScalarCondition(this); return cb;
    }

    protected AccountsCB xcreateScalarConditionPartitionByCB() {
        AccountsCB cb = newMyCB(); cb.xsetupForScalarConditionPartitionBy(this); return cb;
    }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public void xsmyselfDerive(String fn, SubQuery<AccountsCB> sq, String al, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        AccountsCB cb = new AccountsCB(); cb.xsetupForDerivedReferrer(this);
        lockCall(() -> sq.query(cb)); String pp = keepSpecifyMyselfDerived(cb.query()); String pk = "ACCOUNT_ID";
        registerSpecifyMyselfDerived(fn, cb.query(), pk, pk, pp, "myselfDerived", al, op);
    }
    public abstract String keepSpecifyMyselfDerived(AccountsCQ sq);

    /**
     * Prepare for (Query)MyselfDerived (correlated sub-query).
     * @return The object to set up a function for myself table. (NotNull)
     */
    public HpQDRFunction<AccountsCB> myselfDerived() {
        return xcreateQDRFunctionMyselfDerived(AccountsCB.class);
    }
    @SuppressWarnings("unchecked")
    protected <CB extends ConditionBean> void xqderiveMyselfDerived(String fn, SubQuery<CB> sq, String rd, Object vl, DerivedReferrerOption op) {
        assertObjectNotNull("subQuery", sq);
        AccountsCB cb = new AccountsCB(); cb.xsetupForDerivedReferrer(this); sq.query((CB)cb);
        String pk = "ACCOUNT_ID";
        String sqpp = keepQueryMyselfDerived(cb.query()); // for saving query-value.
        String prpp = keepQueryMyselfDerivedParameter(vl);
        registerQueryMyselfDerived(fn, cb.query(), pk, pk, sqpp, "myselfDerived", rd, vl, prpp, op);
    }
    public abstract String keepQueryMyselfDerived(AccountsCQ sq);
    public abstract String keepQueryMyselfDerivedParameter(Object vl);

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    /**
     * Prepare for MyselfExists (correlated sub-query).
     * @param subCBLambda The implementation of sub-query. (NotNull)
     */
    public void myselfExists(SubQuery<AccountsCB> subCBLambda) {
        assertObjectNotNull("subCBLambda", subCBLambda);
        AccountsCB cb = new AccountsCB(); cb.xsetupForMyselfExists(this);
        lockCall(() -> subCBLambda.query(cb)); String pp = keepMyselfExists(cb.query());
        registerMyselfExists(cb.query(), pp);
    }
    public abstract String keepMyselfExists(AccountsCQ sq);

    // ===================================================================================
    //                                                                        Manual Order
    //                                                                        ============
    /**
     * Order along manual ordering information.
     * <pre>
     * cb.query().addOrderBy_Birthdate_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when BIRTHDATE &gt;= '2000/01/01' then 0</span>
     * <span style="color: #3F7E5E">//     else 1</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     *
     * cb.query().addOrderBy_MemberStatusCode_Asc().<span style="color: #CC4747">withManualOrder</span>(<span style="color: #553000">op</span> <span style="color: #90226C; font-weight: bold"><span style="font-size: 120%">-</span>&gt;</span> {
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_GreaterEqual</span>(priorityDate); <span style="color: #3F7E5E">// e.g. 2000/01/01</span>
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Withdrawal);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Formalized);
     *     <span style="color: #553000">op</span>.<span style="color: #CC4747">when_Equal</span>(CDef.MemberStatus.Provisional);
     * });
     * <span style="color: #3F7E5E">// order by </span>
     * <span style="color: #3F7E5E">//   case</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'WDL' then 0</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'FML' then 1</span>
     * <span style="color: #3F7E5E">//     when MEMBER_STATUS_CODE = 'PRV' then 2</span>
     * <span style="color: #3F7E5E">//     else 3</span>
     * <span style="color: #3F7E5E">//   end asc, ...</span>
     * </pre>
     * <p>This function with Union is unsupported!</p>
     * <p>The order values are bound (treated as bind parameter).</p>
     * @param opLambda The callback for option of manual-order containing order values. (NotNull)
     */
    public void withManualOrder(ManualOrderOptionCall opLambda) { // is user public!
        xdoWithManualOrder(cMOO(opLambda));
    }

    // ===================================================================================
    //                                                                    Small Adjustment
    //                                                                    ================
    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    protected AccountsCB newMyCB() {
        return new AccountsCB();
    }
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xabUDT() { return Date.class.getName(); }
    protected String xabCQ() { return AccountsCQ.class.getName(); }
    protected String xabLSO() { return LikeSearchOption.class.getName(); }
    protected String xabSLCS() { return HpSLCSetupper.class.getName(); }
    protected String xabSCP() { return SubQuery.class.getName(); }
}
