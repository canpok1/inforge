package jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.bs;

import java.util.Map;

import org.dbflute.cbean.*;
import org.dbflute.cbean.chelper.*;
import org.dbflute.cbean.coption.*;
import org.dbflute.cbean.cvalue.ConditionValue;
import org.dbflute.cbean.sqlclause.SqlClause;
import org.dbflute.exception.IllegalConditionBeanOperationException;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.ciq.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.*;
import jp.gr.java_conf.ktnet.inforge.dbflute.cbean.cq.*;

/**
 * The base condition-query of TASK_SETTINGS.
 * @author DBFlute(AutoGenerator)
 */
public class BsTaskSettingsCQ extends AbstractBsTaskSettingsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected TaskSettingsCIQ _inlineQuery;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsTaskSettingsCQ(ConditionQuery referrerQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(referrerQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                 InlineView/OrClause
    //                                                                 ===================
    /**
     * Prepare InlineView query. <br>
     * {select ... from ... left outer join (select * from TASK_SETTINGS) where FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">inline()</span>.setFoo...;
     * </pre>
     * @return The condition-query for InlineView query. (NotNull)
     */
    public TaskSettingsCIQ inline() {
        if (_inlineQuery == null) { _inlineQuery = xcreateCIQ(); }
        _inlineQuery.xsetOnClause(false); return _inlineQuery;
    }

    protected TaskSettingsCIQ xcreateCIQ() {
        TaskSettingsCIQ ciq = xnewCIQ();
        ciq.xsetBaseCB(_baseCB);
        return ciq;
    }

    protected TaskSettingsCIQ xnewCIQ() {
        return new TaskSettingsCIQ(xgetReferrerQuery(), xgetSqlClause(), xgetAliasName(), xgetNestLevel(), this);
    }

    /**
     * Prepare OnClause query. <br>
     * {select ... from ... left outer join TASK_SETTINGS on ... and FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #CC4747">on()</span>.setFoo...;
     * </pre>
     * @return The condition-query for OnClause query. (NotNull)
     * @throws IllegalConditionBeanOperationException When this condition-query is base query.
     */
    public TaskSettingsCIQ on() {
        if (isBaseQuery()) { throw new IllegalConditionBeanOperationException("OnClause for local table is unavailable!"); }
        TaskSettingsCIQ inlineQuery = inline(); inlineQuery.xsetOnClause(true); return inlineQuery;
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    protected ConditionValue _taskSettingId;
    public ConditionValue xdfgetTaskSettingId()
    { if (_taskSettingId == null) { _taskSettingId = nCV(); }
      return _taskSettingId; }
    protected ConditionValue xgetCValueTaskSettingId() { return xdfgetTaskSettingId(); }

    public Map<String, ArticlesCQ> xdfgetTaskSettingId_ExistsReferrer_ArticlesList() { return xgetSQueMap("taskSettingId_ExistsReferrer_ArticlesList"); }
    public String keepTaskSettingId_ExistsReferrer_ArticlesList(ArticlesCQ sq) { return xkeepSQue("taskSettingId_ExistsReferrer_ArticlesList", sq); }

    public Map<String, ArticlesCQ> xdfgetTaskSettingId_NotExistsReferrer_ArticlesList() { return xgetSQueMap("taskSettingId_NotExistsReferrer_ArticlesList"); }
    public String keepTaskSettingId_NotExistsReferrer_ArticlesList(ArticlesCQ sq) { return xkeepSQue("taskSettingId_NotExistsReferrer_ArticlesList", sq); }

    public Map<String, ArticlesCQ> xdfgetTaskSettingId_SpecifyDerivedReferrer_ArticlesList() { return xgetSQueMap("taskSettingId_SpecifyDerivedReferrer_ArticlesList"); }
    public String keepTaskSettingId_SpecifyDerivedReferrer_ArticlesList(ArticlesCQ sq) { return xkeepSQue("taskSettingId_SpecifyDerivedReferrer_ArticlesList", sq); }

    public Map<String, ArticlesCQ> xdfgetTaskSettingId_QueryDerivedReferrer_ArticlesList() { return xgetSQueMap("taskSettingId_QueryDerivedReferrer_ArticlesList"); }
    public String keepTaskSettingId_QueryDerivedReferrer_ArticlesList(ArticlesCQ sq) { return xkeepSQue("taskSettingId_QueryDerivedReferrer_ArticlesList", sq); }
    public Map<String, Object> xdfgetTaskSettingId_QueryDerivedReferrer_ArticlesListParameter() { return xgetSQuePmMap("taskSettingId_QueryDerivedReferrer_ArticlesList"); }
    public String keepTaskSettingId_QueryDerivedReferrer_ArticlesListParameter(Object pm) { return xkeepSQuePm("taskSettingId_QueryDerivedReferrer_ArticlesList", pm); }

    /** 
     * Add order-by as ascend. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_TaskSettingId_Asc() { regOBA("TASK_SETTING_ID"); return this; }

    /**
     * Add order-by as descend. <br>
     * TASK_SETTING_ID: {PK, ID, NotNull, BIGINT(19)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_TaskSettingId_Desc() { regOBD("TASK_SETTING_ID"); return this; }

    protected ConditionValue _displayName;
    public ConditionValue xdfgetDisplayName()
    { if (_displayName == null) { _displayName = nCV(); }
      return _displayName; }
    protected ConditionValue xgetCValueDisplayName() { return xdfgetDisplayName(); }

    /** 
     * Add order-by as ascend. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_DisplayName_Asc() { regOBA("DISPLAY_NAME"); return this; }

    /**
     * Add order-by as descend. <br>
     * DISPLAY_NAME: {NotNull, CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_DisplayName_Desc() { regOBD("DISPLAY_NAME"); return this; }

    protected ConditionValue _cronExpression;
    public ConditionValue xdfgetCronExpression()
    { if (_cronExpression == null) { _cronExpression = nCV(); }
      return _cronExpression; }
    protected ConditionValue xgetCValueCronExpression() { return xdfgetCronExpression(); }

    /** 
     * Add order-by as ascend. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_CronExpression_Asc() { regOBA("CRON_EXPRESSION"); return this; }

    /**
     * Add order-by as descend. <br>
     * CRON_EXPRESSION: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_CronExpression_Desc() { regOBD("CRON_EXPRESSION"); return this; }

    protected ConditionValue _conditionType;
    public ConditionValue xdfgetConditionType()
    { if (_conditionType == null) { _conditionType = nCV(); }
      return _conditionType; }
    protected ConditionValue xgetCValueConditionType() { return xdfgetConditionType(); }

    /** 
     * Add order-by as ascend. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ConditionType_Asc() { regOBA("CONDITION_TYPE"); return this; }

    /**
     * Add order-by as descend. <br>
     * CONDITION_TYPE: {NotNull, SMALLINT(5), classification=ConditionType}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ConditionType_Desc() { regOBD("CONDITION_TYPE"); return this; }

    protected ConditionValue _conditionValue;
    public ConditionValue xdfgetConditionValue()
    { if (_conditionValue == null) { _conditionValue = nCV(); }
      return _conditionValue; }
    protected ConditionValue xgetCValueConditionValue() { return xdfgetConditionValue(); }

    /** 
     * Add order-by as ascend. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ConditionValue_Asc() { regOBA("CONDITION_VALUE"); return this; }

    /**
     * Add order-by as descend. <br>
     * CONDITION_VALUE: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ConditionValue_Desc() { regOBD("CONDITION_VALUE"); return this; }

    protected ConditionValue _conditionAccountId;
    public ConditionValue xdfgetConditionAccountId()
    { if (_conditionAccountId == null) { _conditionAccountId = nCV(); }
      return _conditionAccountId; }
    protected ConditionValue xgetCValueConditionAccountId() { return xdfgetConditionAccountId(); }

    /** 
     * Add order-by as ascend. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ConditionAccountId_Asc() { regOBA("CONDITION_ACCOUNT_ID"); return this; }

    /**
     * Add order-by as descend. <br>
     * CONDITION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ConditionAccountId_Desc() { regOBD("CONDITION_ACCOUNT_ID"); return this; }

    protected ConditionValue _executionType;
    public ConditionValue xdfgetExecutionType()
    { if (_executionType == null) { _executionType = nCV(); }
      return _executionType; }
    protected ConditionValue xgetCValueExecutionType() { return xdfgetExecutionType(); }

    /** 
     * Add order-by as ascend. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ExecutionType_Asc() { regOBA("EXECUTION_TYPE"); return this; }

    /**
     * Add order-by as descend. <br>
     * EXECUTION_TYPE: {NotNull, SMALLINT(5), classification=ExecutionType}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ExecutionType_Desc() { regOBD("EXECUTION_TYPE"); return this; }

    protected ConditionValue _executionValue;
    public ConditionValue xdfgetExecutionValue()
    { if (_executionValue == null) { _executionValue = nCV(); }
      return _executionValue; }
    protected ConditionValue xgetCValueExecutionValue() { return xdfgetExecutionValue(); }

    /** 
     * Add order-by as ascend. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ExecutionValue_Asc() { regOBA("EXECUTION_VALUE"); return this; }

    /**
     * Add order-by as descend. <br>
     * EXECUTION_VALUE: {CLOB(2147483647)}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ExecutionValue_Desc() { regOBD("EXECUTION_VALUE"); return this; }

    protected ConditionValue _executionAccountId;
    public ConditionValue xdfgetExecutionAccountId()
    { if (_executionAccountId == null) { _executionAccountId = nCV(); }
      return _executionAccountId; }
    protected ConditionValue xgetCValueExecutionAccountId() { return xdfgetExecutionAccountId(); }

    /** 
     * Add order-by as ascend. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ExecutionAccountId_Asc() { regOBA("EXECUTION_ACCOUNT_ID"); return this; }

    /**
     * Add order-by as descend. <br>
     * EXECUTION_ACCOUNT_ID: {IX, BIGINT(19), FK to ACCOUNTS}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_ExecutionAccountId_Desc() { regOBD("EXECUTION_ACCOUNT_ID"); return this; }

    protected ConditionValue _createdAt;
    public ConditionValue xdfgetCreatedAt()
    { if (_createdAt == null) { _createdAt = nCV(); }
      return _createdAt; }
    protected ConditionValue xgetCValueCreatedAt() { return xdfgetCreatedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_CreatedAt_Asc() { regOBA("CREATED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * CREATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_CreatedAt_Desc() { regOBD("CREATED_AT"); return this; }

    protected ConditionValue _updatedAt;
    public ConditionValue xdfgetUpdatedAt()
    { if (_updatedAt == null) { _updatedAt = nCV(); }
      return _updatedAt; }
    protected ConditionValue xgetCValueUpdatedAt() { return xdfgetUpdatedAt(); }

    /** 
     * Add order-by as ascend. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_UpdatedAt_Asc() { regOBA("UPDATED_AT"); return this; }

    /**
     * Add order-by as descend. <br>
     * UPDATED_AT: {NotNull, TIMESTAMP(23, 10), default=[CURRENT_TIMESTAMP()]}
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addOrderBy_UpdatedAt_Desc() { regOBD("UPDATED_AT"); return this; }

    // ===================================================================================
    //                                                             SpecifiedDerivedOrderBy
    //                                                             =======================
    /**
     * Add order-by for specified derived column as ascend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] asc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Asc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addSpecifiedDerivedOrderBy_Asc(String aliasName) { registerSpecifiedDerivedOrderBy_Asc(aliasName); return this; }

    /**
     * Add order-by for specified derived column as descend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #CC4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] desc</span>
     * cb.<span style="color: #CC4747">addSpecifiedDerivedOrderBy_Desc</span>(<span style="color: #CC4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsTaskSettingsCQ addSpecifiedDerivedOrderBy_Desc(String aliasName) { registerSpecifiedDerivedOrderBy_Desc(aliasName); return this; }

    // ===================================================================================
    //                                                                         Union Query
    //                                                                         ===========
    public void reflectRelationOnUnionQuery(ConditionQuery bqs, ConditionQuery uqs) {
        TaskSettingsCQ bq = (TaskSettingsCQ)bqs;
        TaskSettingsCQ uq = (TaskSettingsCQ)uqs;
        if (bq.hasConditionQueryAccountsByConditionAccountId()) {
            uq.queryAccountsByConditionAccountId().reflectRelationOnUnionQuery(bq.queryAccountsByConditionAccountId(), uq.queryAccountsByConditionAccountId());
        }
        if (bq.hasConditionQueryAccountsByExecutionAccountId()) {
            uq.queryAccountsByExecutionAccountId().reflectRelationOnUnionQuery(bq.queryAccountsByExecutionAccountId(), uq.queryAccountsByExecutionAccountId());
        }
    }

    // ===================================================================================
    //                                                                       Foreign Query
    //                                                                       =============
    /**
     * Get the condition-query for relation table. <br>
     * ACCOUNTS by my CONDITION_ACCOUNT_ID, named 'accountsByConditionAccountId'.
     * @return The instance of condition-query. (NotNull)
     */
    public AccountsCQ queryAccountsByConditionAccountId() {
        return xdfgetConditionQueryAccountsByConditionAccountId();
    }
    public AccountsCQ xdfgetConditionQueryAccountsByConditionAccountId() {
        String prop = "accountsByConditionAccountId";
        if (!xhasQueRlMap(prop)) { xregQueRl(prop, xcreateQueryAccountsByConditionAccountId()); xsetupOuterJoinAccountsByConditionAccountId(); }
        return xgetQueRlMap(prop);
    }
    protected AccountsCQ xcreateQueryAccountsByConditionAccountId() {
        String nrp = xresolveNRP("TASK_SETTINGS", "accountsByConditionAccountId"); String jan = xresolveJAN(nrp, xgetNNLvl());
        return xinitRelCQ(new AccountsCQ(this, xgetSqlClause(), jan, xgetNNLvl()), _baseCB, "accountsByConditionAccountId", nrp);
    }
    protected void xsetupOuterJoinAccountsByConditionAccountId() { xregOutJo("accountsByConditionAccountId"); }
    public boolean hasConditionQueryAccountsByConditionAccountId() { return xhasQueRlMap("accountsByConditionAccountId"); }

    /**
     * Get the condition-query for relation table. <br>
     * ACCOUNTS by my EXECUTION_ACCOUNT_ID, named 'accountsByExecutionAccountId'.
     * @return The instance of condition-query. (NotNull)
     */
    public AccountsCQ queryAccountsByExecutionAccountId() {
        return xdfgetConditionQueryAccountsByExecutionAccountId();
    }
    public AccountsCQ xdfgetConditionQueryAccountsByExecutionAccountId() {
        String prop = "accountsByExecutionAccountId";
        if (!xhasQueRlMap(prop)) { xregQueRl(prop, xcreateQueryAccountsByExecutionAccountId()); xsetupOuterJoinAccountsByExecutionAccountId(); }
        return xgetQueRlMap(prop);
    }
    protected AccountsCQ xcreateQueryAccountsByExecutionAccountId() {
        String nrp = xresolveNRP("TASK_SETTINGS", "accountsByExecutionAccountId"); String jan = xresolveJAN(nrp, xgetNNLvl());
        return xinitRelCQ(new AccountsCQ(this, xgetSqlClause(), jan, xgetNNLvl()), _baseCB, "accountsByExecutionAccountId", nrp);
    }
    protected void xsetupOuterJoinAccountsByExecutionAccountId() { xregOutJo("accountsByExecutionAccountId"); }
    public boolean hasConditionQueryAccountsByExecutionAccountId() { return xhasQueRlMap("accountsByExecutionAccountId"); }

    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String property) {
        return null;
    }

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    public Map<String, TaskSettingsCQ> xdfgetScalarCondition() { return xgetSQueMap("scalarCondition"); }
    public String keepScalarCondition(TaskSettingsCQ sq) { return xkeepSQue("scalarCondition", sq); }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public Map<String, TaskSettingsCQ> xdfgetSpecifyMyselfDerived() { return xgetSQueMap("specifyMyselfDerived"); }
    public String keepSpecifyMyselfDerived(TaskSettingsCQ sq) { return xkeepSQue("specifyMyselfDerived", sq); }

    public Map<String, TaskSettingsCQ> xdfgetQueryMyselfDerived() { return xgetSQueMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerived(TaskSettingsCQ sq) { return xkeepSQue("queryMyselfDerived", sq); }
    public Map<String, Object> xdfgetQueryMyselfDerivedParameter() { return xgetSQuePmMap("queryMyselfDerived"); }
    public String keepQueryMyselfDerivedParameter(Object pm) { return xkeepSQuePm("queryMyselfDerived", pm); }

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    protected Map<String, TaskSettingsCQ> _myselfExistsMap;
    public Map<String, TaskSettingsCQ> xdfgetMyselfExists() { return xgetSQueMap("myselfExists"); }
    public String keepMyselfExists(TaskSettingsCQ sq) { return xkeepSQue("myselfExists", sq); }

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    public Map<String, TaskSettingsCQ> xdfgetMyselfInScope() { return xgetSQueMap("myselfInScope"); }
    public String keepMyselfInScope(TaskSettingsCQ sq) { return xkeepSQue("myselfInScope", sq); }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xCB() { return TaskSettingsCB.class.getName(); }
    protected String xCQ() { return TaskSettingsCQ.class.getName(); }
    protected String xCHp() { return HpQDRFunction.class.getName(); }
    protected String xCOp() { return ConditionOption.class.getName(); }
    protected String xMap() { return Map.class.getName(); }
}
