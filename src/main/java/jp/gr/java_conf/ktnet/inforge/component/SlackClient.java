package jp.gr.java_conf.ktnet.inforge.component;

import jp.gr.java_conf.ktnet.inforge.dbflute.exentity.Accounts;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;

/**
 * Slack関連の操作を行うクラスです.
 * @author tanabe
 *
 */
@Component
@Slf4j
public class SlackClient {

  private static final String SLACK_URL = "https://slack.com/api/chat.postMessage";

  /**
   * Slackにメッセージを投稿します.
   * @param message メッセージ.
   * @param channelName 投稿先のチャネル名.
   * @param account Slackアカウント情報.
   * @throws URISyntaxException メッセージ投稿に失敗した場合.
   */
  public void postMessage(
      @NonNull final String message,
      @NonNull final String channelName,
      @NonNull final Accounts account
  )  {
    log.info("Send message to slack channel[" + channelName + "][" + message + "]");
    
    if (!account.isServiceTypeSlack()) {
      throw new IllegalArgumentException("指定アカウントが通知先サービスのものと異なります[" + account +  "]");
    }
    
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<String, Object>();
    requestMap.add("token", account.getApiToken());
    requestMap.add("channel", channelName);
    requestMap.add("text", message);
    
    if (account.getUserName() != null) {
      requestMap.add("username", account.getUserName());
    }
    if (account.getIconEmoji() != null) {
      requestMap.add("icon_emoji", account.getIconEmoji());
    }
    
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<Response> response
        = restTemplate.postForEntity(SLACK_URL, requestMap, Response.class);
    log.info("Response[{}]", response);
    if (!response.getBody().isOk()) {
      throw new HttpClientErrorException(response.getStatusCode(), response.getBody().toString());
    }
  }
  
  /**
   * Slackの設定を保持するクラスです.
   * @author tanabe
   *
   */
  @Data
  @Builder
  public static class Setting {
    /**
     * APIトークン.
     */
    @NonNull
    private String apiToken;
    
    /**
     * チャネル名.
     */
    @NonNull
    private String channelName;
    
    /**
     * ユーザ名.
     */
    private String username;
    
    /**
     * アイコン.
     */
    private String iconEmoji;
  }
  
  @Data
  @NoArgsConstructor
  private static class Response {
    private boolean ok;
    private String stuff;
    private String error;
    private String warning;
  }
}