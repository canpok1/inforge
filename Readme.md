# Inforge

定期的に情報を扱う仕事をこなすボットです。


## 開発の手順


### 環境構築手順

1. 必要ツールをインストール。

    * jdk8以上

    * Gitクライアント

2. リポジトリをクローン。

3. ビルドが通ることを確認。

    * `./gradlew build`

4. 起動することを確認。

    * `java -jar build/libs/inforge-0.0.1-SNAPSHOT.jar`

    * コンソールに`Started InforgeApplication in 〜`と表示されればOK。

    * 停止するときは`Ctrl`と`C`を同時押し。


### DBスキーマの更新

次の手順で更新を行います。

1. DDLを作成。

    * 保存先は、`src/main/resources/db/migration`

    * ファイル名は、`V連番__概要.sql`とすること。

        * （例）V1__Create_sample.sql

2. テスト用DBを自動生成。

    * Flywayを使ってDBを生成します。

    * コマンド実行。 `./gradlew flywayMigrate`

    * `gradle.properties`の`flyway.url`の設定でH2のDBファイルが生成されます。

        * （例）`flyway.url=jdbc:h2:file:./inforge`の場合、プロジェクト直下に`inforge.mv.db`が生成されます。

3. DB関連のクラスコード等を自動生成。

    * dbfluteクライアントを使ってクラスコード等を生成します。

    * スクリプトを実行し、表示されるメニューで2を選択。 `sh dbflute_inforge/manage.sh`

        * スクリプトの起動引数で指定しても可。 `sh dbflute_inforge/manage.sh 2`

        * Windowsの場合は`dbflute_inforge/manage.bat`を実行すること。

    * テスト用DBを変更している場合、`dbflute_inforge/dfprop/databaseInfoMap.dfprop`の設定値を変更してください。

4. 追加・更新したファイルをコミット。


### DBのレコードの確認

H2データベースのjarを使用することで確認できます。

次の場所に配置しています。

`mydbflute/dbflute-1.1.2/lib/h2-1.4.193.jar`
